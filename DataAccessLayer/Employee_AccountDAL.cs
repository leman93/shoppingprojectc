﻿using Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace DataAccessLayer
{
    public class Employee_AccountDAL
    {
        public DataSet GetAccountByDataSet()
        {
            string SQL = "select * from Employee_Account";
            DataSet dsAccount = new DataSet();
            try
            {
                dsAccount = DataProvider.ExecuteQueryWithDataSet(SQL, CommandType.Text);

            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            return dsAccount;
        }


        public Employee_Account GetAccountByUsername(string username)
        {
            Employee_Account p = null;
            string sqlSelect = "Select * from Employee_Account where Username='" + username + "'";
            SqlDataReader rd = DataProvider.ExecuteQueryWithDataReader
                (sqlSelect, CommandType.Text);
            if (rd.HasRows)
            {
                while (rd.Read())

                {
                    p = new Employee_Account()
                    {
                        Username = rd.GetString(0),
                        Password = rd.GetString(1),
                        Employee_ID = rd.GetString(2),
                        Level_Access = rd.GetInt32(3),
                        Enable = rd.GetBoolean(4),
                        Created_Date = rd.GetDateTime(5),
                        Created_By_Username = rd.GetString(6)
                    };

                }
            }
            return p;
        }
        public bool addNewAccount(Employee_Account emp)
        {

            string SQL =
                "Insert Employee_Account values(@Username,@Password,@Employee_ID,@Level_Access,@Enable,@Created_Date,@Created_By_Username)";
            SqlParameter username = new SqlParameter("@Username", emp.Username);
            SqlParameter password = new SqlParameter("@Password", emp.Password);
            SqlParameter Employee_ID = new SqlParameter("@Employee_ID", emp.Employee_ID);
            SqlParameter Level_Access = new SqlParameter("@Level_Access", emp.Level_Access);
            SqlParameter Enable = new SqlParameter("@Enable", emp.Enable);
            SqlParameter Created_Date = new SqlParameter("@Created_Date", emp.Created_Date);
            SqlParameter Created_By_Username = new SqlParameter("@Created_By_Username", emp.Created_By_Username);

            try
            {
                return DataProvider.ExecuteNonQuery(SQL, CommandType.Text, username, password, Employee_ID, Level_Access, Enable, Created_Date, Created_By_Username);
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
        }

        public bool getUsername(string username)
        {
            string sql = "SELECT * FROM Employee_Account WHERE Username = '" + username + "'";

            SqlDataReader reader = DataProvider.ExecuteQueryWithDataReader(sql, CommandType.Text);
            if (reader.Read())
            {
                return true;
            }

            return false;
        }

        public bool getEmployeeID(string id)
        {
            string sql = "SELECT * FROM Employee_Account WHERE Employee_ID = '" + id + "'";

            SqlDataReader reader = DataProvider.ExecuteQueryWithDataReader(sql, CommandType.Text);
            if (reader.Read())
            {
                return true;
            }

            return false;
        }


        public bool updateAccount(string username, string password, int Level_Access)
        {
            string SQL = "Update Employee_Account set Password=@Password,Level_Access=@Level_Access Where Username =@Username";
            SqlParameter id = new SqlParameter("@Username", username);

            SqlParameter pass = new SqlParameter("@Password", password);
            SqlParameter Level_Acc = new SqlParameter("@Level_Access", Level_Access);


            try
            {
                return DataProvider.ExecuteNonQuery(SQL, CommandType.Text, id, pass, Level_Acc);
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
        }

        public bool deleteAccount(string username)
        {
            string SQL = "Delete Employee_Account where Username=@ID";
            SqlParameter id = new SqlParameter("@ID", username);
            try
            {
                return DataProvider.ExecuteNonQuery(SQL, CommandType.Text, id);
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
        }


        public List<Employee_Account> searchByUsername(string Keyword)
        {
            string sql = "Select * FROM Employee_Account Where Username LIKE N'%"+Keyword+"%'";
            //string sql = "SELECT * FROM Employee_Account WHERE Username = '" + Keyword + "'";
            List<Employee_Account> list = new List<Employee_Account>();
            Employee_Account p;
            SqlDataReader rd = DataProvider.ExecuteQueryWithDataReader
               (sql, CommandType.Text);
            if (rd.HasRows)
            {
                while (rd.Read())

                {
                    p = new Employee_Account()
                    {
                        Username = rd.GetString(0),
                        Password = rd.GetString(1),
                        Employee_ID = rd.GetString(2),
                        Level_Access = rd.GetInt32(3),
                        Enable = rd.GetBoolean(4),
                        Created_Date = rd.GetDateTime(5),
                        Created_By_Username = rd.GetString(6)                    
                    };
                    list.Add(p);
                }
            }
            return list;
        }
    }










}
