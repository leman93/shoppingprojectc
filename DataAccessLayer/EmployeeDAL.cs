﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class EmployeeDAL
    {
        public DataSet GetEmpByDataSet()
        {
            string SQL = "select * from Employee Where Enable=@Enable";
            SqlParameter enable = new SqlParameter("@Enable", true);
            DataSet dsEmp = new DataSet();
            try
            {
                dsEmp = DataProvider.ExecuteQueryWithDataSet(SQL, CommandType.Text, enable);

            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            return dsEmp;
        }
        public bool getEmployeeID(string EmployeeID)
        {
            string sql = "SELECT * FROM Employee WHERE Employee_ID = '" + EmployeeID + "'";

            SqlDataReader reader = DataProvider.ExecuteQueryWithDataReader(sql, CommandType.Text);
            if (reader.Read())
            {
                return true;
            }

            return false;
        }


        public string getTOPEmployee()
        {
            string employee_IDcurr = "";
            string SQL = "SELECT TOP 1 Employee_ID FROM Employee ORDER BY Employee_ID DESC  ";
            SqlDataReader reader = DataProvider.ExecuteQueryWithDataReader(SQL, CommandType.Text);
            if (reader.Read())
            {
                employee_IDcurr = reader.GetString(0);
            }
            return employee_IDcurr;
        }

        public bool createEmployee(string empID, string fullname, string address, string role, string phone)
        {
            bool enable = true;
            string SQL =
                 "Insert Employee values(@Employee_ID,@Full_Name,@Address,@Role_ID,@Phone_Number,@Enable)";
            SqlParameter EmployeeID = new SqlParameter("@Employee_ID", empID);
            SqlParameter Fullname = new SqlParameter("@Full_Name", fullname);
            SqlParameter Address = new SqlParameter("@Address", address);
            SqlParameter Role = new SqlParameter("@Role_ID", role);
            SqlParameter Phone = new SqlParameter("@Phone_Number", phone);
            SqlParameter Enable = new SqlParameter("@Enable", enable);
            try
            {
                return DataProvider.ExecuteNonQuery(SQL, CommandType.Text, EmployeeID, Fullname, Address, Role, Phone, Enable);
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
        }

        public bool updateAccount(string ID, string fullName, string role, string address, string phone)
        {
            string sql = "Update Employee set Role_ID=@Role, Full_Name = @fullName,Address=@Address,Phone_Number=@Phone where Employee_ID=@EmployeeID";
            SqlParameter RoleID = new SqlParameter("@Role", role);
            SqlParameter fullNameParam = new SqlParameter("@fullName", fullName);
            SqlParameter Address = new SqlParameter("@Address", address);
            SqlParameter Phone = new SqlParameter("@Phone", phone);
            SqlParameter EmpID = new SqlParameter("@EmployeeID", ID);
            try
            {
                return DataProvider.ExecuteNonQuery(sql, CommandType.Text, RoleID, fullNameParam, Address, Phone, EmpID);
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
        }

        public bool deleteAccount(string ID)
        {
            string SQL = "Update Employee  set Enable=@Enable Where Employee_ID=@ID";
            bool delete = false;
            SqlParameter id = new SqlParameter("@ID", ID);
            SqlParameter Enable = new SqlParameter("@Enable", delete);
            try
            {
                return DataProvider.ExecuteNonQuery(SQL, CommandType.Text, id, Enable);
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
        }

        public DataSet searchByUsername(string Keyword)
        {
            string sql = "SELECT * FROM Employee WHERE Full_Name LIKE '%" + Keyword + "%'";
            DataSet dsEmployees = new DataSet();
            try
            {
                dsEmployees = DataProvider.ExecuteQueryWithDataSet(sql, CommandType.Text);
            }
            catch (SqlException se)
            {
                throw new Exception(se.Message);
            }
            return dsEmployees;

        }
    }
}
