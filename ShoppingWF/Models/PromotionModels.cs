﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.Models
{
    class PromotionModels
    {
        public int Promotion_ID { get; set; }
        public Nullable<int> Product_ID { get; set; }
        public string Promotion_Description { get; set; }
        public Nullable<int> Discount_Percent { get; set; }
        public Nullable<System.DateTime> From_Date { get; set; }
        public Nullable<System.DateTime> To_Date { get; set; }
        public Nullable<bool> Enable { get; set; }
    }
}
