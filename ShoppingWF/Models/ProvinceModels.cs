﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.Models
{
    class ProvinceModels
    {
        public int Province_ID { get; set; }
        public string Province_Name { get; set; }

        public static List<ProvinceModels> Provinces { get; set; }
    }
}
