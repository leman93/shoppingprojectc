﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.Models
{
    class TownModels
    {
        public int Town_ID { get; set; }
        public string Town_Name { get; set; }
        public Nullable<int> District_ID { get; set; }

        public static List<TownModels> Towns { get; set; }
    }
}
