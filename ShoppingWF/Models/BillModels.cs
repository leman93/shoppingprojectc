﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.Models
{
    class BillModels
    {
        public int Bill_ID { get; set; }
        public int Order_ID { get; set; }
        public System.DateTime Created_Date { get; set; }
        public float Total_Price { get; set; }
        public string Responsible_Man { get; set; }
        public Nullable<System.DateTime> Delivered_Date { get; set; }
        public Nullable<System.DateTime> Received_Date { get; set; }
        public Nullable<bool> Cancelled_Bill { get; set; }
        public string Reason_Cancel { get; set; }
        public string Shipper_ID { get; set; }
        public string Reciever_Name { get; set; }
        public string Reciever_Phone { get; set; }
        public string Reciever_Addresss { get; set; }
    }
}
