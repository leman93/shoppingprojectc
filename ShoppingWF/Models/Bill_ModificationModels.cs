﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.Models
{
    class Bill_ModificationModels
    {
        public int Modification_ID { get; set; }
        public string Username_Modify { get; set; }
        public Nullable<int> Bill_ID { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }

        public virtual Bill Bill { get; set; }
        public virtual Employee_Account Employee_Account { get; set; }
    }
}
