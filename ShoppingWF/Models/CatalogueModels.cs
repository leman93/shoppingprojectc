﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.Models
{
    class CatalogueModels
    {
        public int Catalogue_ID { get; set; }
        public string Catalogue_Name { get; set; }
    }
}
