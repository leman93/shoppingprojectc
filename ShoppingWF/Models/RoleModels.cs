﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.Models
{
    class RoleModels
    {
        public string Role_ID { get; set; }
        public string Role_Name { get; set; }
        public string Department_ID { get; set; }
        public static List<RoleModels> Roles { get; set; }
    }
}
