﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.Models
{
    class Product_Order_DetailsModels
    {
        public int Product_ID { get; set; }
        public int Order_ID { get; set; }
        public int Order_Quantity { get; set; }
        public float Price { get; set; }
        public Nullable<int> Discount_Percent { get; set; }
        public string Promotion_Description { get; set; }

    }
}
