﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.Models
{
    class Order_ModificationModels
    {
        public int Modification_ID { get; set; }
        public string Username_Modify { get; set; }
        public int Order_ID { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> Modified_Date { get; set; }
    }
}
