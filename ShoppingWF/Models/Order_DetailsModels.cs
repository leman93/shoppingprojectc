﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.Models
{
    class Order_DetailsModels
    {
        public int Order_ID { get; set; }
        public string Email { get; set; }
        public Nullable<System.DateTime> Ordered_Date { get; set; }
        public bool Delivered { get; set; }
        public Nullable<bool> Cancelled_Order { get; set; }
        public string Reason_Cancel { get; set; }
        public string Order_Name { get; set; }
        public string Exact_Address { get; set; }
        public string Order_Phone { get; set; }
        public Nullable<int> Province_ID { get; set; }
        public Nullable<int> District_ID { get; set; }
        public Nullable<int> Town_ID { get; set; }
    }
}
