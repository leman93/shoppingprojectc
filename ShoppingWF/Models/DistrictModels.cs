﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.Models
{
    class DistrictModels
    {
        public int District_ID { get; set; }
        public string District_Name { get; set; }
        public Nullable<int> Province_ID { get; set; }       
        public static List<DistrictModels> Districts { get; set; }
    }
}
