﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.Models
{
    class Product_ModificationModels
    {
        public int Modification_ID { get; set; }
        public int Product_ID { get; set; }
        public string Username { get; set; }
        public string Description { get; set; }
        public System.DateTime Modified_Date { get; set; }

    }
}
