﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.Models
{
    class User_AccountModels
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Full_Name { get; set; }
        public Nullable<System.DateTime> Birthday { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public Nullable<int> Cancel_Amount { get; set; }
        public Nullable<System.DateTime> created_Date { get; set; }
        public string Phone_Number { get; set; }
    }
}
