﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.Models
{
    class DepartmentModels
    {
        public string Department_ID { get; set; }
        public string Department_Name { get; set; }
    }
}
