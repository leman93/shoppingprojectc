﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.Models
{
    class ImageModels
    {
        public int Image_ID { get; set; }
        public Nullable<int> Product_ID { get; set; }
        public string Url { get; set; }        
    }
}
