﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.Models
{
    class ProductModels
    {
        public int Product_ID { get; set; }
        public int Catalogue_ID { get; set; }
        public bool Is_Sale { get; set; }
        public string Product_Name { get; set; }
        public float Price { get; set; }
        public int Level_Trending { get; set; }
        public string Description { get; set; }
        public Nullable<int> Products_Available { get; set; }
        public Nullable<int> Total_Sold { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public string Created_Username { get; set; }
        public string Guarantee_Description { get; set; }
        public string Title_Image { get; set; }
        public Nullable<float> Tax_Percent { get; set; }
        public string Manufacturer { get; set; }
    }
}
