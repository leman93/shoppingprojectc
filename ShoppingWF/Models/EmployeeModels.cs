﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.Models
{
    class EmployeeModels
    {
        public string Employee_ID { get; set; }
        public string Full_Name { get; set; }
        public string Address { get; set; }
        public string Role_ID { get; set; }
        public string Department_ID { get; set; }
        public string Phone_Number { get; set; }
    }
}
