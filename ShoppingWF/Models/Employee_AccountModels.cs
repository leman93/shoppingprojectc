﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.Models
{
    class Employee_AccountModels
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Employee_ID { get; set; }
        public int Level_Access { get; set; }
        public bool Enable { get; set; }
        public Nullable<System.DateTime> Created_Date { get; set; }
        public string Created_By_Username { get; set; }
    }
}
