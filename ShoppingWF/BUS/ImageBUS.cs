﻿
using AutoMapper;
using ShoppingWF.DAO;
using ShoppingWF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.BUS
{
    class ImageBUS
    {
        public static List<ImageModels> getAllImages(int id)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Image, ImageModels>();
            });
            var images = ImageDAO.getAllImages(id);
            var result = Mapper.Map<List<ImageModels>>(images);
            return result;
        }
    }
}
