﻿using AutoMapper;
using ShoppingWF.DAO;
using ShoppingWF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.BUS
{
    class DistrictBUS
    {
        public static List<DistrictModels> getAllDistricts()
        {
            //Mapper.Initialize(x =>
            //{
            //    x.CreateMap<District, DistrictModels>();
            //});
            var districts = DistrictDAO.getAllDistricts();
            List<DistrictModels> result = new List<DistrictModels>();
            foreach (var item in districts)
            {
                //var tmp = Mapper.Map<District, DistrictModels>(item);
                var tmp = new DistrictModels()
                {
                    District_ID = item.District_ID,
                    District_Name = item.District_Name,
                    Province_ID = item.Province_ID,
                };
                result.Add(tmp);
            }
            //var result = Mapper.Map< List <District> ,List <DistrictModels>>(districts);
            return result;
        }

        public static List<DistrictModels> getDistrictsByProvinceId(int provinceId)
        {
            //Mapper.Initialize(x =>
            //{
            //    x.CreateMap<District, DistrictModels>();
            //});
            var districts = DistrictDAO.getDistrictsByProvinceId(provinceId);
            List<DistrictModels> result = new List<DistrictModels>();
            foreach (var item in districts)
            {
                //var tmp = Mapper.Map<District, DistrictModels>(item);
                var tmp = new DistrictModels()
                {
                    District_ID = item.District_ID,
                    District_Name = item.District_Name,
                    Province_ID = item.Province_ID,
                };
                result.Add(tmp);
            }
            return result;
            //return Mapper.Map<List<DistrictModels>>(districts);
        }

        public static DistrictModels getDistrictById(int id)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<District, DistrictModels>();
            });
            var district = DistrictDAO.getDistrictById(id);
            var result = new DistrictModels()
            {
                District_ID = district.District_ID,
                District_Name = district.District_Name,
                Province_ID = district.Province_ID,
            };
            return result;
        }
    }
}
