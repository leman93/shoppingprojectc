﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShoppingWF.Models;
using ShoppingWF.DAO;
using AutoMapper;
namespace ShoppingWF.BUS
{
    class BillBUS
    {
        public static List<BillModels> GetAllBills()
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Bill, BillModels>();
            });
            var listProduct = BillDAO.GetAllBills();
            var result = Mapper.Map<List<BillModels>>(listProduct);
            return result;
        }

        public static BillModels GetBillByOrderId(int id)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Bill, BillModels>();
            });
            if (id > 0)
            {
                var product = BillDAO.GetBillByOrderId(id);
                var result = Mapper.Map<BillModels>(product);
                return result;
            }
            else return null;
        }
        public static BillModels GetBillById(int id)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<BillModels, Bill>();
            });
            if (id > 0)
            {
                var product = BillDAO.GetBillById(id);
                var result = Mapper.Map<BillModels>(product);
                return result;
            }
            else return null;
        }
        public static bool Update(BillModels obj)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<BillModels, Bill>();
            });
            var tmp = Mapper.Map<Bill>(obj);
            var billUpdate = BillDAO.GetBillById(tmp.Bill_ID);
            return BillDAO.Update(billUpdate);
        }

        internal static bool Insert(int cataID, bool isSale, string proName, float price, int level_trending, string description, int proAvai, int totalSole, string guaranteeDesc, string title_image, float tax_percent, string manufacture, string userCreated)
        {
            return ProductDAO.Insert(cataID, isSale, proName, price, level_trending, description, proAvai, totalSole, guaranteeDesc, title_image, tax_percent, manufacture, userCreated);
        }

        public static bool Delete(long objID)
        {
            return BillDAO.Delete(objID);
        }
        public static List<BillModels> Search(int billId, int ordreId, bool canceled, DateTime fromDate, DateTime toDate)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Bill_Filter_Result, BillModels>();
            });
            var listProduct = BillDAO.Search(billId, ordreId, fromDate, toDate, canceled);
            var result = Mapper.Map<List<BillModels>>(listProduct);
            return result;
        }

        internal static bool Insert(BillModels bill)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<BillModels, Bill>();
            });
            var tmp = Mapper.Map<Bill>(bill);
            return BillDAO.Insert(tmp);
        }
    }
}
