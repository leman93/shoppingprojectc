﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ShoppingWF.Models;
using ShoppingWF.DAO;
namespace ShoppingWF.BUS
{
    class Employee_AccountBUS
    {
        public static Employee_AccountModels getEmployeeByUsername(string userName)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Employee_Account, Employee_AccountModels>();
            });
            if (userName != null)
            {
                var employee_Account = Employee_AccountDAO.getEmployeeByUsername(userName);
                var result = Mapper.Map<Employee_AccountModels>(employee_Account);
                return result;
            }
            else return null;
        }
        public static List<Employee_AccountModels> GetAllEmployee_Account()
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Employee_Account, Employee_AccountModels>();
            });
            var employee_Account = Employee_AccountDAO.GetAllEmployee_Account();
            var result = Mapper.Map<List<Employee_AccountModels>>(employee_Account);
            return result;
        }
        public static bool Delete(string username)
        {
            return Employee_AccountDAO.Delete(username);
        }
        public static bool Insert(Employee_AccountModels obj)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Employee_AccountModels, Employee_Account>();
            });
            return Employee_AccountDAO.Insert(Mapper.Map<Employee_Account>(obj));
        }

        internal static bool Update(Employee_AccountModels tmp)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Employee_AccountModels, Employee_Account>();
            });
            return Employee_AccountDAO.Update(Mapper.Map<Employee_Account>(tmp));
        }
    }
}
