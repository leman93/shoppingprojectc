﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShoppingWF.Models;
using ShoppingWF.DAO;
using AutoMapper;
namespace ShoppingWF.BUS
{ 
    class PromotionBUS
    {
        public static List<PromotionModels> GetAllProducts()
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Promotion, PromotionModels>();
            });
            var listProduct = PromotionDAO.GetAllPromotions();
            var result = Mapper.Map<List<PromotionModels>>(listProduct);
            return result;
        }
        public static PromotionModels GetPromotionById(int id)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<PromotionModels, Promotion>();
            });
            if (id > 0)
            {
                var product = PromotionDAO.GetProtionById(id);
                var result = Mapper.Map<PromotionModels>(product);
                return result;
            }
            else return null;
        }
        public static bool Update(PromotionModels obj)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<PromotionModels, Promotion>();
            });
            var productUpdate = PromotionDAO.GetProtionById(obj.Promotion_ID);
            if (productUpdate != null)
            {
                

                return PromotionDAO.Update(productUpdate);
            }
            else
            {
                return false;
            }

        }
        public static bool Delete(int objID)
        {
            return PromotionDAO.Delete(objID);
        }
        public static List<PromotionModels> Search(int promoID, int proId, bool enable, DateTime fromDate, DateTime toDate)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Promotion_Filter_Result, PromotionModels>();
            });
            var listProduct = PromotionDAO.Search(promoID, proId, enable, fromDate, toDate);
            var result = Mapper.Map<List<PromotionModels>>(listProduct);
            return result;
        }

        public static List<PromotionModels> Search(int promoID, int proId, bool enable)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Promotion, PromotionModels>();
            });
            var listProduct = PromotionDAO.Search(promoID, proId, enable);
            var result = Mapper.Map<List<PromotionModels>>(listProduct);
            return result;
        }
    }
}
