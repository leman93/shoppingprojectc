﻿using AutoMapper;
using ShoppingWF.DAO;
using ShoppingWF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.BUS
{
    class RoleBUS
    {
        public static List<RoleModels> getAllRoles()
        {
            Mapper.Initialize(x =>
           {
               x.CreateMap<Role, RoleModels>();
           });
            var listRoles = RoleDAO.getAllRoles();
            return Mapper.Map<List<RoleModels>>(listRoles);
        }


    }
}
