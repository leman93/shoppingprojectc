﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShoppingWF.Models;
using ShoppingWF.DAO;
using AutoMapper;

namespace ShoppingWF.BUS
{
    class Order_DetailBUS
    {
        public static List<Order_DetailsModels> GetAllOrders()
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Order_Details, Order_DetailsModels>();
            });
            var listOrder = Order_DetailDAO.GetAllOrder_DetailsModels();
            var result = Mapper.Map<List<Order_DetailsModels>>(listOrder);
            return result;
        }
        public static Order_DetailsModels GetOrder_DetailsById(int id)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Order_Details, Order_DetailsModels>();
            });
            if (id > 0)
            {
                var product = Order_DetailDAO.GetOrder_DetailsById(id);
                var result = Mapper.Map<Order_DetailsModels>(product);
                return result;
            }
            else return null;
        }
        public static bool Update(Order_DetailsModels obj)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Order_DetailsModels, Order_Details>();
            });
            var tmp = Mapper.Map<Order_Details>(obj);
            return Order_DetailDAO.Update(tmp);


        }
        public static bool Delete(long objID)
        {
            return Order_DetailDAO.Delete(objID);
        }
        public static List<Order_DetailsModels> Search(int ordreID, string email,bool isCanceled, DateTime fromDate, DateTime toDate)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Order_Details_Filter_Result, Order_DetailsModels>();
            });
            var listProduct = Order_DetailDAO.Search(ordreID, email, isCanceled, fromDate, toDate);
            var result = Mapper.Map<List<Order_DetailsModels>>(listProduct);
            return result;
        }
    }
}
