﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ShoppingWF.DAO;
using ShoppingWF.Models;
namespace ShoppingWF.BUS
{
    class Product_Order_DetailsBUS
    {

        public static List<Product_Order_DetailsModels> GetAllProductByOrderID(int id)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Product_Order_Details, Product_Order_DetailsModels>();
            });
            var listProduct = Product_Order_DetailsDAO.GetAllProductByOrderID(id);
            var result = Mapper.Map<List<Product_Order_DetailsModels>>(listProduct);
            return result;
        }
    }
}
