﻿using AutoMapper;
using ShoppingWF.DAO;
using ShoppingWF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.BUS
{
    class TownBUS
    {
        public static List<TownModels> getAllTowns()
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Town, TownModels>();
            });
            var towns = TownDAO.getAllTowns();
            return Mapper.Map<List<TownModels>>(towns);
        }

        public static List<TownModels> getAllTownsByDistrictId(int districtId)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Town, TownModels>();
            });
            var towns = TownDAO.getAllTownsByDistrictId(districtId);
            return Mapper.Map<List<TownModels>>(towns);
        }

        public static TownModels getTownbyId(int id)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Town, TownModels>();
            });
            var town = TownDAO.getTownbyId(id);
            return Mapper.Map<TownModels>(town);
        }
    }
}
