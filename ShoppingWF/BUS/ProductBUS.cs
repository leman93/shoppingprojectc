﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShoppingWF.Models;
using AutoMapper;
using ShoppingWF.DAO;

namespace ShoppingWF.BUS
{
    class ProductBUS
    {
        public static List<ProductModels> GetAllProducts()
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Product, ProductModels>();
            });
            var listProduct = ProductDAO.GetAllProducts();
            var result = Mapper.Map<List<ProductModels>>(listProduct);
            return result;
        }
        public static ProductModels GetProductById(int id)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap< Product, ProductModels>();
            });
            if (id > 0)
            {
                var product = ProductDAO.GetProductById(id);
                var result = Mapper.Map<ProductModels>(product);
                return result;
            }
            else return null;
        }
        public static bool Update(ProductModels obj)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<ProductModels, Product>();
            });
            var productUpdate = ProductDAO.GetProductById(obj.Product_ID);
            if (productUpdate != null)
            {
                productUpdate.Catalogue_ID = obj.Catalogue_ID;
                productUpdate.Is_Sale = obj.Is_Sale;
                productUpdate.Product_Name = obj.Product_Name;
                productUpdate.Price = obj.Price;
                productUpdate.Level_Trending = obj.Level_Trending;
                productUpdate.Description = obj.Description;
                productUpdate.Products_Available = obj.Products_Available;
                productUpdate.Total_Sold = obj.Total_Sold;
                productUpdate.Created_Date = obj.Created_Date;
                productUpdate.Created_Username = obj.Created_Username;
                productUpdate.Guarantee_Description = obj.Guarantee_Description;
                productUpdate.Title_Image = obj.Title_Image;
                productUpdate.Tax_Percent = obj.Tax_Percent;
                productUpdate.Manufacturer = obj.Manufacturer;

                return ProductDAO.Update(productUpdate);
            }
            else
            {
                return false;
            }

        }

        internal static bool Insert(int cataID, bool isSale, string proName, float price, int level_trending, string description, int proAvai, int totalSole, string guaranteeDesc, string title_image, float tax_percent, string manufacture, string userCreated)
        {
            return ProductDAO.Insert(cataID, isSale, proName, price, level_trending, description, proAvai, totalSole, guaranteeDesc, title_image, tax_percent, manufacture , userCreated);
        }

        public static bool Delete(int objID)
        {
            return ProductDAO.Delete(objID);
        }
        public static List<ProductModels> Search(int proID, int cataId, string proName, DateTime fromDate, DateTime toDate)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Product_Filter_Result,ProductModels>();
            });
            var listProduct = ProductDAO.Search(proID, cataId, proName, fromDate, toDate);
            var result = Mapper.Map<List<ProductModels>>(listProduct);
            return result;
        }
        public static List<ProductModels> Search(int proID, int cataId, string proName)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Product, ProductModels>();
            });
            var listProduct = ProductDAO.Search(proID, cataId, proName);
            var result = Mapper.Map<List<ProductModels>>(listProduct);
            return result;
        }

    }
}
