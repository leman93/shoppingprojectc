﻿using AutoMapper;
using ShoppingWF.DAO;
using ShoppingWF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.BUS
{
    class ProvinceBUS
    {

        public static List<ProvinceModels> getAllProvinces()
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Province, ProvinceModels>();
            });
            var provinces = ProvinceDAO.getAllProvinces();
            var result = Mapper.Map<List<ProvinceModels>>(provinces);
            return result;
        }

        public static ProvinceModels getProvinceById(int id)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<Province, ProvinceModels>();
            });
            var province = ProvinceDAO.getProvinceById(id);
            var result = Mapper.Map<ProvinceModels>(province);
            return result;
        }
    }
}
