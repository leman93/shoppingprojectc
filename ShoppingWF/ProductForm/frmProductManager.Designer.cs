﻿namespace ShoppingWF
{
    partial class frmProductManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblUsername = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.dtimeCreate = new System.Windows.Forms.DateTimePicker();
            this.txtProductID = new System.Windows.Forms.TextBox();
            this.txtCatalougeID = new System.Windows.Forms.TextBox();
            this.txtProductName = new System.Windows.Forms.TextBox();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.txtLevelTrending = new System.Windows.Forms.TextBox();
            this.txtProductAvai = new System.Windows.Forms.TextBox();
            this.rtxtGuaranteeDesc = new System.Windows.Forms.RichTextBox();
            this.txtTotalSold = new System.Windows.Forms.TextBox();
            this.rtbSale = new System.Windows.Forms.RadioButton();
            this.rtxtDescription = new System.Windows.Forms.RichTextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.dgvProduct = new System.Windows.Forms.DataGridView();
            this.txtTaxPercent = new System.Windows.Forms.TextBox();
            this.txtManufacturer = new System.Windows.Forms.TextBox();
            this.txtCreatedUser = new System.Windows.Forms.TextBox();
            this.txtTitleImage = new System.Windows.Forms.TextBox();
            this.btnCreateProduct = new System.Windows.Forms.Button();
            this.btnGetAllProducts = new System.Windows.Forms.Button();
            this.btnUpdateProduct = new System.Windows.Forms.Button();
            this.btnDeleteProduct = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnExportToExcel = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.dtimeTo = new System.Windows.Forms.DateTimePicker();
            this.dtimeFrom = new System.Windows.Forms.DateTimePicker();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtSearchProName = new System.Windows.Forms.TextBox();
            this.txtSearchCataID = new System.Windows.Forms.TextBox();
            this.txtSearchProID = new System.Windows.Forms.TextBox();
            this.labelname = new System.Windows.Forms.Label();
            this.labelcata = new System.Windows.Forms.Label();
            this.labelpro = new System.Windows.Forms.Label();
            this.btnPromoteProduct = new System.Windows.Forms.Button();
            this.btnManageImages = new System.Windows.Forms.Button();
            this.chkAllowDate = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduct)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Font = new System.Drawing.Font("Yu Mincho", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lblUsername.Location = new System.Drawing.Point(28, 15);
            this.lblUsername.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(85, 35);
            this.lblUsername.TabIndex = 0;
            this.lblUsername.Text = "label1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(79, 101);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Product ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(66, 134);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Catalouge ID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(100, 174);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "Is Sale";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(56, 211);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 17);
            this.label4.TabIndex = 1;
            this.label4.Text = "Product_Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(110, 241);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 17);
            this.label5.TabIndex = 1;
            this.label5.Text = "Price";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(53, 275);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 17);
            this.label6.TabIndex = 1;
            this.label6.Text = "Level_Trending";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(74, 342);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 17);
            this.label7.TabIndex = 1;
            this.label7.Text = "Description";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(501, 102);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(125, 17);
            this.label8.TabIndex = 1;
            this.label8.Text = "Products Available";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(535, 136);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 17);
            this.label9.TabIndex = 1;
            this.label9.Text = "Total Sold";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(524, 176);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(92, 17);
            this.label10.TabIndex = 1;
            this.label10.Text = "Created Date";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(535, 344);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(77, 17);
            this.label11.TabIndex = 1;
            this.label11.Text = "Title Image";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(497, 211);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(127, 17);
            this.label12.TabIndex = 1;
            this.label12.Text = "Created Username";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(66, 439);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 17);
            this.label13.TabIndex = 1;
            this.label13.Text = "Tax Percent";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(479, 276);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(151, 17);
            this.label14.TabIndex = 1;
            this.label14.Text = "Guarantee Description";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(526, 439);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(92, 17);
            this.label15.TabIndex = 1;
            this.label15.Text = "Manufacturer";
            // 
            // dtimeCreate
            // 
            this.dtimeCreate.Location = new System.Drawing.Point(652, 173);
            this.dtimeCreate.Margin = new System.Windows.Forms.Padding(2);
            this.dtimeCreate.Name = "dtimeCreate";
            this.dtimeCreate.Size = new System.Drawing.Size(188, 20);
            this.dtimeCreate.TabIndex = 2;
            this.dtimeCreate.Value = new System.DateTime(2017, 3, 7, 22, 32, 5, 0);
            // 
            // txtProductID
            // 
            this.txtProductID.Location = new System.Drawing.Point(180, 101);
            this.txtProductID.Margin = new System.Windows.Forms.Padding(2);
            this.txtProductID.Name = "txtProductID";
            this.txtProductID.ReadOnly = true;
            this.txtProductID.Size = new System.Drawing.Size(66, 20);
            this.txtProductID.TabIndex = 3;
            // 
            // txtCatalougeID
            // 
            this.txtCatalougeID.Location = new System.Drawing.Point(180, 136);
            this.txtCatalougeID.Margin = new System.Windows.Forms.Padding(2);
            this.txtCatalougeID.Name = "txtCatalougeID";
            this.txtCatalougeID.Size = new System.Drawing.Size(66, 20);
            this.txtCatalougeID.TabIndex = 3;
            // 
            // txtProductName
            // 
            this.txtProductName.Location = new System.Drawing.Point(180, 211);
            this.txtProductName.Margin = new System.Windows.Forms.Padding(2);
            this.txtProductName.Name = "txtProductName";
            this.txtProductName.Size = new System.Drawing.Size(188, 20);
            this.txtProductName.TabIndex = 3;
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(180, 241);
            this.txtPrice.Margin = new System.Windows.Forms.Padding(2);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(66, 20);
            this.txtPrice.TabIndex = 3;
            // 
            // txtLevelTrending
            // 
            this.txtLevelTrending.Location = new System.Drawing.Point(180, 276);
            this.txtLevelTrending.Margin = new System.Windows.Forms.Padding(2);
            this.txtLevelTrending.Name = "txtLevelTrending";
            this.txtLevelTrending.Size = new System.Drawing.Size(66, 20);
            this.txtLevelTrending.TabIndex = 3;
            // 
            // txtProductAvai
            // 
            this.txtProductAvai.Location = new System.Drawing.Point(652, 100);
            this.txtProductAvai.Margin = new System.Windows.Forms.Padding(2);
            this.txtProductAvai.Name = "txtProductAvai";
            this.txtProductAvai.Size = new System.Drawing.Size(66, 20);
            this.txtProductAvai.TabIndex = 3;
            // 
            // rtxtGuaranteeDesc
            // 
            this.rtxtGuaranteeDesc.Location = new System.Drawing.Point(652, 243);
            this.rtxtGuaranteeDesc.Margin = new System.Windows.Forms.Padding(2);
            this.rtxtGuaranteeDesc.Name = "rtxtGuaranteeDesc";
            this.rtxtGuaranteeDesc.Size = new System.Drawing.Size(188, 79);
            this.rtxtGuaranteeDesc.TabIndex = 4;
            this.rtxtGuaranteeDesc.Text = "";
            // 
            // txtTotalSold
            // 
            this.txtTotalSold.Location = new System.Drawing.Point(652, 133);
            this.txtTotalSold.Margin = new System.Windows.Forms.Padding(2);
            this.txtTotalSold.Name = "txtTotalSold";
            this.txtTotalSold.Size = new System.Drawing.Size(66, 20);
            this.txtTotalSold.TabIndex = 3;
            // 
            // rtbSale
            // 
            this.rtbSale.AutoCheck = false;
            this.rtbSale.AutoSize = true;
            this.rtbSale.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.rtbSale.Location = new System.Drawing.Point(180, 176);
            this.rtbSale.Margin = new System.Windows.Forms.Padding(2);
            this.rtbSale.Name = "rtbSale";
            this.rtbSale.Size = new System.Drawing.Size(14, 13);
            this.rtbSale.TabIndex = 5;
            this.rtbSale.UseVisualStyleBackColor = true;
            this.rtbSale.Click += new System.EventHandler(this.rtbSale_Click);
            // 
            // rtxtDescription
            // 
            this.rtxtDescription.Location = new System.Drawing.Point(180, 314);
            this.rtxtDescription.Margin = new System.Windows.Forms.Padding(2);
            this.rtxtDescription.Name = "rtxtDescription";
            this.rtxtDescription.Size = new System.Drawing.Size(188, 79);
            this.rtxtDescription.TabIndex = 4;
            this.rtxtDescription.Text = "";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Yu Mincho", 28.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label16.Location = new System.Drawing.Point(484, 32);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(298, 49);
            this.label16.TabIndex = 0;
            this.label16.Text = "Manage Product";
            // 
            // dgvProduct
            // 
            this.dgvProduct.AllowUserToAddRows = false;
            this.dgvProduct.AllowUserToDeleteRows = false;
            this.dgvProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProduct.Location = new System.Drawing.Point(137, 497);
            this.dgvProduct.Margin = new System.Windows.Forms.Padding(2);
            this.dgvProduct.Name = "dgvProduct";
            this.dgvProduct.ReadOnly = true;
            this.dgvProduct.RowTemplate.Height = 24;
            this.dgvProduct.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProduct.Size = new System.Drawing.Size(1165, 267);
            this.dgvProduct.TabIndex = 6;
            // 
            // txtTaxPercent
            // 
            this.txtTaxPercent.Location = new System.Drawing.Point(180, 439);
            this.txtTaxPercent.Margin = new System.Windows.Forms.Padding(2);
            this.txtTaxPercent.Name = "txtTaxPercent";
            this.txtTaxPercent.Size = new System.Drawing.Size(66, 20);
            this.txtTaxPercent.TabIndex = 3;
            // 
            // txtManufacturer
            // 
            this.txtManufacturer.Location = new System.Drawing.Point(652, 438);
            this.txtManufacturer.Margin = new System.Windows.Forms.Padding(2);
            this.txtManufacturer.Name = "txtManufacturer";
            this.txtManufacturer.Size = new System.Drawing.Size(188, 20);
            this.txtManufacturer.TabIndex = 3;
            // 
            // txtCreatedUser
            // 
            this.txtCreatedUser.Location = new System.Drawing.Point(652, 211);
            this.txtCreatedUser.Margin = new System.Windows.Forms.Padding(2);
            this.txtCreatedUser.Name = "txtCreatedUser";
            this.txtCreatedUser.ReadOnly = true;
            this.txtCreatedUser.Size = new System.Drawing.Size(188, 20);
            this.txtCreatedUser.TabIndex = 3;
            this.txtCreatedUser.TextChanged += new System.EventHandler(this.txtCreatedUser_TextChanged);
            // 
            // txtTitleImage
            // 
            this.txtTitleImage.Location = new System.Drawing.Point(652, 345);
            this.txtTitleImage.Margin = new System.Windows.Forms.Padding(2);
            this.txtTitleImage.Name = "txtTitleImage";
            this.txtTitleImage.Size = new System.Drawing.Size(188, 20);
            this.txtTitleImage.TabIndex = 3;
            this.txtTitleImage.TextChanged += new System.EventHandler(this.txtCreatedUser_TextChanged);
            // 
            // btnCreateProduct
            // 
            this.btnCreateProduct.Location = new System.Drawing.Point(912, 173);
            this.btnCreateProduct.Margin = new System.Windows.Forms.Padding(2);
            this.btnCreateProduct.Name = "btnCreateProduct";
            this.btnCreateProduct.Size = new System.Drawing.Size(124, 51);
            this.btnCreateProduct.TabIndex = 7;
            this.btnCreateProduct.Text = "Create new Product";
            this.btnCreateProduct.UseVisualStyleBackColor = true;
            this.btnCreateProduct.Click += new System.EventHandler(this.btnCreateProduct_Click);
            // 
            // btnGetAllProducts
            // 
            this.btnGetAllProducts.Location = new System.Drawing.Point(912, 99);
            this.btnGetAllProducts.Margin = new System.Windows.Forms.Padding(2);
            this.btnGetAllProducts.Name = "btnGetAllProducts";
            this.btnGetAllProducts.Size = new System.Drawing.Size(124, 51);
            this.btnGetAllProducts.TabIndex = 7;
            this.btnGetAllProducts.Text = "Get All Products";
            this.btnGetAllProducts.UseVisualStyleBackColor = true;
            this.btnGetAllProducts.Click += new System.EventHandler(this.btnGetAllProducts_Click);
            // 
            // btnUpdateProduct
            // 
            this.btnUpdateProduct.Location = new System.Drawing.Point(1069, 102);
            this.btnUpdateProduct.Margin = new System.Windows.Forms.Padding(2);
            this.btnUpdateProduct.Name = "btnUpdateProduct";
            this.btnUpdateProduct.Size = new System.Drawing.Size(124, 51);
            this.btnUpdateProduct.TabIndex = 7;
            this.btnUpdateProduct.Text = "Update Product";
            this.btnUpdateProduct.UseVisualStyleBackColor = true;
            this.btnUpdateProduct.Click += new System.EventHandler(this.btnUpdateProduct_Click);
            // 
            // btnDeleteProduct
            // 
            this.btnDeleteProduct.Location = new System.Drawing.Point(1069, 173);
            this.btnDeleteProduct.Margin = new System.Windows.Forms.Padding(2);
            this.btnDeleteProduct.Name = "btnDeleteProduct";
            this.btnDeleteProduct.Size = new System.Drawing.Size(124, 51);
            this.btnDeleteProduct.TabIndex = 7;
            this.btnDeleteProduct.Text = "Delete Product";
            this.btnDeleteProduct.UseVisualStyleBackColor = true;
            this.btnDeleteProduct.Click += new System.EventHandler(this.btnDeleteProduct_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chkAllowDate);
            this.panel1.Controls.Add(this.btnExportToExcel);
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.dtimeTo);
            this.panel1.Controls.Add(this.dtimeFrom);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.txtSearchProName);
            this.panel1.Controls.Add(this.txtSearchCataID);
            this.panel1.Controls.Add(this.txtSearchProID);
            this.panel1.Controls.Add(this.labelname);
            this.panel1.Controls.Add(this.labelcata);
            this.panel1.Controls.Add(this.labelpro);
            this.panel1.Location = new System.Drawing.Point(885, 260);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(466, 208);
            this.panel1.TabIndex = 8;
            // 
            // btnExportToExcel
            // 
            this.btnExportToExcel.Location = new System.Drawing.Point(320, 166);
            this.btnExportToExcel.Margin = new System.Windows.Forms.Padding(2);
            this.btnExportToExcel.Name = "btnExportToExcel";
            this.btnExportToExcel.Size = new System.Drawing.Size(124, 30);
            this.btnExportToExcel.TabIndex = 13;
            this.btnExportToExcel.Text = "Export to Excel";
            this.btnExportToExcel.UseVisualStyleBackColor = true;
            this.btnExportToExcel.Click += new System.EventHandler(this.btnExportToExcel_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(157, 166);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(124, 30);
            this.btnSearch.TabIndex = 12;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // dtimeTo
            // 
            this.dtimeTo.Location = new System.Drawing.Point(262, 130);
            this.dtimeTo.Margin = new System.Windows.Forms.Padding(2);
            this.dtimeTo.Name = "dtimeTo";
            this.dtimeTo.Size = new System.Drawing.Size(182, 20);
            this.dtimeTo.TabIndex = 11;
            this.dtimeTo.Value = new System.DateTime(2017, 3, 7, 22, 32, 5, 0);
            // 
            // dtimeFrom
            // 
            this.dtimeFrom.Location = new System.Drawing.Point(58, 130);
            this.dtimeFrom.Margin = new System.Windows.Forms.Padding(2);
            this.dtimeFrom.Name = "dtimeFrom";
            this.dtimeFrom.Size = new System.Drawing.Size(171, 20);
            this.dtimeFrom.TabIndex = 10;
            this.dtimeFrom.Value = new System.DateTime(2017, 3, 7, 22, 32, 5, 0);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(233, 133);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(25, 17);
            this.label18.TabIndex = 9;
            this.label18.Text = "To";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(2, 130);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(40, 17);
            this.label17.TabIndex = 8;
            this.label17.Text = "From";
            // 
            // txtSearchProName
            // 
            this.txtSearchProName.Location = new System.Drawing.Point(191, 91);
            this.txtSearchProName.Margin = new System.Windows.Forms.Padding(2);
            this.txtSearchProName.Name = "txtSearchProName";
            this.txtSearchProName.Size = new System.Drawing.Size(221, 20);
            this.txtSearchProName.TabIndex = 7;
            // 
            // txtSearchCataID
            // 
            this.txtSearchCataID.Location = new System.Drawing.Point(191, 68);
            this.txtSearchCataID.Margin = new System.Windows.Forms.Padding(2);
            this.txtSearchCataID.Name = "txtSearchCataID";
            this.txtSearchCataID.Size = new System.Drawing.Size(221, 20);
            this.txtSearchCataID.TabIndex = 6;
            // 
            // txtSearchProID
            // 
            this.txtSearchProID.Location = new System.Drawing.Point(191, 34);
            this.txtSearchProID.Margin = new System.Windows.Forms.Padding(2);
            this.txtSearchProID.Name = "txtSearchProID";
            this.txtSearchProID.Size = new System.Drawing.Size(221, 20);
            this.txtSearchProID.TabIndex = 5;
            // 
            // labelname
            // 
            this.labelname.AutoSize = true;
            this.labelname.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelname.Location = new System.Drawing.Point(53, 96);
            this.labelname.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelname.Name = "labelname";
            this.labelname.Size = new System.Drawing.Size(98, 17);
            this.labelname.TabIndex = 4;
            this.labelname.Text = "Product Name";
            // 
            // labelcata
            // 
            this.labelcata.AutoSize = true;
            this.labelcata.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelcata.Location = new System.Drawing.Point(62, 71);
            this.labelcata.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelcata.Name = "labelcata";
            this.labelcata.Size = new System.Drawing.Size(89, 17);
            this.labelcata.TabIndex = 3;
            this.labelcata.Text = "Catalouge ID";
            // 
            // labelpro
            // 
            this.labelpro.AutoSize = true;
            this.labelpro.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelpro.Location = new System.Drawing.Point(73, 37);
            this.labelpro.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelpro.Name = "labelpro";
            this.labelpro.Size = new System.Drawing.Size(74, 17);
            this.labelpro.TabIndex = 2;
            this.labelpro.Text = "Product ID";
            // 
            // btnPromoteProduct
            // 
            this.btnPromoteProduct.Location = new System.Drawing.Point(1210, 102);
            this.btnPromoteProduct.Margin = new System.Windows.Forms.Padding(2);
            this.btnPromoteProduct.Name = "btnPromoteProduct";
            this.btnPromoteProduct.Size = new System.Drawing.Size(124, 51);
            this.btnPromoteProduct.TabIndex = 10;
            this.btnPromoteProduct.Text = "Manage Promotion";
            this.btnPromoteProduct.UseVisualStyleBackColor = true;
            this.btnPromoteProduct.Click += new System.EventHandler(this.btnPromoteProduct_Click);
            // 
            // btnManageImages
            // 
            this.btnManageImages.Location = new System.Drawing.Point(1210, 173);
            this.btnManageImages.Name = "btnManageImages";
            this.btnManageImages.Size = new System.Drawing.Size(124, 51);
            this.btnManageImages.TabIndex = 11;
            this.btnManageImages.Text = "Manage Images";
            this.btnManageImages.UseVisualStyleBackColor = true;
            this.btnManageImages.Click += new System.EventHandler(this.btnManageImages_Click);
            // 
            // chkAllowDate
            // 
            this.chkAllowDate.AutoSize = true;
            this.chkAllowDate.Location = new System.Drawing.Point(15, 174);
            this.chkAllowDate.Name = "chkAllowDate";
            this.chkAllowDate.Size = new System.Drawing.Size(114, 17);
            this.chkAllowDate.TabIndex = 14;
            this.chkAllowDate.Text = "Allow Search Date";
            this.chkAllowDate.UseVisualStyleBackColor = true;
            // 
            // frmProductManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1362, 602);
            this.Controls.Add(this.btnManageImages);
            this.Controls.Add(this.btnPromoteProduct);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnDeleteProduct);
            this.Controls.Add(this.btnUpdateProduct);
            this.Controls.Add(this.btnGetAllProducts);
            this.Controls.Add(this.btnCreateProduct);
            this.Controls.Add(this.dgvProduct);
            this.Controls.Add(this.rtbSale);
            this.Controls.Add(this.rtxtDescription);
            this.Controls.Add(this.rtxtGuaranteeDesc);
            this.Controls.Add(this.txtTitleImage);
            this.Controls.Add(this.txtCreatedUser);
            this.Controls.Add(this.txtTotalSold);
            this.Controls.Add(this.txtProductAvai);
            this.Controls.Add(this.txtLevelTrending);
            this.Controls.Add(this.txtManufacturer);
            this.Controls.Add(this.txtTaxPercent);
            this.Controls.Add(this.txtPrice);
            this.Controls.Add(this.txtProductName);
            this.Controls.Add(this.txtCatalougeID);
            this.Controls.Add(this.txtProductID);
            this.Controls.Add(this.dtimeCreate);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.lblUsername);
            this.ForeColor = System.Drawing.Color.Black;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmProductManager";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmProductManager";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmProductManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduct)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DateTimePicker dtimeCreate;
        private System.Windows.Forms.TextBox txtProductID;
        private System.Windows.Forms.TextBox txtCatalougeID;
        private System.Windows.Forms.TextBox txtProductName;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.TextBox txtLevelTrending;
        private System.Windows.Forms.TextBox txtProductAvai;
        private System.Windows.Forms.RichTextBox rtxtGuaranteeDesc;
        private System.Windows.Forms.TextBox txtTotalSold;
        private System.Windows.Forms.RadioButton rtbSale;
        private System.Windows.Forms.RichTextBox rtxtDescription;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DataGridView dgvProduct;
        private System.Windows.Forms.TextBox txtTaxPercent;
        private System.Windows.Forms.TextBox txtManufacturer;
        private System.Windows.Forms.TextBox txtCreatedUser;
        private System.Windows.Forms.TextBox txtTitleImage;
        private System.Windows.Forms.Button btnCreateProduct;
        private System.Windows.Forms.Button btnGetAllProducts;
        private System.Windows.Forms.Button btnUpdateProduct;
        private System.Windows.Forms.Button btnDeleteProduct;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DateTimePicker dtimeTo;
        private System.Windows.Forms.DateTimePicker dtimeFrom;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtSearchProName;
        private System.Windows.Forms.TextBox txtSearchCataID;
        private System.Windows.Forms.TextBox txtSearchProID;
        private System.Windows.Forms.Label labelname;
        private System.Windows.Forms.Label labelcata;
        private System.Windows.Forms.Label labelpro;
        private System.Windows.Forms.Button btnPromoteProduct;
        private System.Windows.Forms.Button btnManageImages;
        private System.Windows.Forms.Button btnExportToExcel;
        private System.Windows.Forms.CheckBox chkAllowDate;
    }
}