﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ShoppingWF.Models;
using ShoppingWF.BUS;

namespace ShoppingWF
{
    public partial class frmCreateProduct : Form
    {
        string username;
        public frmCreateProduct(string username)
        {
            InitializeComponent();
            this.username = username;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            int cataID;
            if (Int32.TryParse(txtCatalougeID.Text, out cataID) == false)
            {
                MessageBox.Show("Catalouge Id must be integer.");
                return;
            }else if(cataID <= 0)
            {
                MessageBox.Show("Catalouge Id must be greater than zero.");
                return;
            }

            if(txtProductName.Text.Length == 0)
            {
                MessageBox.Show("Product Name is required.");
                return;
            }

            int level_trending ;
            if (Int32.TryParse(txtLevelTrending.Text, out level_trending) == false)
            {
                MessageBox.Show("level_trending must be integer.");
                return;
            }else if(level_trending <= 0)
            {
                MessageBox.Show("level_trending must be greater than zero.");
                return;
            }

            float price;
            if (float.TryParse(txtPrice.Text, out price) == false)
            {
                MessageBox.Show("Price must be float number.");
                return;
            }else if(price <= 0)
            {
                MessageBox.Show("Price Id must be greater than zero.");
                return;
            }

            float tax_percent;
            if (float.TryParse(txtTaxPercent.Text, out tax_percent) == false)
            {
                MessageBox.Show("Tax percent must be float.");
                return;
            }else  if(tax_percent <= 0)
            {
                MessageBox.Show("Tax percent must be greater than zero.");
                return;
            }

            int totalSole;
            if (Int32.TryParse(txtTotalSold.Text, out totalSole) == false)
            {
                MessageBox.Show("Total Sold  must be integer.");
                return;
            }else if(totalSole < 0)
            {
                MessageBox.Show("Total Sold  must be greater than zero.");
                return;
            }

            int proAvai;
            if (Int32.TryParse(txtProductAvai.Text, out proAvai) == false)
            {
                MessageBox.Show("Product Available must be integer.");
                return;
            }else if(proAvai < 0)
            {
                MessageBox.Show("Product Available must be greater than zero.");
                return;
            }

            bool result = ProductBUS.Insert(cataID, rtbSale.Checked, txtProductName.Text, price, level_trending, rtxtDescription.Text, proAvai, totalSole, rtxtGuaranteeDesc.Text, txtTitleImage.Text, tax_percent, txtManufacturer.Text, this.username);
            if (result)
            {
                MessageBox.Show("Add successfuly!!");
                this.Close();
            }
            else
            {
                MessageBox.Show("Add failed.Try again!");
            }
        }

        private void rtbSale_Click(object sender, EventArgs e)
        {
            if(rtbSale.Checked == true)
            {
                rtbSale.Checked = false;
                return;
            }
            if (rtbSale.Checked == false)
            {
                rtbSale.Checked = true;
            }
        }
    }
}
