﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ShoppingWF.Models;
using ShoppingWF.BUS;
using ShoppingWF.DAO;

namespace ShoppingWF.ProductForm
{
    public partial class frmPromotion : Form
    {
        private int productId;
        string username;
        DataTable dtPromotion = null;
        public frmPromotion(string username)
        {
            InitializeComponent();
            this.username = username;
            lblUsername.Text = "Welcome, " + username;
            dtimeSFrom.Value = DateTime.Now.AddDays(-30);
            dtimeSTo.Value = DateTime.Now;
        }

        public frmPromotion(string username, int productId)
        {
            InitializeComponent();
            this.productId = productId;
            this.username = username;
            lblUsername.Text = "Welcome, " + username;
            dtimeSFrom.Value = DateTime.Now.AddDays(-30);
            dtimeSTo.Value = DateTime.Now;
            this.WindowState = FormWindowState.Maximized;
            txtSearchProId.Text = productId.ToString();
            btnSearch_Click(null, null);
        }

        private void frmPromotion_Load(object sender, EventArgs e)
        {
            frmProductManager proForm = (frmProductManager)(this.Owner);
        }

        private void btnGetAllPromotion_Click(object sender, EventArgs e)
        {
            var listProduct = new List<PromotionModels>(PromotionBUS.GetAllProducts());
            //add to tabledata
            dtPromotion = new DataTable();

            dtPromotion.Columns.Add("Promotion_ID");
            dtPromotion.Columns.Add("Product_ID");
            dtPromotion.Columns.Add("Promotion_Description");
            dtPromotion.Columns.Add("Discount_Percent");
            dtPromotion.Columns.Add("From_Date");
            dtPromotion.Columns.Add("To_Date");
            dtPromotion.Columns.Add("Enable");
            
            
            foreach (var item in listProduct)
            {
                DataRow row = dtPromotion.NewRow();
                row["Promotion_ID"] = item.Promotion_ID;
                row["Product_ID"] = item.Product_ID;
                row["Promotion_Description"] = item.Promotion_Description;
                row["Discount_Percent"] = item.Discount_Percent;
                row["From_Date"] = item.From_Date;
                row["To_Date"] = item.To_Date;
                row["Enable"] = item.Enable;
                
                dtPromotion.Rows.Add(row);
            }
            dtPromotion.PrimaryKey = new DataColumn[] { dtPromotion.Columns[0] };

            //clear binding all field
            txtPromotionID.DataBindings.Clear();
            txtProductId.DataBindings.Clear();
            txtPromotionDesc.DataBindings.Clear();
            txtDisPercent.DataBindings.Clear();
            dtimeFromDate.DataBindings.Clear();
            dtimeToDate.DataBindings.Clear();
            chkEnable.DataBindings.Clear();



            //add binding
            txtPromotionID.DataBindings.Add("Text", dtPromotion, "Promotion_ID");
            txtProductId.DataBindings.Add("Text", dtPromotion, "Product_ID");
            txtPromotionDesc.DataBindings.Add("Text", dtPromotion, "Promotion_Description");
            txtDisPercent.DataBindings.Add("Text", dtPromotion, "Discount_Percent");
            dtimeFromDate.DataBindings.Add("Text", dtPromotion, "From_Date");
            dtimeToDate.DataBindings.Add("Text", dtPromotion, "To_Date");
            chkEnable.DataBindings.Add("Checked", dtPromotion, "Enable");

            dgvPromotion.DataSource = dtPromotion;

        }
        

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            
            int promoID = 0;
            Int32.TryParse(txtSearchPromoId.Text, out promoID);
            int proId = 0;
            Int32.TryParse(txtSearchProId.Text, out proId);
            DateTime fromDate = dtimeSFrom.Value;
            DateTime toDate = dtimeSTo.Value;

            List<PromotionModels> listPromo;
            if (chkDateAllow.Checked)
            {
                listPromo = PromotionBUS.Search(promoID, proId, chkSearchEnable.Checked, fromDate, toDate);
            }
            else
            {
                listPromo = PromotionBUS.Search(promoID, proId, chkSearchEnable.Checked);
            }

            dtPromotion = new DataTable();

            dtPromotion.Columns.Add("Promotion_ID");
            dtPromotion.Columns.Add("Product_ID");
            dtPromotion.Columns.Add("Promotion_Description");
            dtPromotion.Columns.Add("Discount_Percent");
            dtPromotion.Columns.Add("From_Date");
            dtPromotion.Columns.Add("To_Date");
            dtPromotion.Columns.Add("Enable");


            foreach (var item in listPromo)
            {
                DataRow row = dtPromotion.NewRow();
                row["Promotion_ID"] = item.Promotion_ID;
                row["Product_ID"] = item.Product_ID;
                row["Promotion_Description"] = item.Promotion_Description;
                row["Discount_Percent"] = item.Discount_Percent;
                row["From_Date"] = item.From_Date;
                row["To_Date"] = item.To_Date;
                row["Enable"] = item.Enable;

                dtPromotion.Rows.Add(row);
            }
            dtPromotion.PrimaryKey = new DataColumn[] { dtPromotion.Columns[0] };

            //clear binding all field
            txtPromotionID.DataBindings.Clear();
            txtProductId.DataBindings.Clear();
            txtPromotionDesc.DataBindings.Clear();
            txtDisPercent.DataBindings.Clear();
            dtimeFromDate.DataBindings.Clear();
            dtimeToDate.DataBindings.Clear();
            chkEnable.DataBindings.Clear();



            //add binding
            txtPromotionID.DataBindings.Add("Text", dtPromotion, "Promotion_ID");
            txtProductId.DataBindings.Add("Text", dtPromotion, "Product_ID");
            txtPromotionDesc.DataBindings.Add("Text", dtPromotion, "Promotion_Description");
            txtDisPercent.DataBindings.Add("Text", dtPromotion, "Discount_Percent");
            dtimeFromDate.DataBindings.Add("Text", dtPromotion, "From_Date");
            dtimeToDate.DataBindings.Add("Text", dtPromotion, "To_Date");
            chkEnable.DataBindings.Add("Checked", dtPromotion, "Enable");

            dgvPromotion.DataSource = dtPromotion;
        }
        

        private void btnBackToProductForm_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private bool isValidData()
        {
            string description = txtPromotionDesc.Text;
            if (string.IsNullOrWhiteSpace(description))
            {
                MessageBox.Show("Description can not is empty");
                return false;
            }
            int discount;
            if (string.IsNullOrWhiteSpace(txtDisPercent.Text))
            {
                MessageBox.Show("Discount Percent can not is empty");
                return false;
            }
            if (!int.TryParse(txtDisPercent.Text.Trim(), out discount))
            {
                MessageBox.Show("Discount percent must is an integer");
                return false;
            }
            else if (discount < 0)
            {
                MessageBox.Show("Discount percent must be greater than zero");
                return false;
            }

            var fromDate = dtimeFromDate.Value;
            var toDate = dtimeToDate.Value;
            if (fromDate > toDate)
            {
                MessageBox.Show("ToDate must is later than FromDate");
                return false;
            }
            return true;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            int promotionId = int.Parse(txtPromotionID.Text.Trim());
            int productId = int.Parse(txtProductId.Text.Trim());
            int discountPercent = int.Parse(txtDisPercent.Text.Trim());
            Promotion promotion = new Promotion()
            {
                Promotion_ID = promotionId,
                Product_ID = productId,
                Promotion_Description = txtPromotionDesc.Text.Trim(),
                Discount_Percent = discountPercent,
                From_Date = dtimeFromDate.Value,
                To_Date = dtimeToDate.Value,
                Enable = chkEnable.Checked,
            };
            bool result = PromotionDAO.Update(promotion);
            if (result)
            {
                MessageBox.Show("Promotion update success");
                btnGetAllPromotion_Click(null, null);
            }
            else
            {
                MessageBox.Show("Promotion update fail");
            }
        }

        private void btnAddPromotion_Click(object sender, EventArgs e)
        {
            this.Opacity = 0;
            FrmCreatePromotion frmPromotion = new FrmCreatePromotion(productId);
            frmPromotion.ShowDialog(this);
            this.Opacity = 1;
            btnSearch_Click(null, null);
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            int promotionId;
            if(!int.TryParse(txtPromotionID.Text, out promotionId))
            {
                MessageBox.Show("Promotion remove fail");
                return;
            }

            bool result = PromotionDAO.Delete(promotionId);
            if (result)
            {
                MessageBox.Show("Promotion Remove success");
                btnGetAllPromotion_Click(null, null);
            }
            else
            {
                MessageBox.Show("Promotion remove fail");
            }
        }
    }
}
