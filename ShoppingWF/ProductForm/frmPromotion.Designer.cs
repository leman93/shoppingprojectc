﻿namespace ShoppingWF.ProductForm
{
    partial class frmPromotion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label16 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtPromotionID = new System.Windows.Forms.TextBox();
            this.txtProductId = new System.Windows.Forms.TextBox();
            this.txtDisPercent = new System.Windows.Forms.TextBox();
            this.txtPromotionDesc = new System.Windows.Forms.RichTextBox();
            this.dtimeFromDate = new System.Windows.Forms.DateTimePicker();
            this.dtimeToDate = new System.Windows.Forms.DateTimePicker();
            this.lblUsername = new System.Windows.Forms.Label();
            this.dgvPromotion = new System.Windows.Forms.DataGridView();
            this.btnGetAllPromotion = new System.Windows.Forms.Button();
            this.btnAddPromotion = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnBackToProductForm = new System.Windows.Forms.Button();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtSearchPromoId = new System.Windows.Forms.TextBox();
            this.txtSearchProId = new System.Windows.Forms.TextBox();
            this.dtimeSFrom = new System.Windows.Forms.DateTimePicker();
            this.dtimeSTo = new System.Windows.Forms.DateTimePicker();
            this.btnSearch = new System.Windows.Forms.Button();
            this.chkEnable = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkDateAllow = new System.Windows.Forms.CheckBox();
            this.chkSearchEnable = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPromotion)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Yu Mincho", 28.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label16.Location = new System.Drawing.Point(423, 15);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(343, 49);
            this.label16.TabIndex = 1;
            this.label16.Text = "Manage Promotion";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(65, 102);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Promotion ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(478, 102);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Discount Percent";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 223);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(147, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Promotion Description";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(79, 147);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Product ID";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(516, 166);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 17);
            this.label6.TabIndex = 7;
            this.label6.Text = "From Date";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(531, 216);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 17);
            this.label7.TabIndex = 8;
            this.label7.Text = "To Date";
            // 
            // txtPromotionID
            // 
            this.txtPromotionID.Location = new System.Drawing.Point(170, 102);
            this.txtPromotionID.Margin = new System.Windows.Forms.Padding(2);
            this.txtPromotionID.Name = "txtPromotionID";
            this.txtPromotionID.ReadOnly = true;
            this.txtPromotionID.Size = new System.Drawing.Size(246, 20);
            this.txtPromotionID.TabIndex = 9;
            // 
            // txtProductId
            // 
            this.txtProductId.Location = new System.Drawing.Point(170, 147);
            this.txtProductId.Margin = new System.Windows.Forms.Padding(2);
            this.txtProductId.Name = "txtProductId";
            this.txtProductId.ReadOnly = true;
            this.txtProductId.Size = new System.Drawing.Size(246, 20);
            this.txtProductId.TabIndex = 9;
            // 
            // txtDisPercent
            // 
            this.txtDisPercent.Location = new System.Drawing.Point(606, 102);
            this.txtDisPercent.Margin = new System.Windows.Forms.Padding(2);
            this.txtDisPercent.Name = "txtDisPercent";
            this.txtDisPercent.Size = new System.Drawing.Size(204, 20);
            this.txtDisPercent.TabIndex = 9;
            // 
            // txtPromotionDesc
            // 
            this.txtPromotionDesc.Location = new System.Drawing.Point(170, 187);
            this.txtPromotionDesc.Margin = new System.Windows.Forms.Padding(2);
            this.txtPromotionDesc.Name = "txtPromotionDesc";
            this.txtPromotionDesc.Size = new System.Drawing.Size(246, 89);
            this.txtPromotionDesc.TabIndex = 10;
            this.txtPromotionDesc.Text = "";
            // 
            // dtimeFromDate
            // 
            this.dtimeFromDate.Location = new System.Drawing.Point(606, 165);
            this.dtimeFromDate.Margin = new System.Windows.Forms.Padding(2);
            this.dtimeFromDate.Name = "dtimeFromDate";
            this.dtimeFromDate.Size = new System.Drawing.Size(204, 20);
            this.dtimeFromDate.TabIndex = 12;
            // 
            // dtimeToDate
            // 
            this.dtimeToDate.Location = new System.Drawing.Point(606, 216);
            this.dtimeToDate.Margin = new System.Windows.Forms.Padding(2);
            this.dtimeToDate.Name = "dtimeToDate";
            this.dtimeToDate.Size = new System.Drawing.Size(204, 20);
            this.dtimeToDate.TabIndex = 12;
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsername.ForeColor = System.Drawing.Color.Blue;
            this.lblUsername.Location = new System.Drawing.Point(18, 15);
            this.lblUsername.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(146, 31);
            this.lblUsername.TabIndex = 13;
            this.lblUsername.Text = "User name";
            // 
            // dgvPromotion
            // 
            this.dgvPromotion.AllowUserToAddRows = false;
            this.dgvPromotion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPromotion.Location = new System.Drawing.Point(82, 323);
            this.dgvPromotion.Margin = new System.Windows.Forms.Padding(2);
            this.dgvPromotion.Name = "dgvPromotion";
            this.dgvPromotion.ReadOnly = true;
            this.dgvPromotion.RowTemplate.Height = 24;
            this.dgvPromotion.Size = new System.Drawing.Size(728, 232);
            this.dgvPromotion.TabIndex = 14;
            // 
            // btnGetAllPromotion
            // 
            this.btnGetAllPromotion.Location = new System.Drawing.Point(913, 86);
            this.btnGetAllPromotion.Margin = new System.Windows.Forms.Padding(2);
            this.btnGetAllPromotion.Name = "btnGetAllPromotion";
            this.btnGetAllPromotion.Size = new System.Drawing.Size(142, 51);
            this.btnGetAllPromotion.TabIndex = 15;
            this.btnGetAllPromotion.Text = "Get All Promotions";
            this.btnGetAllPromotion.UseVisualStyleBackColor = true;
            this.btnGetAllPromotion.Click += new System.EventHandler(this.btnGetAllPromotion_Click);
            // 
            // btnAddPromotion
            // 
            this.btnAddPromotion.Location = new System.Drawing.Point(913, 165);
            this.btnAddPromotion.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddPromotion.Name = "btnAddPromotion";
            this.btnAddPromotion.Size = new System.Drawing.Size(142, 51);
            this.btnAddPromotion.TabIndex = 16;
            this.btnAddPromotion.Text = "Create a promotion";
            this.btnAddPromotion.UseVisualStyleBackColor = true;
            this.btnAddPromotion.Click += new System.EventHandler(this.btnAddPromotion_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(1092, 86);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(2);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(136, 51);
            this.btnUpdate.TabIndex = 17;
            this.btnUpdate.Text = "Update a Promotion";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(1092, 165);
            this.btnRemove.Margin = new System.Windows.Forms.Padding(2);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(136, 51);
            this.btnRemove.TabIndex = 18;
            this.btnRemove.Text = "Remove Promotion";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnBackToProductForm
            // 
            this.btnBackToProductForm.Location = new System.Drawing.Point(312, 579);
            this.btnBackToProductForm.Margin = new System.Windows.Forms.Padding(2);
            this.btnBackToProductForm.Name = "btnBackToProductForm";
            this.btnBackToProductForm.Size = new System.Drawing.Size(278, 51);
            this.btnBackToProductForm.TabIndex = 19;
            this.btnBackToProductForm.Text = "Back to Manage Product";
            this.btnBackToProductForm.UseVisualStyleBackColor = true;
            this.btnBackToProductForm.Click += new System.EventHandler(this.btnBackToProductForm_Click);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Margin = new System.Windows.Forms.Padding(2);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(2, 711);
            this.splitter1.TabIndex = 20;
            this.splitter1.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(538, 260);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 17);
            this.label5.TabIndex = 21;
            this.label5.Text = "Enable";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(39, 77);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 17);
            this.label8.TabIndex = 23;
            this.label8.Text = "Enable";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(2, 13);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 17);
            this.label9.TabIndex = 23;
            this.label9.Text = "Promotion ID";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(17, 46);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 17);
            this.label10.TabIndex = 24;
            this.label10.Text = "Product ID";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(17, 110);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 17);
            this.label12.TabIndex = 25;
            this.label12.Text = "From Date";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(32, 150);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(59, 17);
            this.label13.TabIndex = 26;
            this.label13.Text = "To Date";
            // 
            // txtSearchPromoId
            // 
            this.txtSearchPromoId.Location = new System.Drawing.Point(127, 10);
            this.txtSearchPromoId.Margin = new System.Windows.Forms.Padding(2);
            this.txtSearchPromoId.Name = "txtSearchPromoId";
            this.txtSearchPromoId.Size = new System.Drawing.Size(188, 20);
            this.txtSearchPromoId.TabIndex = 27;
            // 
            // txtSearchProId
            // 
            this.txtSearchProId.Location = new System.Drawing.Point(127, 46);
            this.txtSearchProId.Margin = new System.Windows.Forms.Padding(2);
            this.txtSearchProId.Name = "txtSearchProId";
            this.txtSearchProId.Size = new System.Drawing.Size(188, 20);
            this.txtSearchProId.TabIndex = 27;
            // 
            // dtimeSFrom
            // 
            this.dtimeSFrom.Location = new System.Drawing.Point(127, 110);
            this.dtimeSFrom.Margin = new System.Windows.Forms.Padding(2);
            this.dtimeSFrom.Name = "dtimeSFrom";
            this.dtimeSFrom.Size = new System.Drawing.Size(188, 20);
            this.dtimeSFrom.TabIndex = 12;
            this.dtimeSFrom.Value = new System.DateTime(2017, 1, 10, 0, 0, 0, 0);
            this.dtimeSFrom.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // dtimeSTo
            // 
            this.dtimeSTo.Location = new System.Drawing.Point(127, 147);
            this.dtimeSTo.Margin = new System.Windows.Forms.Padding(2);
            this.dtimeSTo.Name = "dtimeSTo";
            this.dtimeSTo.Size = new System.Drawing.Size(188, 20);
            this.dtimeSTo.TabIndex = 12;
            this.dtimeSTo.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(60, 190);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(124, 29);
            this.btnSearch.TabIndex = 16;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // chkEnable
            // 
            this.chkEnable.AutoSize = true;
            this.chkEnable.Location = new System.Drawing.Point(606, 267);
            this.chkEnable.Name = "chkEnable";
            this.chkEnable.Size = new System.Drawing.Size(15, 14);
            this.chkEnable.TabIndex = 28;
            this.chkEnable.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.chkDateAllow);
            this.panel1.Controls.Add(this.chkSearchEnable);
            this.panel1.Controls.Add(this.dtimeSFrom);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.txtSearchProId);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.txtSearchPromoId);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Controls.Add(this.dtimeSTo);
            this.panel1.Location = new System.Drawing.Point(913, 323);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(327, 232);
            this.panel1.TabIndex = 29;
            // 
            // chkDateAllow
            // 
            this.chkDateAllow.AutoSize = true;
            this.chkDateAllow.Location = new System.Drawing.Point(210, 197);
            this.chkDateAllow.Name = "chkDateAllow";
            this.chkDateAllow.Size = new System.Drawing.Size(114, 17);
            this.chkDateAllow.TabIndex = 29;
            this.chkDateAllow.Text = "Allow Search Date";
            this.chkDateAllow.UseVisualStyleBackColor = true;
            // 
            // chkSearchEnable
            // 
            this.chkSearchEnable.AutoSize = true;
            this.chkSearchEnable.Checked = true;
            this.chkSearchEnable.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSearchEnable.Location = new System.Drawing.Point(127, 80);
            this.chkSearchEnable.Name = "chkSearchEnable";
            this.chkSearchEnable.Size = new System.Drawing.Size(15, 14);
            this.chkSearchEnable.TabIndex = 28;
            this.chkSearchEnable.UseVisualStyleBackColor = true;
            // 
            // frmPromotion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1336, 711);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.chkEnable);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.btnBackToProductForm);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnAddPromotion);
            this.Controls.Add(this.btnGetAllPromotion);
            this.Controls.Add(this.dgvPromotion);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.dtimeToDate);
            this.Controls.Add(this.dtimeFromDate);
            this.Controls.Add(this.txtPromotionDesc);
            this.Controls.Add(this.txtDisPercent);
            this.Controls.Add(this.txtProductId);
            this.Controls.Add(this.txtPromotionID);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label16);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmPromotion";
            this.Text = "frmPromotion";
            this.Load += new System.EventHandler(this.frmPromotion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPromotion)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtPromotionID;
        private System.Windows.Forms.TextBox txtProductId;
        private System.Windows.Forms.TextBox txtDisPercent;
        private System.Windows.Forms.RichTextBox txtPromotionDesc;
        private System.Windows.Forms.DateTimePicker dtimeFromDate;
        private System.Windows.Forms.DateTimePicker dtimeToDate;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.DataGridView dgvPromotion;
        private System.Windows.Forms.Button btnGetAllPromotion;
        private System.Windows.Forms.Button btnAddPromotion;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Button btnBackToProductForm;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtSearchPromoId;
        private System.Windows.Forms.TextBox txtSearchProId;
        private System.Windows.Forms.DateTimePicker dtimeSFrom;
        private System.Windows.Forms.DateTimePicker dtimeSTo;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.CheckBox chkEnable;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox chkSearchEnable;
        private System.Windows.Forms.CheckBox chkDateAllow;
    }
}