﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ShoppingWF.BUS;
using ShoppingWF.Models;
using ShoppingWF.ProductForm;
using ShoppingWF.DAO;


namespace ShoppingWF
{
    public partial class frmProductManager : Form
    {
        public string userName;
        DataTable dtProduct = null;
        public frmProductManager(string username)
        {
            InitializeComponent();
            this.userName = username;
            lblUsername.Text = "Welcome, " + userName + "!";

            dtimeFrom.Value = DateTime.Now.AddDays(-30);
            dtimeTo.Value = DateTime.Now;
        }

        private void frmProductManager_Load(object sender, EventArgs e)
        {
            btnGetAllProducts_Click(sender, e);
        }


        private void txtCreatedUser_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnGetAllProducts_Click(object sender, EventArgs e)
        {
            //list all product from DB and bind on datagridview
            var listProduct = new List<ProductModels>(ProductBUS.GetAllProducts());
            //add to tabledata
            dtProduct = new DataTable();
            dtProduct.Columns.Add("Product_ID");
            dtProduct.Columns.Add("Catalogue_ID");
            dtProduct.Columns.Add("Is_Sale");
            dtProduct.Columns.Add("Product_Name");
            dtProduct.Columns.Add("Price");
            dtProduct.Columns.Add("Level_Trending");
            dtProduct.Columns.Add("Description");
            dtProduct.Columns.Add("Products_Available");
            dtProduct.Columns.Add("Total_Sold");
            dtProduct.Columns.Add("Created_Date");
            dtProduct.Columns.Add("Created_Username");
            dtProduct.Columns.Add("Guarantee_Description");
            dtProduct.Columns.Add("Title_Image");
            dtProduct.Columns.Add("Tax_Percent");
            dtProduct.Columns.Add("Manufacturer");
            foreach (var item in listProduct)
            {
                DataRow row = dtProduct.NewRow();
                row["Product_ID"] = item.Product_ID;
                row["Catalogue_ID"] = item.Catalogue_ID;
                row["Is_Sale"] = item.Is_Sale;
                row["Product_Name"] = item.Product_Name;
                row["Price"] = item.Price;
                row["Level_Trending"] = item.Level_Trending;
                row["Description"] = item.Description;
                row["Products_Available"] = item.Products_Available;
                row["Total_Sold"] = item.Total_Sold;
                row["Created_Date"] = item.Created_Date;
                row["Created_Username"] = item.Created_Username;
                row["Guarantee_Description"] = item.Guarantee_Description;
                row["Title_Image"] = item.Title_Image;
                row["Tax_Percent"] = item.Tax_Percent;
                row["Manufacturer"] = item.Manufacturer;
                dtProduct.Rows.Add(row);
            }
            dtProduct.PrimaryKey = new DataColumn[] { dtProduct.Columns[0] };

            //clear binding all field
            txtProductID.DataBindings.Clear();
            txtCatalougeID.DataBindings.Clear();
            rtbSale.DataBindings.Clear();
            txtProductName.DataBindings.Clear();
            txtPrice.DataBindings.Clear();
            txtLevelTrending.DataBindings.Clear();
            rtxtDescription.DataBindings.Clear();
            txtProductAvai.DataBindings.Clear();
            txtTotalSold.DataBindings.Clear();
            dtimeCreate.DataBindings.Clear();
            txtCreatedUser.DataBindings.Clear();
            rtxtGuaranteeDesc.DataBindings.Clear();
            txtTitleImage.DataBindings.Clear();
            txtTaxPercent.DataBindings.Clear();
            txtManufacturer.DataBindings.Clear();

            //add binding
            txtProductID.DataBindings.Add("Text", dtProduct, "Product_ID");
            txtCatalougeID.DataBindings.Add("Text", dtProduct, "Catalogue_ID");
            rtbSale.DataBindings.Add("Checked", dtProduct, "Is_Sale");
            txtProductName.DataBindings.Add("Text", dtProduct, "Product_Name");
            txtPrice.DataBindings.Add("Text", dtProduct, "Price");
            txtLevelTrending.DataBindings.Add("Text", dtProduct, "Level_Trending");
            rtxtDescription.DataBindings.Add("Text", dtProduct, "Description");
            txtProductAvai.DataBindings.Add("Text", dtProduct, "Products_Available");
            txtTotalSold.DataBindings.Add("Text", dtProduct, "Total_Sold");
            dtimeCreate.DataBindings.Add("Value", dtProduct, "Created_Date");
            //dtimeCreate.DataBindings["Value"].Format += new ConvertEventHandler(Formatter);
            //dtimeCreate.DataBindings["Value"].Parse += new ConvertEventHandler(Parser);
            txtCreatedUser.DataBindings.Add("Text", dtProduct, "Created_Username");
            rtxtGuaranteeDesc.DataBindings.Add("Text", dtProduct, "Guarantee_Description");
            txtTitleImage.DataBindings.Add("Text", dtProduct, "Title_Image");
            txtTaxPercent.DataBindings.Add("Text", dtProduct, "Tax_Percent");
            txtManufacturer.DataBindings.Add("Text", dtProduct, "Manufacturer");
            dgvProduct.DataSource = dtProduct;

        }

        private void btnCreateProduct_Click(object sender, EventArgs e)
        {
            frmCreateProduct frmCreatePro = new frmCreateProduct(userName);
            frmCreatePro.ShowDialog(this);
            btnGetAllProducts_Click(null, null);
            this.Opacity = 1;
        }

        private void btnUpdateProduct_Click(object sender, EventArgs e)
        {


            int proID = 0, catalougeID, level_Trending, products_Available, total_Sold;
            float price, taxPercent;
            Int32.TryParse(txtProductID.Text, out proID);
            if (Int32.TryParse(txtCatalougeID.Text, out catalougeID) == false)
            {
                MessageBox.Show("Catalouge Id must be integer.");
                return;
            }
            else if (catalougeID <= 0)
            {
                MessageBox.Show("Catalouge Id must be greater than zero.");
                return;
            }

            if (txtProductName.Text.Length == 0)
            {
                MessageBox.Show("Product Name is required.");
                return;
            }

            if (float.TryParse(txtPrice.Text, out price) == false)
            {
                MessageBox.Show("Price must be float number.");
                return;
            }
            else if (price <= 0)
            {
                MessageBox.Show("Price Id must be greater than zero.");
                return;
            }

            if (Int32.TryParse(txtLevelTrending.Text, out level_Trending) == false)
            {
                MessageBox.Show("level_trending must be integer.");
                return;
            }
            else if (level_Trending <= 0)
            {
                MessageBox.Show("level_trending must be greater than zero.");
                return;
            }

            if(float.TryParse(txtTaxPercent.Text, out taxPercent) == false)
            {
                MessageBox.Show("Tax percent must be float.");
                return;
            }else if(taxPercent <= 0)
            {
                MessageBox.Show("Tax percent must be greater than zero.");
                return;
            }

            if(Int32.TryParse(txtProductAvai.Text, out products_Available) == false)
            {
                MessageBox.Show("Product Available must be integer.");
                return;
            }
            else if(products_Available < 0)
            {
                MessageBox.Show("Product Available must be greater than zero.");
                return;
            }

            if(Int32.TryParse(txtTotalSold.Text, out total_Sold) == false)
            {
                MessageBox.Show("Total Sold  must be integer.");
                return;
            }
            else if(total_Sold < 0) {
                MessageBox.Show("Total Sold  must be greater than zero.");
                return;
            }

            bool isSale = rtbSale.Checked;
            //valid all field successto update


            ProductModels tmp = new ProductModels
            {
                Product_ID = proID,
                Catalogue_ID = catalougeID,
                Is_Sale = isSale,
                Product_Name = txtProductName.Text,
                Price = price,
                Level_Trending = level_Trending,
                Description = rtxtDescription.Text,
                Products_Available = products_Available,
                Total_Sold = total_Sold,
                Created_Date = DateTime.Parse(dtimeCreate.Text),
                Created_Username = txtCreatedUser.Text,
                Guarantee_Description = rtxtGuaranteeDesc.Text,
                Title_Image = txtTitleImage.Text,
                Tax_Percent = taxPercent,
                Manufacturer = txtManufacturer.Text
            };
            if (ProductBUS.Update(tmp))
            {
                dgvProduct.Refresh();
                MessageBox.Show("Update Successful.");
                btnGetAllProducts_Click(null, null);
            }
            else MessageBox.Show("Update fail.");
        }

        private void rtbSale_Click(object sender, EventArgs e)
        {
            if (rtbSale.Checked == true)
            {
                rtbSale.Checked = false;
            }
            else if (rtbSale.Checked == false)
            {
                rtbSale.Checked = true;
            }
        }

        private void btnDeleteProduct_Click(object sender, EventArgs e)
        {
            if (txtProductID.Text == null)
            {
                MessageBox.Show("Delete fail!");
            }
            int tmpID = 0;
            Int32.TryParse(txtProductID.Text, out tmpID);
            if (ProductBUS.Delete(tmpID))
            {
                MessageBox.Show("Delete successfull!");
                btnGetAllProducts_Click(null, null);
            }
            else MessageBox.Show("Delete fail.");
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            int proId = 0;
            Int32.TryParse(txtSearchProID.Text, out proId);
            int cataID = 0;
            Int32.TryParse(txtSearchCataID.Text, out cataID);
            string proName = txtSearchProName.Text;
            DateTime fromDate = dtimeFrom.Value;
            DateTime toDate = dtimeTo.Value;

            //same getallPro
            //list all product from DB and bind on datagridview
            List<ProductModels> listProduct;
            if (chkAllowDate.Checked)
            {
                listProduct = new List<ProductModels>(ProductBUS.Search(proId, cataID, proName, fromDate, toDate));
            }
            else
            {
                listProduct = ProductBUS.Search(proId, cataID, proName);
            }

            //add to tabledata
            dtProduct = new DataTable();
            dtProduct.Columns.Add("Product_ID");
            dtProduct.Columns.Add("Catalogue_ID");
            dtProduct.Columns.Add("Is_Sale");
            dtProduct.Columns.Add("Product_Name");
            dtProduct.Columns.Add("Price");
            dtProduct.Columns.Add("Level_Trending");
            dtProduct.Columns.Add("Description");
            dtProduct.Columns.Add("Products_Available");
            dtProduct.Columns.Add("Total_Sold");
            dtProduct.Columns.Add("Created_Date");
            dtProduct.Columns.Add("Created_Username");
            dtProduct.Columns.Add("Guarantee_Description");
            dtProduct.Columns.Add("Title_Image");
            dtProduct.Columns.Add("Tax_Percent");
            dtProduct.Columns.Add("Manufacturer");
            foreach (var item in listProduct)
            {
                DataRow row = dtProduct.NewRow();
                row["Product_ID"] = item.Product_ID;
                row["Catalogue_ID"] = item.Catalogue_ID;
                row["Is_Sale"] = item.Is_Sale;
                row["Product_Name"] = item.Product_Name;
                row["Price"] = item.Price;
                row["Level_Trending"] = item.Level_Trending;
                row["Description"] = item.Description;
                row["Products_Available"] = item.Products_Available;
                row["Total_Sold"] = item.Total_Sold;
                row["Created_Date"] = item.Created_Date;
                row["Created_Username"] = item.Created_Username;
                row["Guarantee_Description"] = item.Guarantee_Description;
                row["Title_Image"] = item.Title_Image;
                row["Tax_Percent"] = item.Tax_Percent;
                row["Manufacturer"] = item.Manufacturer;
                dtProduct.Rows.Add(row);
            }
            dtProduct.PrimaryKey = new DataColumn[] { dtProduct.Columns[0] };

            //clear binding all field
            txtProductID.DataBindings.Clear();
            txtCatalougeID.DataBindings.Clear();
            rtbSale.DataBindings.Clear();
            txtProductName.DataBindings.Clear();
            txtPrice.DataBindings.Clear();
            txtLevelTrending.DataBindings.Clear();
            rtxtDescription.DataBindings.Clear();
            txtProductAvai.DataBindings.Clear();
            txtTotalSold.DataBindings.Clear();
            dtimeCreate.DataBindings.Clear();
            txtCreatedUser.DataBindings.Clear();
            rtxtGuaranteeDesc.DataBindings.Clear();
            txtTitleImage.DataBindings.Clear();
            txtTaxPercent.DataBindings.Clear();
            txtManufacturer.DataBindings.Clear();

            //add binding
            txtProductID.DataBindings.Add("Text", dtProduct, "Product_ID");
            txtCatalougeID.DataBindings.Add("Text", dtProduct, "Catalogue_ID");
            rtbSale.DataBindings.Add("Checked", dtProduct, "Is_Sale");
            txtProductName.DataBindings.Add("Text", dtProduct, "Product_Name");
            txtPrice.DataBindings.Add("Text", dtProduct, "Price");
            txtLevelTrending.DataBindings.Add("Text", dtProduct, "Level_Trending");
            rtxtDescription.DataBindings.Add("Text", dtProduct, "Description");
            txtProductAvai.DataBindings.Add("Text", dtProduct, "Products_Available");
            txtTotalSold.DataBindings.Add("Text", dtProduct, "Total_Sold");
            dtimeCreate.DataBindings.Add("Value", dtProduct, "Created_Date");
            txtCreatedUser.DataBindings.Add("Text", dtProduct, "Created_Username");
            rtxtGuaranteeDesc.DataBindings.Add("Text", dtProduct, "Guarantee_Description");
            txtTitleImage.DataBindings.Add("Text", dtProduct, "Title_Image");
            txtTaxPercent.DataBindings.Add("Text", dtProduct, "Tax_Percent");
            txtManufacturer.DataBindings.Add("Text", dtProduct, "Manufacturer");
            dgvProduct.DataSource = dtProduct;

        }

        private void btnPromoteProduct_Click(object sender, EventArgs e)
        {
            var row = dgvProduct.CurrentRow;
            frmPromotion frmPromotion;
            if (row != null)
            {
                int productId;
                int.TryParse(row.Cells[0].Value.ToString(), out productId);

                this.Opacity = 0;
                frmPromotion = new frmPromotion(userName, productId);
                frmPromotion.ShowDialog(this);
                this.Opacity = 1;

                return;
            }

            this.Opacity = 0;
            frmPromotion = new frmPromotion(userName);
            frmPromotion.ShowDialog(this);
        }

        private void btnManageImages_Click(object sender, EventArgs e)
        {
            if (dgvProduct.SelectedCells.Count < 1)
            {
                MessageBox.Show("Please select a product");
                return;
            }

            string id = dgvProduct.CurrentRow.Cells[0].Value.ToString();
            this.Opacity = 0;
            FrmManageImages frmImages = new FrmManageImages(id);
            frmImages.ShowDialog(this);
            this.Opacity = 1;
        }

        private void btnExportToExcel_Click(object sender, EventArgs e)
        {
            try
            {
                ExportExcel.ExportToExcel(dgvProduct, @"C:\Users\Hoang Man-PC\Desktop\", "Products");
                MessageBox.Show("Export successful");
            }
            catch (Exception)
            {
                MessageBox.Show("Export Fail.");
                throw;

            }
        }


        //private void Formatter(object source, ConvertEventArgs e)
        //{
        //    Binding binding = source as Binding;
        //    DateTimePicker dtp = binding.Control as DateTimePicker;
        //    if (dtp == null)
        //        return;
        //    if (e.Value == null || e.Value == DBNull.Value)
        //    {
        //        dtp.Value = DateTime.Now; // optional
        //    }
        //    else
        //    {
        //        dtp.Value = (DateTime)e.Value;
        //    }
        //}

        //private void Parser(object source, ConvertEventArgs e)
        //{
        //    Binding binding = source as Binding;
        //    DateTimePicker dtp = binding.Control as DateTimePicker;
        //    if (dtp == null)
        //        return;
        //    if (dtp.Checked)
        //        e.Value = dtp.Value;
        //    else
        //        e.Value = DBNull.Value;
        //}
    }
}
