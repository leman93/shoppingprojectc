﻿using ShoppingWF.BUS;
using ShoppingWF.DAO;
using ShoppingWF.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShoppingWF.ProductForm
{
    public partial class FrmManageImages : Form
    {
        private Product product;
        public FrmManageImages()
        {
            InitializeComponent();
        }

        public FrmManageImages(string idStr)
        {
            InitializeComponent();
            int id = int.Parse(idStr);
            product = ProductDAO.GetProductById(id);
            lblProductId.Text = product.Product_ID.ToString();
            lblProductName.Text = product.Product_Name;
        }

        private void btnLoadAll_Click(object sender, EventArgs e)
        {
            dgvImages.DataSource = ImageBUS.getAllImages(product.Product_ID);
        }

        private void dgvImages_Click(object sender, EventArgs e)
        {
            if (dgvImages.SelectedCells.Count > 0)
            {
                var row = dgvImages.CurrentRow;
                txtUrl.Text = row.Cells[2].Value.ToString();
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (dgvImages.SelectedCells.Count < 1)
            {
                MessageBox.Show("Please select image");
                return;
            }

            if (string.IsNullOrEmpty(txtUrl.Text.Trim()))
            {
                MessageBox.Show("Url can not is empty");
                return;
            }
            var row = dgvImages.CurrentRow;
            Image img = new Image()
            {
                Image_ID = int.Parse(row.Cells[0].Value.ToString()),
                Url = txtUrl.Text.Trim(),
            };

            bool result = ImageDAO.update(img);
            if (result)
            {
                MessageBox.Show("Image update success");
            }
            else
            {
                MessageBox.Show("Image update fail");
                return;
            }
            btnLoadAll_Click(null, null);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtUrl.Text.Trim()))
            {
                MessageBox.Show("Url can not is empty");
                return;
            }

            Image img = new Image()
            {
                Product_ID = product.Product_ID,
                Url = txtUrl.Text.Trim(),
            };
            bool result = ImageDAO.insert(img);
            if (result)
            {
                MessageBox.Show("Image add success");
            }
            else
            {
                MessageBox.Show("Image add fail");
                return;
            }
            btnLoadAll_Click(null, null);

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvImages.SelectedCells.Count < 1)
            {
                MessageBox.Show("Please select image");
                return;
            }

            string idStr = dgvImages.CurrentRow.Cells[0].Value.ToString();
            int id;
            if(!int.TryParse(idStr, out id))
            {
                MessageBox.Show("Image delete fail");
                return;
            }

            bool result = ImageDAO.delete(id);
            if (result)
            {
                MessageBox.Show("Image delete success");
            }
            else
            {
                MessageBox.Show("Image delete fail");
                return;
            }
            btnLoadAll_Click(null, null);
        }
    }
}
