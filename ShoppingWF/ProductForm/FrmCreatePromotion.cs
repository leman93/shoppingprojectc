﻿using ShoppingWF.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShoppingWF.ProductForm
{
    public partial class FrmCreatePromotion : Form
    {
        private int productId;
        public FrmCreatePromotion()
        {
            InitializeComponent();
        }

        public FrmCreatePromotion(int productId)
        {
            InitializeComponent();
            this.productId = productId;
            txtProductId.Text = productId.ToString();
        }

        private bool isValidData()
        {
            string description = txtDescription.Text;
            if (string.IsNullOrWhiteSpace(description))
            {
                MessageBox.Show("Description can not is empty");
                return false;
            }
            int discount;
            if (string.IsNullOrWhiteSpace(txtDiscount.Text))
            {
                MessageBox.Show("Discount Percent can not is empty");
                return false;
            }
            if(!int.TryParse(txtDiscount.Text.Trim(), out discount))
            {
                MessageBox.Show("Discount percent must is an integer");
                return false;
            }
            else if(discount < 0)
            {
                MessageBox.Show("Discount percent must be greater than zero");
                return false;
            }

            var fromDate = dtpFromDate.Value;
            var toDate = dtpToDate.Value;
            if(fromDate > toDate)
            {
                MessageBox.Show("ToDate must is later than FromDate");
                return false;
            }
            return true;
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (!isValidData())
            {
                return;
            }

            Promotion newPromotion = new Promotion()
            {
                Product_ID = this.productId,
                Promotion_Description = txtDescription.Text.Trim(),
                Discount_Percent = int.Parse(txtDiscount.Text.Trim()),
                From_Date = dtpFromDate.Value,
                To_Date = dtpToDate.Value,
                Enable = chkEnable.Checked,
            };

            bool result = PromotionDAO.insert(newPromotion);
            if (result)
            {
                MessageBox.Show("Promotion create success");
                this.Close();
            }
            else
            {
                MessageBox.Show("Promotion create fail");
            }
        }
    }
}
