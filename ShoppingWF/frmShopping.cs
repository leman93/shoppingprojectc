﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ShoppingWF.Models;
using ShoppingWF.BUS;
using Encryption;
using ShoppingWF.AdminForm;
using ShoppingWF.OrderAndBillForm;

namespace ShoppingWF
{
    public partial class frmShopping : Form
    {
        public Object obj = null;
        public frmShopping()
        {
            InitializeComponent();
            txtPassword.PasswordChar = '*';
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string username = txtuserName.Text;
            string passwordinput = txtPassword.Text;
            if (username.Equals("") || passwordinput.Equals(""))
            {
                MessageBox.Show("Please input Username and password!!!");
                return;
            }
            Employee_AccountModels emp = Employee_AccountBUS.getEmployeeByUsername(username);
            if ( emp == null)
            {
                MessageBox.Show("Invalid Username!!!");
                return;
            }

            EncryptionSHA sha = new EncryptionSHA();
            //emp.Password = sha.getHashedPassword(passwordinput);
            bool result = sha.doesPasswordMatch(passwordinput, emp.Password);
            if (result)
            {
                this.Opacity = 0;
                int level_Access = emp.Level_Access;
                if (level_Access == 1)
                {
                    AdministratorForm frmAd = new AdministratorForm(emp.Username);
                    frmAd.ShowDialog(this);
                }
                else if (level_Access == 2)
                {
                    OrderForm frmOrder = new OrderForm(username);
                    frmOrder.ShowDialog(this);
                }
                else if (level_Access == 3)
                {
                    frmProductManager frmPro = new frmProductManager(username);
                    frmPro.ShowDialog(this);
                }
                txtPassword.Text = "";
                txtuserName.Text = "";
                this.Opacity = 1;
            }
            else
            {
                MessageBox.Show("Wrong pass!Try again");                
                return;

            }
        }
    }
}
