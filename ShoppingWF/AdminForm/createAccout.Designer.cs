﻿namespace ShoppingWF.AdminForm
{
    partial class createAccout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtEmployeeID = new System.Windows.Forms.TextBox();
            this.cbxLevelAccess = new System.Windows.Forms.ComboBox();
            this.btnCreate = new System.Windows.Forms.Button();
            this.uservalid = new System.Windows.Forms.Label();
            this.passvalid = new System.Windows.Forms.Label();
            this.EmployeeValid = new System.Windows.Forms.Label();
            this.lbLAccess = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPassword2 = new System.Windows.Forms.TextBox();
            this.passvalid2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label1.Location = new System.Drawing.Point(28, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Username";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label2.Location = new System.Drawing.Point(29, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label3.Location = new System.Drawing.Point(32, 179);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Employee_ID";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label4.Location = new System.Drawing.Point(32, 219);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Level_Access";
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(158, 60);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(207, 20);
            this.txtUsername.TabIndex = 1;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(159, 99);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(207, 20);
            this.txtPassword.TabIndex = 2;
            // 
            // txtEmployeeID
            // 
            this.txtEmployeeID.Location = new System.Drawing.Point(159, 176);
            this.txtEmployeeID.Name = "txtEmployeeID";
            this.txtEmployeeID.Size = new System.Drawing.Size(207, 20);
            this.txtEmployeeID.TabIndex = 4;
            // 
            // cbxLevelAccess
            // 
            this.cbxLevelAccess.FormattingEnabled = true;
            this.cbxLevelAccess.Items.AddRange(new object[] {
            "admin",
            "manager",
            "orderchecker"});
            this.cbxLevelAccess.Location = new System.Drawing.Point(160, 215);
            this.cbxLevelAccess.Name = "cbxLevelAccess";
            this.cbxLevelAccess.Size = new System.Drawing.Size(121, 21);
            this.cbxLevelAccess.TabIndex = 5;
            // 
            // btnCreate
            // 
            this.btnCreate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnCreate.Location = new System.Drawing.Point(173, 272);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(75, 23);
            this.btnCreate.TabIndex = 6;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // uservalid
            // 
            this.uservalid.AutoSize = true;
            this.uservalid.ForeColor = System.Drawing.Color.Red;
            this.uservalid.Location = new System.Drawing.Point(157, 83);
            this.uservalid.Name = "uservalid";
            this.uservalid.Size = new System.Drawing.Size(262, 13);
            this.uservalid.TabIndex = 5;
            this.uservalid.Text = "Username was exist.Please choose another username";
            this.uservalid.Visible = false;
            // 
            // passvalid
            // 
            this.passvalid.AutoSize = true;
            this.passvalid.ForeColor = System.Drawing.Color.Red;
            this.passvalid.Location = new System.Drawing.Point(157, 119);
            this.passvalid.Name = "passvalid";
            this.passvalid.Size = new System.Drawing.Size(96, 13);
            this.passvalid.TabIndex = 5;
            this.passvalid.Text = "Password is invaild";
            this.passvalid.Visible = false;
            // 
            // EmployeeValid
            // 
            this.EmployeeValid.AutoSize = true;
            this.EmployeeValid.ForeColor = System.Drawing.Color.Red;
            this.EmployeeValid.Location = new System.Drawing.Point(157, 199);
            this.EmployeeValid.Name = "EmployeeValid";
            this.EmployeeValid.Size = new System.Drawing.Size(209, 13);
            this.EmployeeValid.TabIndex = 5;
            this.EmployeeValid.Text = "EmployeeID must be Ex with x is a number.";
            this.EmployeeValid.Visible = false;
            // 
            // lbLAccess
            // 
            this.lbLAccess.AutoSize = true;
            this.lbLAccess.ForeColor = System.Drawing.Color.Red;
            this.lbLAccess.Location = new System.Drawing.Point(157, 239);
            this.lbLAccess.Name = "lbLAccess";
            this.lbLAccess.Size = new System.Drawing.Size(147, 13);
            this.lbLAccess.TabIndex = 5;
            this.lbLAccess.Text = "Please choose Level_Access";
            this.lbLAccess.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label8.Location = new System.Drawing.Point(29, 143);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(121, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "Confirm Password";
            // 
            // txtPassword2
            // 
            this.txtPassword2.Location = new System.Drawing.Point(158, 140);
            this.txtPassword2.Name = "txtPassword2";
            this.txtPassword2.PasswordChar = '*';
            this.txtPassword2.Size = new System.Drawing.Size(207, 20);
            this.txtPassword2.TabIndex = 3;
            // 
            // passvalid2
            // 
            this.passvalid2.AutoSize = true;
            this.passvalid2.ForeColor = System.Drawing.Color.Red;
            this.passvalid2.Location = new System.Drawing.Point(156, 160);
            this.passvalid2.Name = "passvalid2";
            this.passvalid2.Size = new System.Drawing.Size(107, 13);
            this.passvalid2.TabIndex = 5;
            this.passvalid2.Text = "Password is incorrect";
            this.passvalid2.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label5.Location = new System.Drawing.Point(142, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(162, 25);
            this.label5.TabIndex = 46;
            this.label5.Text = "Create Account";
            // 
            // createAccout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(428, 307);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lbLAccess);
            this.Controls.Add(this.EmployeeValid);
            this.Controls.Add(this.passvalid2);
            this.Controls.Add(this.passvalid);
            this.Controls.Add(this.uservalid);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.cbxLevelAccess);
            this.Controls.Add(this.txtPassword2);
            this.Controls.Add(this.txtEmployeeID);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "createAccout";
            this.Text = "createAccout";
            this.Load += new System.EventHandler(this.createAccout_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtEmployeeID;
        private System.Windows.Forms.ComboBox cbxLevelAccess;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Label uservalid;
        private System.Windows.Forms.Label passvalid;
        private System.Windows.Forms.Label EmployeeValid;
        private System.Windows.Forms.Label lbLAccess;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPassword2;
        private System.Windows.Forms.Label passvalid2;
        private System.Windows.Forms.Label label5;
    }
}