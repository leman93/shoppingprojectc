﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ShoppingWF.Models;

namespace ShoppingWF.AdminForm
{
    public partial class frmAdmin : Form
    {
        public string username { get; }

        public frmAdmin(string username)
        {
            InitializeComponent();
            this.username = username;
            lblAccountInfo.Text = username;
        }
       

        private void frmAdmin_Load(object sender, EventArgs e)
        {

        }

    }
}
