﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessLogicLayer;
using ShoppingWF.DAO;
using ShoppingWF.Models;
using ShoppingWF.BUS;
using System.Text.RegularExpressions;

namespace ShoppingWF.AdminForm
{
    public partial class CreateEmployee : Form
    {
        public CreateEmployee()
        {
            InitializeComponent();
            RoleModels.Roles = RoleBUS.getAllRoles();
            cbxRole.DataSource = RoleModels.Roles;
            //Role.Roles = RoleDAO.getAllRoles();
            //cbxRole.DataSource = Role.Roles;
            cbxRole.DisplayMember = "Role_Name";
            cbxRole.ValueMember = "Role_ID";
        }

        private void btnCreateAccount_Click(object sender, EventArgs e)
        {
            inputData();
            if (lbFullname.Visible == true || lbAddress.Visible == true ||
                lbPhone.Visible == true || lbRole.Visible == true)
            {
                MessageBox.Show("You're missing some requirement");
                return;
            }
            else
            {
                EmployeeBLL BLL = new EmployeeBLL();
                string ID = BLL.createEmployee(txtFullName.Text, txtAddress.Text, cbxRole.SelectedValue.ToString(), txtPhoneNumber.Text);
                if (ID.Equals("No"))
                {
                    MessageBox.Show("Employee create fail");
                }
                else
                {
                    MessageBox.Show("Created with Employee_ID : " + ID);
                    this.Close();
                }

            }
        }
        private void inputData()
        {
            Regex myregexName = new Regex("^(\\b[A-Za-z]*\\b(\\s+\\b[A-Za-z]*\\b)*(\\.[A-Za-z])?)$");
            Regex myregexPhone = new Regex(@"\d{10,11}");
            string name = txtFullName.Text;
            string phoneNumber = txtPhoneNumber.Text;
            Match matchName = myregexName.Match(name);
            Match matchPhone = myregexPhone.Match(phoneNumber);
            if (txtFullName.Text.Equals(""))
            {
                lbFullname.Text = "Full Name is empty";
                lbFullname.Visible = true;
            }
            else if (!matchName.Success)
            {
                lbFullname.Text = "Full Name is invalid";
                lbFullname.Visible = true;
            }
            else
            {
                lbFullname.Visible = false;
            }

            if (txtAddress.Text.Equals(""))
            {
                lbAddress.Visible = true;
            }
            else
            {
                lbAddress.Visible = false;
            }

            if (cbxRole.Text.Equals(""))
            {
                lbRole.Visible = true;
            }
            else
            {
                lbRole.Visible = false;
            }
            if (txtPhoneNumber.Text.Equals(""))
            {
                lbPhone.Text = "Phone number is empty";
                lbPhone.Visible = true;
            }
            else if (!matchPhone.Success)
            {
                lbPhone.Text = "Phone number is invalid";
                lbPhone.Visible = true;
            }
            else
            {
                lbPhone.Visible = false;
            }
        }

        private void CreateEmployee_Load(object sender, EventArgs e)
        {
            EmployeeBLL BLL = new EmployeeBLL();
            string EM = BLL.getEmpID();
        }

        //private void cbRole_SelectedValueChanged(object sender, EventArgs e)
        //{
        //    string value;
        //    value = cbRole.Text;
        //    if (value.Equals(""))
        //    {
        //        lbRole.Visible = true;

        //    }
        //    else
        //    {
        //        lbRole.Visible = false;

        //    }
        //}
    }
}
