﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessLogicLayer;
using ShoppingWF.BUS;
using ShoppingWF.Models;
using Encryption;
namespace ShoppingWF.AdminForm
{
    public partial class createAccout : Form
    {
        Employee_AccountBLL EmpBLL = new Employee_AccountBLL();
        public string created_by_username;

        public createAccout(string username)
        {

            InitializeComponent();
            created_by_username = username;
            cbxLevelAccess.DropDownStyle = ComboBoxStyle.DropDownList;
            cbxLevelAccess.SelectedIndex = 0;
        }
        public string usernamelogin()
        {
            return txtUsername.Text;
        }
        
        private void createAccout_Load(object sender, EventArgs e)
        {
            
            AdministratorForm adform = (AdministratorForm)(this.Owner);
         
            
        }
        public int checkRole(string role)
        {
            if (role.Equals("admin"))
            {
                return 1;
            }
            else if (role.Equals("manager"))
            {
                return 2;

            }
            else
            {
                return 3;
            }


        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            
             
  
            invalidInputData();
            if (uservalid.Visible == true || passvalid.Visible == true || passvalid2.Visible == true
            || EmployeeValid.Visible == true || lbLAccess.Visible == true
                )
            {
                MessageBox.Show("Requirement is not complete");
            }

            else
            {
                EncryptionSHA sha = new EncryptionSHA();
                Employee_AccountModels tmp = new Employee_AccountModels
                {
                    Username = txtUsername.Text,
                    Password = sha.getHashedPassword(txtPassword.Text),
                    Created_Date = DateTime.Now,
                    Employee_ID = txtEmployeeID.Text,
                    Enable = true,
                    Level_Access = checkRole(cbxLevelAccess.Text),
                    Created_By_Username = created_by_username
                };
                bool checkCreate = Employee_AccountBUS.Insert(tmp);
                if (checkCreate)
                {
                    MessageBox.Show("Create Successful");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Create fail");
                }

            }


        }
       
        private void invalidInputData()
        {
            EmployeeBLL BLL = new EmployeeBLL();
            Employee_AccountBLL ABLL = new Employee_AccountBLL();
            if (txtUsername.Text.Trim().Equals("") )
            {
                txtUsername.Focus();
                uservalid.Text = "Username is empty";
                uservalid.Visible = true;

            }else if(ABLL.checkUsernameCreated(txtUsername.Text)==true){
                uservalid.Text = "Username was exist! Please choose another username";
                uservalid.Visible = true;
            }      
            else
            {
                uservalid.Visible = false;

            }



            if (!txtPassword.Text.Trim().Equals(txtPassword2.Text))
            {
                passvalid2.Visible = true;
            }else
            {
                passvalid2.Visible = false;
            }


            if (txtPassword.Text.Trim().Equals(""))
            {
                txtPassword.Focus();
                passvalid.Text = "Password is empty";
                passvalid.Visible = true;
                

            }
            else
            {
                passvalid.Visible = false;
               
            }

            if (txtEmployeeID.Text.Trim().Equals(""))
            {
                txtUsername.Focus();
                EmployeeValid.Text = "EmployeeID is required";
                EmployeeValid.Visible = true;
            }else if (!BLL.checkEmployeeID(txtEmployeeID.Text))
            {
                EmployeeValid.Text = "EmployeeID must be Ex with x is a number.";
              EmployeeValid.Visible = true;
            }else if (ABLL.checkEmployeeID(txtEmployeeID.Text) == true)
            {
                EmployeeValid.Text = "This EmployeeID has already a account.";
                EmployeeValid.Visible = true;
            }
            else
            {
                EmployeeValid.Visible = false;
                 
            }            
        }
    }
}
