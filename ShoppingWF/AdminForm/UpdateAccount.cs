﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessLogicLayer;

using ShoppingWF.Models;
using ShoppingWF.BUS;
using Encryption;

namespace ShoppingWF.AdminForm
{
    public partial class UpdateAccount : Form
    {
        public string username;
        public UpdateAccount(string username)
        {
            InitializeComponent();
            this.username = username;

        }
       

        private void UpdateAccount_Load(object sender, EventArgs e)
        {
            AdministratorForm form = (AdministratorForm)(this.Owner);
            Employee_AccountModels empAcc = Employee_AccountBUS.getEmployeeByUsername(this.username);
            string username = empAcc.Username;
            string createdUsername = empAcc.Created_By_Username;
            string employee_ID = empAcc.Employee_ID;
            DateTime modifyDate = DateTime.Now;
            lbUsername.Text = username;
            lbModifyBy.Text = createdUsername;
            lbEmployeeID.Text = employee_ID;
            cbxLevelAccess.DropDownStyle = ComboBoxStyle.DropDownList;
            cbxLevelAccess.SelectedIndex = 0;
        }
        private void inputvalidate()
        {
            if (!txtPassword.Text.Trim().Equals(txtPassword2.Text))
            {
                passvalid2.Visible = true;
            }
            else
            {
                passvalid2.Visible = false;
            }


            if (txtPassword.Text.Trim().Equals(""))
            {
                txtPassword.Focus();
                passvalid.Visible = true;


            }
            else
            {
                passvalid.Visible = false;

            }


            if (cbxLevelAccess.Text.Trim().Equals(""))
            {
                lbLAccess.Visible = true;

            }else
            {
                lbLAccess.Visible = false;
            }
        }
        public int checkRole(string role)
        {
            if (role.Equals("admin"))
            {
                return 1;
            }
            else if (role.Equals("manager"))
            {
                return 2;

            }
            else
            {
                return 3;
            }


        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

            inputvalidate();
            if ( passvalid.Visible == true || passvalid2.Visible == true
            ||  lbLAccess.Visible == true
                )
            {
                MessageBox.Show("Requirement is not complete");
            }
            else
            {
                EncryptionSHA sha = new EncryptionSHA();
                Employee_AccountModels current = Employee_AccountBUS.getEmployeeByUsername(lbUsername.Text);
                Employee_AccountModels tmp = new Employee_AccountModels
                {
                    Username = lbUsername.Text,
                    Password = sha.getHashedPassword(txtPassword.Text),
                    Employee_ID = lbEmployeeID.Text,
                    Level_Access = this.checkRole(cbxLevelAccess.Text),
                    Created_By_Username = current.Created_By_Username,
                    Created_Date = current.Created_Date,
                    Enable = current.Enable
                };
                bool checkCreate = Employee_AccountBUS.Update(tmp);
                if (checkCreate)
                {
                    MessageBox.Show("Update Successful");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Update fail");
                }

            }


        }
        public bool checkpassword(string pass1,string pass2)
        {
            if (pass1.Equals(pass2))
            {
                return true;
            }
            return false;
        }        
    }
}
