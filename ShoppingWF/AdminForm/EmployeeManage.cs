﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessLogicLayer;
using ShoppingWF.DAO;

namespace ShoppingWF.AdminForm
{
    public partial class EmployeeManage : Form
    {
        DataTable dtEmp = new DataTable();
        public string LoginEmpID;
        public string employeeID;
        public string employeeIDUpdate()
        {
            return txtEmployeeID.Text;
        }
        public string employeeNameUpdate()
        {

            return txtFullName.Text;
        }
        public EmployeeManage()
        {
            InitializeComponent();
        }

        private void btnCreateAccount_Click(object sender, EventArgs e)
        {
            this.Opacity = 0;
            CreateEmployee createEmployee = new CreateEmployee();
            createEmployee.ShowDialog();
            this.Opacity = 1;
            btnGetAllAccount_Click(null, null);
        }



        private void btnGetAllAccount_Click(object sender, EventArgs e)
        {
            EmployeeBLL empBLL = new EmployeeBLL();
            dtEmp = empBLL.getAllEmp().Tables[0];
            dtEmp.PrimaryKey = new DataColumn[] { dtEmp.Columns[0] };

            txtFullName.DataBindings.Clear();
            txtEmployeeID.DataBindings.Clear();
            txtPhoneNumber.DataBindings.Clear();
            txtAddress.DataBindings.Clear();

            txtEmployeeID.DataBindings.Add("Text", dtEmp, "Employee_ID");
            txtFullName.DataBindings.Add("Text", dtEmp, "Full_Name");
            txtAddress.DataBindings.Add("Text", dtEmp, "Address");
            txtPhoneNumber.DataBindings.Add("Text", dtEmp, "Phone_Number");

            dvgEmployeeList.DataSource = dtEmp;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dvgEmployeeList.Rows.Count < 1)
            {
                MessageBox.Show("Please choose account to Delete");
                return;
            }
            if (LoginEmpID.Equals(txtEmployeeID.Text))
            {
                MessageBox.Show("You Can't delete information of your account ");
                return;
            }
            string ID = txtEmployeeID.Text;
            EmployeeBLL BLL = new EmployeeBLL();
            bool result = BLL.DeleteAccount(ID);
            if (result)
            {

                DataRow r = dtEmp.Rows.Find(ID);
                dtEmp.Rows.Remove(r);
                MessageBox.Show("Account delete success");
            }
            else
            {
                MessageBox.Show("Account delete fail");
                return;
            }
            btnGetAllAccount_Click(null, null);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (dvgEmployeeList.Rows.Count == 0)
            {
                MessageBox.Show("Please choose account to update");
                return;
            }
            this.Opacity = 0;
            UpdateEmployee updateEmp = new UpdateEmployee();
            updateEmp.ShowDialog(this);
            this.Opacity = 1;
            btnGetAllAccount_Click(null, null);
        }

        private void dvgEmployeeList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dvgEmployeeList.Rows.Count != 0)
            {
                return;
            }
            int selectedRowIndex = e.RowIndex;
            DataGridViewRow row = new DataGridViewRow();
            row = dvgEmployeeList.Rows[selectedRowIndex];

            string empID = row.Cells[0].Value.ToString();
        }

        private void EmployeeManage_Load(object sender, EventArgs e)
        {
            AdministratorForm admin = (AdministratorForm)(this.Owner);
            LoginEmpID = admin.EmpIDAccount();


        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ExportExcel.ExportToExcel(dvgEmployeeList, @"C:\Users\HuynhHQ\Desktop\", "Employees");
                MessageBox.Show("Export successful");
            }
            catch (Exception)
            {
                MessageBox.Show("Export Fail.");
                throw;

            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (dvgEmployeeList.Rows.Count == 0)
            {
                MessageBox.Show("Please get all employee before search.");
            }
            else
            {
                string searchValue = txtSearch.Text;
                EmployeeBLL empBLL = new EmployeeBLL();
                dtEmp = empBLL.SearchEmployeeNameByDataSet(searchValue).Tables[0];
                dtEmp.PrimaryKey = new DataColumn[] { dtEmp.Columns[0] };
                txtAddress.DataBindings.Clear();
                txtEmployeeID.DataBindings.Clear();
                txtFullName.DataBindings.Clear();
                txtPhoneNumber.DataBindings.Clear();

                txtEmployeeID.DataBindings.Add("Text", dtEmp, "Employee_ID");
                txtFullName.DataBindings.Add("Text", dtEmp, "Full_Name");
                txtAddress.DataBindings.Add("Text", dtEmp, "Address");
                txtPhoneNumber.DataBindings.Add("Text", dtEmp, "Phone_Number");

                dvgEmployeeList.DataSource = dtEmp;
            }
        }
    }
}
