﻿using BussinessLogicLayer;
using ShoppingWF.BUS;
using ShoppingWF.DAO;
using ShoppingWF.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShoppingWF.AdminForm
{
    public partial class UpdateEmployee : Form
    {
        public UpdateEmployee()
        {
            InitializeComponent();
            RoleModels.Roles = RoleBUS.getAllRoles();
            cbxRole.DataSource = RoleModels.Roles;
            //Role.Roles = RoleDAO.getAllRoles();
            //cbxRole.DataSource = Role.Roles;
            cbxRole.DisplayMember = "Role_Name";
            cbxRole.ValueMember = "Role_ID";
            
        }
        public string ID;
        private void UpdateEmployee_Load(object sender, EventArgs e)
        {
            EmployeeManage empM =(EmployeeManage)(this.Owner);
            string empID = empM.employeeIDUpdate();
            lbEmployeeID.Text = empID;
            ID = empID;
            
            Employee currentEmployee = EmployeeDAO.getEmployeeByID(empID);
            txtAddress.Text = currentEmployee.Address;
            txtPhoneNumber.Text = currentEmployee.Phone_Number;
            txtFullName.Text = currentEmployee.Full_Name;

            foreach (var item in RoleModels.Roles)
            {
                if (currentEmployee.Role_ID.Equals(item.Role_ID)){
                    cbxRole.SelectedItem = item;
                    break;
                }
            }
            //{
            //    if (currentEmployee.Role_ID.Equals(item.Role_ID)){
            //        cbxRole.SelectedItem = item;
            //        break;
            //    }
            //}
        }
        private bool inputvalidate()
        {
            Regex myregexName = new Regex("^(\\b[A-Za-z]*\\b(\\s+\\b[A-Za-z]*\\b)*(\\.[A-Za-z])?)$");
            Regex myregexPhone = new Regex(@"\d{10,11}");
            string name = txtFullName.Text;
            string phoneNumber = txtPhoneNumber.Text;
            Match matchName = myregexName.Match(name);
            Match matchPhone = myregexPhone.Match(phoneNumber);
            bool result = true;
            if (string.IsNullOrEmpty(txtFullName.Text.Trim()))
            {
                lblFullName.Text = "Full name is empty";
                lblFullName.Visible = true;
                result = false;
            }else if (!matchName.Success)
            {
                lblFullName.Text = "Full name is invalid";
                lblFullName.Visible = true;
                result = false;
            }
            else
            {               
                lblFullName.Visible = false;
            }

            if (cbxRole.Text.Trim().Equals(""))
            {
                result = false;
                lbRole.Visible = true;
            }
            else
            {
                lbRole.Visible = false;
            }
            if (txtAddress.Text.Trim().Equals(""))
            {
                result = false;
                lbAddress.Visible = true;
            }
            else
            {
                lbAddress.Visible = false;
            }
            if (txtPhoneNumber.Text.Trim().Equals(""))
            {
                lbPhone.Text = "Phone number is empty";
                result = false;
                lbPhone.Visible = true;

            }
            else if (!matchPhone.Success)
            {
                lbPhone.Text = "Phone number is invalid";
                lbPhone.Visible = true;
                result = false;
            }
            else
            {
                lbPhone.Visible = false;
            }
            return result;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if(!inputvalidate())
            {
                MessageBox.Show("Requirement is not complete");
            }
            else
            {
                EmployeeBLL BLL = new EmployeeBLL();
                bool checkCreate = BLL.UpdateEmployee(ID, txtFullName.Text, cbxRole.SelectedValue.ToString(),txtAddress.Text, txtPhoneNumber.Text);
                if (checkCreate)
                {
                    MessageBox.Show("Employee Update Success");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Employee Update fail");
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
