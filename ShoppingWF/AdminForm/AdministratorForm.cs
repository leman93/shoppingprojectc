﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BussinessLogicLayer;
using ShoppingWF.AdminForm;
using ShoppingWF.BUS;
using ShoppingWF.Models;

namespace ShoppingWF.AdminForm
{


    public partial class AdministratorForm : Form
    {

        Employee_AccountBLL accBLL = new Employee_AccountBLL();

        public string username = "";

        private static createAccout createAccount;

        public static DataTable dtAcc = new DataTable();
        public string usernameSelected;
        public string createdUsernameSelected;
        public string createdDateSelected;
        public int rowseleted = 0;
        public AdministratorForm(string username)
        {

            InitializeComponent();
            this.username = username;
        }


        public string EmpIDAccount()
        {
            Employee_AccountModels acc = Employee_AccountBUS.getEmployeeByUsername(username);
            string ID = acc.Employee_ID;
            return ID;
        }
        private void btnLogout_Click(object sender, EventArgs e)
        {

            this.Close();
            username = "";

            //frmShopping loginForm = new frmShopping();
            //loginForm.Visible = true;
        }



        private void AdministratorForm_Load(object sender, EventArgs e)
        {
            dvgAccountList.Refresh();
            frmShopping login = (frmShopping)(this.Owner);
            label3.ForeColor = System.Drawing.Color.Red;

            lbUsername.Text = "Welcome : " + username;
        }

        private void btnCreateAccount_Click(object sender, EventArgs e)
        {
            this.Opacity = 0;
            createAccount = new createAccout(username);
            createAccount.ShowDialog(this);
            this.Opacity = 1;
            btnGetAllAccount_Click(null, null);
        }

        private void btnGetAllAccount_Click(object sender, EventArgs e)
        {
            var listEmpAccount = Employee_AccountBUS.GetAllEmployee_Account();
            dtAcc = new DataTable();
            dtAcc.Columns.Add("Username");
            dtAcc.Columns.Add("Password");
            dtAcc.Columns.Add("Employee_ID");
            dtAcc.Columns.Add("Level_Access");
            dtAcc.Columns.Add("Enable");
            dtAcc.Columns.Add("Created_Date");
            dtAcc.Columns.Add("Created_By_Username");

            foreach (var item in listEmpAccount)
            {
                DataRow row = dtAcc.NewRow();
                row["Username"] = item.Username;
                row["Password"] = item.Password;
                row["Employee_ID"] = item.Employee_ID;
                row["Level_Access"] = item.Level_Access;
                row["Enable"] = item.Enable;
                row["Created_Date"] = item.Created_Date;
                row["Created_By_Username"] = item.Created_By_Username;
                dtAcc.Rows.Add(row);
            }
            dtAcc.PrimaryKey = new DataColumn[] { dtAcc.Columns[0] };

            txtUsername.DataBindings.Clear();
            txtEmployeeID.DataBindings.Clear();
            txtLevel_Access.DataBindings.Clear();

            txtUsername.DataBindings.Add("Text", dtAcc, "Username");
            txtEmployeeID.DataBindings.Add("Text", dtAcc, "Employee_ID");
            txtLevel_Access.DataBindings.Add("Text", dtAcc, "Level_Access");


            //      dvgAccountList.AutoGenerateColumns = false;
            dvgAccountList.DataSource = dtAcc;

        }





        private void dvgAccountList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dvgAccountList.Rows.Count != 0)
            {
                return;
            }
            int selectedRowIndex = e.RowIndex;
            rowseleted = e.RowIndex;
            DataGridViewRow row = new DataGridViewRow();
            row = dvgAccountList.Rows[selectedRowIndex];
            string username = row.Cells[0].Value.ToString();
            usernameSelected = username;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (dvgAccountList.Rows.Count == 0)
            {
                MessageBox.Show("Please choose account to update");
                return;
            }
            this.Opacity = 0;
            UpdateAccount form = new UpdateAccount(txtUsername.Text);
            form.ShowDialog(this);
            this.Opacity = 1;
            btnGetAllAccount_Click(null, null);
        }


        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dvgAccountList.Rows.Count < 1)
            {
                MessageBox.Show("Please choose account to Delete");
                return;
            }
            if (txtUsername.Text.Equals(this.username))
            {
                MessageBox.Show("You Cant delete your account");
                return;
            }
            string username = txtUsername.Text;
            bool result = Employee_AccountBUS.Delete(username);
            if (result)
            {

                DataRow r = dtAcc.Rows.Find(username);
                dtAcc.Rows.Remove(r);
                MessageBox.Show("Account Delete Success");
            }
            else
            {
                MessageBox.Show("Account Delete fail");
                return;
            }

            btnGetAllAccount_Click(null, null);
        }

        //MAN sua phan search lai
        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (dvgAccountList.Rows.Count == 0)
            {
                MessageBox.Show("Please get all account before search.");
            }
            else
            {
                string keyword = txtSearch.Text;
                Employee_AccountBLL BLL = new Employee_AccountBLL();
                List<Entities.Employee_Account> listEmp = new List<Entities.Employee_Account>();
                listEmp = BLL.searchbyUsername(keyword);

                txtUsername.DataBindings.Clear();
                txtEmployeeID.DataBindings.Clear();
                txtLevel_Access.DataBindings.Clear();

                txtUsername.DataBindings.Add("Text", listEmp, "Username");
                txtEmployeeID.DataBindings.Add("Text", listEmp, "Employee_ID");
                txtLevel_Access.DataBindings.Add("Text", listEmp, "Level_Access");

                dvgAccountList.DataSource = listEmp;
            }
        }

        private void dvgAccountList_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dvgAccountList_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (dvgAccountList.Columns.Count > 0)
            {
                dvgAccountList.Columns[1].Visible = false;

            }
        }
        frmShopping lg = new frmShopping();

        private void btnEmployee_Click(object sender, EventArgs e)
        {
            this.Opacity = 0;
            EmployeeManage empManager = new EmployeeManage();
            empManager.ShowDialog(this);
            this.Opacity = 1;
        }

        private void AdministratorForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            username = "";
        }
    }
}
