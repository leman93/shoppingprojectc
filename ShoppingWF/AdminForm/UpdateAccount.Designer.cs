﻿namespace ShoppingWF.AdminForm
{
    partial class UpdateAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbLAccess = new System.Windows.Forms.Label();
            this.cbxLevelAccess = new System.Windows.Forms.ComboBox();
            this.txtPassword2 = new System.Windows.Forms.TextBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.lbUsername = new System.Windows.Forms.Label();
            this.lbModifyBy = new System.Windows.Forms.Label();
            this.lbEmployeeID = new System.Windows.Forms.Label();
            this.passvalid2 = new System.Windows.Forms.Label();
            this.passvalid = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbLAccess
            // 
            this.lbLAccess.AutoSize = true;
            this.lbLAccess.ForeColor = System.Drawing.Color.Red;
            this.lbLAccess.Location = new System.Drawing.Point(218, 166);
            this.lbLAccess.Name = "lbLAccess";
            this.lbLAccess.Size = new System.Drawing.Size(147, 13);
            this.lbLAccess.TabIndex = 26;
            this.lbLAccess.Text = "Please choose Level_Access";
            this.lbLAccess.Visible = false;
            // 
            // cbxLevelAccess
            // 
            this.cbxLevelAccess.FormattingEnabled = true;
            this.cbxLevelAccess.Items.AddRange(new object[] {
            "admin",
            "manager",
            "orderchecker"});
            this.cbxLevelAccess.Location = new System.Drawing.Point(219, 142);
            this.cbxLevelAccess.Name = "cbxLevelAccess";
            this.cbxLevelAccess.Size = new System.Drawing.Size(121, 21);
            this.cbxLevelAccess.TabIndex = 1;
            // 
            // txtPassword2
            // 
            this.txtPassword2.Location = new System.Drawing.Point(221, 260);
            this.txtPassword2.Name = "txtPassword2";
            this.txtPassword2.PasswordChar = '*';
            this.txtPassword2.Size = new System.Drawing.Size(207, 20);
            this.txtPassword2.TabIndex = 3;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(219, 219);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(207, 20);
            this.txtPassword.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label7.Location = new System.Drawing.Point(93, 190);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 17);
            this.label7.TabIndex = 14;
            this.label7.Text = "Modify_by ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label4.Location = new System.Drawing.Point(93, 146);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Level_Access";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label8.Location = new System.Drawing.Point(93, 263);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(121, 17);
            this.label8.TabIndex = 9;
            this.label8.Text = "Confirm Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label3.Location = new System.Drawing.Point(93, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Employee_ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label2.Location = new System.Drawing.Point(93, 222);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 17);
            this.label2.TabIndex = 16;
            this.label2.Text = "Password";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label1.Location = new System.Drawing.Point(93, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "Username";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnUpdate.Location = new System.Drawing.Point(223, 310);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 4;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // lbUsername
            // 
            this.lbUsername.AutoSize = true;
            this.lbUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lbUsername.Location = new System.Drawing.Point(219, 68);
            this.lbUsername.Name = "lbUsername";
            this.lbUsername.Size = new System.Drawing.Size(79, 17);
            this.lbUsername.TabIndex = 28;
            this.lbUsername.Text = "username";
            // 
            // lbModifyBy
            // 
            this.lbModifyBy.AutoSize = true;
            this.lbModifyBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lbModifyBy.Location = new System.Drawing.Point(216, 190);
            this.lbModifyBy.Name = "lbModifyBy";
            this.lbModifyBy.Size = new System.Drawing.Size(73, 17);
            this.lbModifyBy.TabIndex = 29;
            this.lbModifyBy.Text = "modifyBy";
            // 
            // lbEmployeeID
            // 
            this.lbEmployeeID.AutoSize = true;
            this.lbEmployeeID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.lbEmployeeID.Location = new System.Drawing.Point(220, 110);
            this.lbEmployeeID.Name = "lbEmployeeID";
            this.lbEmployeeID.Size = new System.Drawing.Size(93, 17);
            this.lbEmployeeID.TabIndex = 29;
            this.lbEmployeeID.Text = "EmployeeID";
            // 
            // passvalid2
            // 
            this.passvalid2.AutoSize = true;
            this.passvalid2.ForeColor = System.Drawing.Color.Red;
            this.passvalid2.Location = new System.Drawing.Point(220, 283);
            this.passvalid2.Name = "passvalid2";
            this.passvalid2.Size = new System.Drawing.Size(107, 13);
            this.passvalid2.TabIndex = 30;
            this.passvalid2.Text = "Password is incorrect";
            this.passvalid2.Visible = false;
            // 
            // passvalid
            // 
            this.passvalid.AutoSize = true;
            this.passvalid.ForeColor = System.Drawing.Color.Red;
            this.passvalid.Location = new System.Drawing.Point(216, 244);
            this.passvalid.Name = "passvalid";
            this.passvalid.Size = new System.Drawing.Size(96, 13);
            this.passvalid.TabIndex = 31;
            this.passvalid.Text = "Password is invaild";
            this.passvalid.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label5.Location = new System.Drawing.Point(162, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(166, 25);
            this.label5.TabIndex = 46;
            this.label5.Text = "Update Account";
            // 
            // UpdateAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 345);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.passvalid);
            this.Controls.Add(this.passvalid2);
            this.Controls.Add(this.lbEmployeeID);
            this.Controls.Add(this.lbModifyBy);
            this.Controls.Add(this.lbUsername);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.lbLAccess);
            this.Controls.Add(this.cbxLevelAccess);
            this.Controls.Add(this.txtPassword2);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "UpdateAccount";
            this.Text = "UpdateAccount";
            this.Load += new System.EventHandler(this.UpdateAccount_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbLAccess;
        private System.Windows.Forms.ComboBox cbxLevelAccess;
        private System.Windows.Forms.TextBox txtPassword2;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Label lbUsername;
        private System.Windows.Forms.Label lbModifyBy;
        private System.Windows.Forms.Label lbEmployeeID;
        private System.Windows.Forms.Label passvalid2;
        private System.Windows.Forms.Label passvalid;
        private System.Windows.Forms.Label label5;
    }
}