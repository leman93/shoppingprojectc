﻿using ShoppingWF;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ShoppingWF.Models;
using ShoppingWF.BUS;
using System.Net.Mail;
using System.Net;
using ShoppingWF.OrderAndBillForm;

namespace ShoppingWF.OrderAndBillForm
{
    public partial class OrderForm : Form
    {
        private ProvinceModels province = null;
        private DistrictModels district = null;
        private TownModels town = null;
        public string username;
        DataTable dtOrder = null;
        public OrderForm(string username1)
        {
            InitializeComponent();
            this.username = username1;
            lblUsername.Text = "Welcome, " + this.username;
            this.WindowState = FormWindowState.Maximized;
        }

        private void OrderForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            new frmShopping().Show();
        }



        private void loadComboboxData()
        {
            ProvinceModels.Provinces = ProvinceBUS.getAllProvinces();
            cbxProvince.DataSource = ProvinceModels.Provinces;
            cbxProvince.DisplayMember = "Province_Name";
            cbxProvince.ValueMember = "Province_ID";

            cbxDistrict.DisplayMember = "District_Name";
            cbxDistrict.ValueMember = "District_ID";

            cbxTown.DisplayMember = "Town_Name";
            cbxTown.ValueMember = "Town_ID";
        }


        private void btnGetAllProducts_Click(object sender, EventArgs e)
        {
            loadComboboxData();
            var listOrder = Order_DetailBUS.GetAllOrders();

            cbxProvince.SelectedValue = listOrder[0].Province_ID;
            cbxDistrict.SelectedValue = listOrder[0].District_ID;
            cbxTown.SelectedValue = listOrder[0].Town_ID;

            dtOrder = new DataTable();
            dtOrder.Columns.Add("Order_ID");
            dtOrder.Columns.Add("Email");
            dtOrder.Columns.Add("Ordered_Date");
            dtOrder.Columns.Add("Delivered");
            dtOrder.Columns.Add("Cancelled_Order");
            dtOrder.Columns.Add("Reason_Cancel");
            dtOrder.Columns.Add("Order_Name");
            dtOrder.Columns.Add("Exact_Address");
            dtOrder.Columns.Add("Order_Phone");
            dtOrder.Columns.Add("Province_ID");
            dtOrder.Columns.Add("District_ID");
            dtOrder.Columns.Add("Town_ID");

            foreach (var item in listOrder)
            {
                DataRow row = dtOrder.NewRow();
                row["Order_ID"] = item.Order_ID;
                row["Email"] = item.Email;
                row["Ordered_Date"] = item.Ordered_Date;
                row["Delivered"] = item.Delivered;
                row["Cancelled_Order"] = item.Cancelled_Order;
                row["Reason_Cancel"] = item.Reason_Cancel;
                row["Order_Name"] = item.Order_Name;
                row["Exact_Address"] = item.Exact_Address;
                row["Order_Phone"] = item.Order_Phone;
                row["Province_ID"] = item.Province_ID;
                row["District_ID"] = item.District_ID;
                row["Town_ID"] = item.Town_ID;
                dtOrder.Rows.Add(row);
            }
            dtOrder.PrimaryKey = new DataColumn[] { dtOrder.Columns[0] };

            txtOrder_ID.DataBindings.Clear();
            txtOrderName.DataBindings.Clear();
            txtEmail.DataBindings.Clear();
            dtimeOrderDate.DataBindings.Clear();
            cbxDelivered.DataBindings.Clear();
            cbxCancelled.DataBindings.Clear();
            txtReasonCancel.DataBindings.Clear();
            txtExactAddress.DataBindings.Clear();
            txtOrdrePhone.DataBindings.Clear();
            txtProvinceId.DataBindings.Clear();
            txtDistrictID.DataBindings.Clear();
            txtTownId.DataBindings.Clear();

            txtOrder_ID.DataBindings.Add("Text", dtOrder, "Order_ID");
            txtOrderName.DataBindings.Add("Text", dtOrder, "Order_Name");
            txtEmail.DataBindings.Add("Text", dtOrder, "Email");
            dtimeOrderDate.DataBindings.Add("Text", dtOrder, "Ordered_Date");
            cbxDelivered.DataBindings.Add("Checked", dtOrder, "Delivered");
            cbxCancelled.DataBindings.Add("Checked", dtOrder, "Cancelled_Order");
            txtReasonCancel.DataBindings.Add("Text", dtOrder, "Reason_Cancel");
            txtExactAddress.DataBindings.Add("Text", dtOrder, "Exact_Address");
            txtOrdrePhone.DataBindings.Add("Text", dtOrder, "Order_Phone");
            txtProvinceId.DataBindings.Add("Text", dtOrder, "Province_ID");
            txtDistrictID.DataBindings.Add("Text", dtOrder, "District_ID");
            txtTownId.DataBindings.Add("Text", dtOrder, "Town_ID");
            dgvOrder.DataSource = dtOrder;
        }

        private void OrderForm_Load(object sender, EventArgs e)
        {
            dtimeSearchFrom.Value = DateTime.Now.AddDays(-30);
            dtimeSearchTo.Value = DateTime.Now;
        }

        private bool isValidData()
        {
            string email = txtEmail.Text;
            if (email.Equals(""))
            {
                MessageBox.Show("Email must not be empty!");
                return false;
            }

            province = cbxProvince.SelectedItem as ProvinceModels;
            if (province == null)
            {
                MessageBox.Show("Province is invalid~");
                return false;
            }

            district = cbxDistrict.SelectedItem as DistrictModels;
            if (district == null)
            {
                MessageBox.Show("District is invalid");
                return false;
            }

            town = cbxTown.SelectedItem as TownModels;
            if (town == null)
            {
                MessageBox.Show("Town is invalid");
                return false;
            }
            return true;
        }

        private void btnEditOrder_Click(object sender, EventArgs e)
        {
            int orderID = 0;
            Int32.TryParse(txtOrder_ID.Text, out orderID);
            if (orderID == 0)
            {
                MessageBox.Show("No Order choose!Try again.");
            }
            else
            {
                if (!isValidData()) return;

                string email = txtEmail.Text;
                bool isCancel = cbxCancelled.Checked;
                bool isDeli = cbxDelivered.Checked;
                Order_DetailsModels tmp = new Order_DetailsModels
                {
                    Order_ID = orderID,
                    Email = email,
                    Ordered_Date = dtimeOrderDate.Value,
                    Delivered = cbxDelivered.Checked,
                    Cancelled_Order = cbxCancelled.Checked,
                    Reason_Cancel = txtReasonCancel.Text,
                    Order_Name = txtOrderName.Text,
                    Exact_Address = txtExactAddress.Text,
                    Order_Phone = txtOrdrePhone.Text,
                    Province_ID = province.Province_ID,
                    District_ID = district.District_ID,
                    Town_ID = town.Town_ID,

                };

                bool result = Order_DetailBUS.Update(tmp);
                if (result)
                {
                    MessageBox.Show("Update successful!");
                    dgvOrder.Refresh();
                }
                else
                {
                    MessageBox.Show("Update failed!");
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Opacity = 0;
            BillForm frmBill = new BillForm(username);
            frmBill.ShowDialog(this);
            this.Opacity = 1;
        }

        private void btnAddtoBill_Click(object sender, EventArgs e)
        {
            int OrderId = 0;
            Int32.TryParse(txtOrder_ID.Text, out OrderId);
            if (OrderId == 0)
            {
                MessageBox.Show("Please choose Order!");
                return;
            }
            else
            {
                var check = BillBUS.GetBillByOrderId(OrderId);
                if (check != null)
                {
                    MessageBox.Show("This order have ID " + OrderId + " have already bill!");
                    return;
                }
                bool result = false;
                var order = Order_DetailBUS.GetOrder_DetailsById(OrderId);
                float totalPrice = 0;
                var listProEachOrder = Product_Order_DetailsBUS.GetAllProductByOrderID(OrderId);
                foreach (var item in listProEachOrder)
                {
                    var tempItem = ProductBUS.GetProductById(item.Product_ID);
                    if (tempItem.Products_Available < item.Order_Quantity)
                    {
                        MessageBox.Show("Product " + tempItem.Product_Name + " is sold out.");
                        return;
                    }
                    totalPrice += item.Order_Quantity * item.Price;
                }
                foreach (var item in listProEachOrder)
                {
                    var tempItem = ProductBUS.GetProductById(item.Product_ID);
                    tempItem.Products_Available -= item.Order_Quantity;
                    ProductBUS.Update(tempItem);
                }

                BillModels bill = new BillModels
                {
                    Order_ID = order.Order_ID,
                    Created_Date = DateTime.Now,
                    Total_Price = totalPrice,
                    Responsible_Man = username,
                    Cancelled_Bill = false

                };
                result = BillBUS.Insert(bill);
                if (result)
                {


                    var fromAddress = new MailAddress("mantest9371@gmail.com");
                    string password = "maiyeungocham";
                    var toAddress = new MailAddress(order.Email);
                    string body = "Your OrderID: " + order.Order_ID + " is arriving. \n";
                    foreach (var item in listProEachOrder)
                    {
                        var product = ProductBUS.GetProductById(item.Product_ID);
                        body += "   -) " + product.Product_Name + " . Quantity: " + item.Order_Quantity + ". Price each one: " + item.Price + ".\n";
                    }
                    body += "Thanks for shopping. Your order is our life!!!";

                    SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                    client.EnableSsl = true;

                    NetworkCredential myCreds = new NetworkCredential(fromAddress.Address, password, "");
                    client.Credentials = myCreds;

                    //SmtpClient client = new SmtpClient
                    //{
                    //    Host = "smtp.google.com",
                    //    Port = 587,
                    //    EnableSsl = true,
                    //    DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                    //    UseDefaultCredentials = false,
                    //    Credentials = new NetworkCredential(fromAddress.Address, password)
                    //};

                    var message = new MailMessage(fromAddress, toAddress);
                    message.Subject = "Your Order";
                    message.Body = body;
                    client.Send(message);

                    MessageBox.Show("Export to Bill successful.");
                }
                else
                {
                    MessageBox.Show("Export to Bill fail.");
                }


            }
        }

        private void dgvOrder_Click(object sender, EventArgs e)
        {
            if (dgvOrder.SelectedCells.Count < 1)
            {
                return;
            }

            var row = dgvOrder.CurrentRow;
            int provinceId = int.Parse(row.Cells[9].Value.ToString());
            cbxProvince.SelectedValue = provinceId;
            province = cbxProvince.SelectedItem as ProvinceModels;
            DistrictModels.Districts = null;
            DistrictModels.Districts = DistrictBUS.getDistrictsByProvinceId(province.Province_ID);

            int districtId = int.Parse(row.Cells[10].Value.ToString());
            cbxDistrict.SelectedValue = districtId;
            district = cbxDistrict.SelectedItem as DistrictModels;
            TownModels.Towns = null;
            TownModels.Towns = TownBUS.getAllTownsByDistrictId(district.District_ID);

            int townId = int.Parse(row.Cells[11].Value.ToString());
            cbxTown.SelectedValue = townId;
        }

        private void cbxProvince_SelectedIndexChanged(object sender, EventArgs e)
        {
            province = cbxProvince.SelectedItem as ProvinceModels;
            if (province == null)
            {
                MessageBox.Show("Province is invalid");
                cbxDistrict.DataSource = null;
                cbxTown.DataSource = null;
                return;
            }
            cbxDistrict.DataSource = DistrictBUS.getDistrictsByProvinceId(province.Province_ID);
            txtProvinceId.Text = province.Province_ID.ToString();
            district = cbxDistrict.SelectedItem as DistrictModels;
            cbxTown.DataSource = TownBUS.getAllTownsByDistrictId(district.District_ID);
        }

        private void cbxDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            district = cbxDistrict.SelectedItem as DistrictModels;
            if (district == null)
            {
                MessageBox.Show("District is invalid");
                cbxTown.DataSource = null;
                return;
            }
            txtDistrictID.Text = district.District_ID.ToString();
            cbxTown.DataSource = TownBUS.getAllTownsByDistrictId(district.District_ID);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            loadComboboxData();
            int orderId = 0;
            int.TryParse(txtSearchOrderID.Text, out orderId);

            var listOrder = Order_DetailBUS.Search(orderId, txtSearchUsername.Text, chbSearchCanceled.Checked, dtimeSearchFrom.Value, dtimeSearchTo.Value);

            cbxProvince.SelectedValue = listOrder[0].Province_ID;
            cbxDistrict.SelectedValue = listOrder[0].District_ID;
            cbxTown.SelectedValue = listOrder[0].Town_ID;

            dtOrder = new DataTable();
            dtOrder.Columns.Add("Order_ID");
            dtOrder.Columns.Add("Email");
            dtOrder.Columns.Add("Ordered_Date");
            dtOrder.Columns.Add("Delivered");
            dtOrder.Columns.Add("Cancelled_Order");
            dtOrder.Columns.Add("Reason_Cancel");
            dtOrder.Columns.Add("Order_Name");
            dtOrder.Columns.Add("Exact_Address");
            dtOrder.Columns.Add("Order_Phone");
            dtOrder.Columns.Add("Province_ID");
            dtOrder.Columns.Add("District_ID");
            dtOrder.Columns.Add("Town_ID");

            foreach (var item in listOrder)
            {
                DataRow row = dtOrder.NewRow();
                row["Order_ID"] = item.Order_ID;
                row["Email"] = item.Email;
                row["Ordered_Date"] = item.Ordered_Date;
                row["Delivered"] = item.Delivered;
                row["Cancelled_Order"] = item.Cancelled_Order;
                row["Reason_Cancel"] = item.Reason_Cancel;
                row["Order_Name"] = item.Order_Name;
                row["Exact_Address"] = item.Exact_Address;
                row["Order_Phone"] = item.Order_Phone;
                row["Province_ID"] = item.Province_ID;
                row["District_ID"] = item.District_ID;
                row["Town_ID"] = item.Town_ID;
                dtOrder.Rows.Add(row);
            }
            dtOrder.PrimaryKey = new DataColumn[] { dtOrder.Columns[0] };

            txtOrder_ID.DataBindings.Clear();
            txtOrderName.DataBindings.Clear();
            txtEmail.DataBindings.Clear();
            dtimeOrderDate.DataBindings.Clear();
            cbxDelivered.DataBindings.Clear();
            cbxCancelled.DataBindings.Clear();
            txtReasonCancel.DataBindings.Clear();
            txtExactAddress.DataBindings.Clear();
            txtOrdrePhone.DataBindings.Clear();
            txtProvinceId.DataBindings.Clear();
            txtDistrictID.DataBindings.Clear();
            txtTownId.DataBindings.Clear();

            txtOrder_ID.DataBindings.Add("Text", dtOrder, "Order_ID");
            txtOrderName.DataBindings.Add("Text", dtOrder, "Order_Name");
            txtEmail.DataBindings.Add("Text", dtOrder, "Email");
            dtimeOrderDate.DataBindings.Add("Text", dtOrder, "Ordered_Date");
            cbxDelivered.DataBindings.Add("Checked", dtOrder, "Delivered");
            cbxCancelled.DataBindings.Add("Checked", dtOrder, "Cancelled_Order");
            txtReasonCancel.DataBindings.Add("Text", dtOrder, "Reason_Cancel");
            txtExactAddress.DataBindings.Add("Text", dtOrder, "Exact_Address");
            txtOrdrePhone.DataBindings.Add("Text", dtOrder, "Order_Phone");
            txtProvinceId.DataBindings.Add("Text", dtOrder, "Province_ID");
            txtDistrictID.DataBindings.Add("Text", dtOrder, "District_ID");
            txtTownId.DataBindings.Add("Text", dtOrder, "Town_ID");
            dgvOrder.DataSource = dtOrder;
        }

        private void btnListProductById_Click(object sender, EventArgs e)
        {
            int OrderId = 0;
            Int32.TryParse(txtOrder_ID.Text, out OrderId);
            if (OrderId == 0)
            {
                MessageBox.Show("Please choose Order!");
                return;
            }
            else
            {
                this.Opacity = 0;
                var frmList = new UpdateBill(OrderId);
                frmList.ShowDialog(this);
                this.Opacity = 1;
            }
        }
    }
}
