﻿namespace ShoppingWF.OrderAndBillForm
{
    partial class OrderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbResponsible = new System.Windows.Forms.Label();
            this.lbCreatedDate = new System.Windows.Forms.Label();
            this.lbOrderID = new System.Windows.Forms.Label();
            this.dtimeOrderDate = new System.Windows.Forms.DateTimePicker();
            this.txtDistrictID = new System.Windows.Forms.TextBox();
            this.txtProvinceId = new System.Windows.Forms.TextBox();
            this.txtExactAddress = new System.Windows.Forms.TextBox();
            this.txtRecieverPhone = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtOrderName = new System.Windows.Forms.TextBox();
            this.txtTownId = new System.Windows.Forms.TextBox();
            this.txtOrder_ID = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtOrdrePhone = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblUsername = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.btnGetAllProducts = new System.Windows.Forms.Button();
            this.btnEditOrder = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.dgvOrder = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.txtReasonCancel = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnAddtoBill = new System.Windows.Forms.Button();
            this.cbxProvince = new System.Windows.Forms.ComboBox();
            this.cbxDistrict = new System.Windows.Forms.ComboBox();
            this.cbxTown = new System.Windows.Forms.ComboBox();
            this.cbxCancelled = new System.Windows.Forms.CheckBox();
            this.cbxDelivered = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtSearchOrderID = new System.Windows.Forms.TextBox();
            this.txtSearchUsername = new System.Windows.Forms.TextBox();
            this.chbSearchCanceled = new System.Windows.Forms.CheckBox();
            this.dtimeSearchFrom = new System.Windows.Forms.DateTimePicker();
            this.dtimeSearchTo = new System.Windows.Forms.DateTimePicker();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnListProductById = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrder)).BeginInit();
            this.SuspendLayout();
            // 
            // lbResponsible
            // 
            this.lbResponsible.AutoSize = true;
            this.lbResponsible.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbResponsible.Location = new System.Drawing.Point(339, 441);
            this.lbResponsible.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbResponsible.Name = "lbResponsible";
            this.lbResponsible.Size = new System.Drawing.Size(206, 20);
            this.lbResponsible.TabIndex = 36;
            this.lbResponsible.Text = "Responsible Man is empty";
            this.lbResponsible.Visible = false;
            // 
            // lbCreatedDate
            // 
            this.lbCreatedDate.AutoSize = true;
            this.lbCreatedDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCreatedDate.Location = new System.Drawing.Point(339, 255);
            this.lbCreatedDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbCreatedDate.Name = "lbCreatedDate";
            this.lbCreatedDate.Size = new System.Drawing.Size(183, 20);
            this.lbCreatedDate.TabIndex = 34;
            this.lbCreatedDate.Text = "Ordered_Date is empty";
            this.lbCreatedDate.Visible = false;
            // 
            // lbOrderID
            // 
            this.lbOrderID.AutoSize = true;
            this.lbOrderID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOrderID.Location = new System.Drawing.Point(340, 151);
            this.lbOrderID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbOrderID.Name = "lbOrderID";
            this.lbOrderID.Size = new System.Drawing.Size(142, 20);
            this.lbOrderID.TabIndex = 33;
            this.lbOrderID.Text = "Order ID is empty";
            this.lbOrderID.Visible = false;
            // 
            // dtimeOrderDate
            // 
            this.dtimeOrderDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtimeOrderDate.Location = new System.Drawing.Point(344, 225);
            this.dtimeOrderDate.Margin = new System.Windows.Forms.Padding(4);
            this.dtimeOrderDate.Name = "dtimeOrderDate";
            this.dtimeOrderDate.Size = new System.Drawing.Size(225, 27);
            this.dtimeOrderDate.TabIndex = 30;
            // 
            // txtDistrictID
            // 
            this.txtDistrictID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDistrictID.Location = new System.Drawing.Point(1135, 351);
            this.txtDistrictID.Margin = new System.Windows.Forms.Padding(4);
            this.txtDistrictID.Name = "txtDistrictID";
            this.txtDistrictID.Size = new System.Drawing.Size(15, 27);
            this.txtDistrictID.TabIndex = 27;
            this.txtDistrictID.Visible = false;
            // 
            // txtProvinceId
            // 
            this.txtProvinceId.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProvinceId.Location = new System.Drawing.Point(1135, 287);
            this.txtProvinceId.Margin = new System.Windows.Forms.Padding(4);
            this.txtProvinceId.Name = "txtProvinceId";
            this.txtProvinceId.Size = new System.Drawing.Size(15, 27);
            this.txtProvinceId.TabIndex = 29;
            this.txtProvinceId.Visible = false;
            // 
            // txtExactAddress
            // 
            this.txtExactAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtExactAddress.Location = new System.Drawing.Point(344, 291);
            this.txtExactAddress.Margin = new System.Windows.Forms.Padding(4);
            this.txtExactAddress.Name = "txtExactAddress";
            this.txtExactAddress.Size = new System.Drawing.Size(225, 27);
            this.txtExactAddress.TabIndex = 20;
            // 
            // txtRecieverPhone
            // 
            this.txtRecieverPhone.AutoSize = true;
            this.txtRecieverPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRecieverPhone.Location = new System.Drawing.Point(814, 355);
            this.txtRecieverPhone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.txtRecieverPhone.Name = "txtRecieverPhone";
            this.txtRecieverPhone.Size = new System.Drawing.Size(90, 20);
            this.txtRecieverPhone.TabIndex = 17;
            this.txtRecieverPhone.Text = "District_ID";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(802, 287);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 20);
            this.label11.TabIndex = 7;
            this.label11.Text = "Province_ID";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(200, 291);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(122, 20);
            this.label10.TabIndex = 16;
            this.label10.Text = "Exact_Address";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(822, 163);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 20);
            this.label9.TabIndex = 15;
            this.label9.Text = "Delivered";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(767, 120);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(135, 20);
            this.label8.TabIndex = 14;
            this.label8.Text = "Cancelled_Order";
            // 
            // txtOrderName
            // 
            this.txtOrderName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrderName.Location = new System.Drawing.Point(343, 348);
            this.txtOrderName.Margin = new System.Windows.Forms.Padding(4);
            this.txtOrderName.Name = "txtOrderName";
            this.txtOrderName.Size = new System.Drawing.Size(225, 27);
            this.txtOrderName.TabIndex = 24;
            // 
            // txtTownId
            // 
            this.txtTownId.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTownId.Location = new System.Drawing.Point(1135, 415);
            this.txtTownId.Margin = new System.Windows.Forms.Padding(4);
            this.txtTownId.Name = "txtTownId";
            this.txtTownId.Size = new System.Drawing.Size(15, 27);
            this.txtTownId.TabIndex = 25;
            this.txtTownId.Visible = false;
            // 
            // txtOrder_ID
            // 
            this.txtOrder_ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrder_ID.Location = new System.Drawing.Point(344, 120);
            this.txtOrder_ID.Margin = new System.Windows.Forms.Padding(4);
            this.txtOrder_ID.Name = "txtOrder_ID";
            this.txtOrder_ID.ReadOnly = true;
            this.txtOrder_ID.Size = new System.Drawing.Size(225, 27);
            this.txtOrder_ID.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(827, 423);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 20);
            this.label6.TabIndex = 12;
            this.label6.Text = "Town_ID";
            // 
            // txtOrdrePhone
            // 
            this.txtOrdrePhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtOrdrePhone.Location = new System.Drawing.Point(343, 412);
            this.txtOrdrePhone.Margin = new System.Windows.Forms.Padding(4);
            this.txtOrdrePhone.Name = "txtOrdrePhone";
            this.txtOrdrePhone.Size = new System.Drawing.Size(225, 27);
            this.txtOrdrePhone.TabIndex = 21;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(214, 351);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 20);
            this.label4.TabIndex = 11;
            this.label4.Text = "Order_Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(211, 415);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "Order_Phone";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(244, 123);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "Order_ID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(204, 225);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 20);
            this.label3.TabIndex = 8;
            this.label3.Text = "Ordered_Date";
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Font = new System.Drawing.Font("Yu Mincho", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lblUsername.Location = new System.Drawing.Point(53, 31);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(105, 43);
            this.lblUsername.TabIndex = 39;
            this.lblUsername.Text = "label1";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Yu Mincho", 28.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label16.Location = new System.Drawing.Point(659, 17);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(338, 62);
            this.label16.TabIndex = 40;
            this.label16.Text = "Manage Order";
            // 
            // btnGetAllProducts
            // 
            this.btnGetAllProducts.Location = new System.Drawing.Point(1268, 100);
            this.btnGetAllProducts.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnGetAllProducts.Name = "btnGetAllProducts";
            this.btnGetAllProducts.Size = new System.Drawing.Size(163, 63);
            this.btnGetAllProducts.TabIndex = 41;
            this.btnGetAllProducts.Text = "Get All Orders";
            this.btnGetAllProducts.UseVisualStyleBackColor = true;
            this.btnGetAllProducts.Click += new System.EventHandler(this.btnGetAllProducts_Click);
            // 
            // btnEditOrder
            // 
            this.btnEditOrder.Location = new System.Drawing.Point(1268, 185);
            this.btnEditOrder.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnEditOrder.Name = "btnEditOrder";
            this.btnEditOrder.Size = new System.Drawing.Size(163, 63);
            this.btnEditOrder.TabIndex = 43;
            this.btnEditOrder.Text = "Edit Order";
            this.btnEditOrder.UseVisualStyleBackColor = true;
            this.btnEditOrder.Click += new System.EventHandler(this.btnEditOrder_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(1462, 185);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(171, 63);
            this.button3.TabIndex = 44;
            this.button3.Text = "Go to Manage Bills";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // dgvOrder
            // 
            this.dgvOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOrder.Location = new System.Drawing.Point(65, 677);
            this.dgvOrder.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvOrder.MultiSelect = false;
            this.dgvOrder.Name = "dgvOrder";
            this.dgvOrder.ReadOnly = true;
            this.dgvOrder.RowTemplate.Height = 24;
            this.dgvOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvOrder.Size = new System.Drawing.Size(1634, 221);
            this.dgvOrder.TabIndex = 45;
            this.dgvOrder.Click += new System.EventHandler(this.dgvOrder_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(271, 184);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 20);
            this.label1.TabIndex = 46;
            this.label1.Text = "Email";
            // 
            // txtEmail
            // 
            this.txtEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.Location = new System.Drawing.Point(344, 181);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(4);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.ReadOnly = true;
            this.txtEmail.Size = new System.Drawing.Size(225, 27);
            this.txtEmail.TabIndex = 47;
            // 
            // txtReasonCancel
            // 
            this.txtReasonCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtReasonCancel.Location = new System.Drawing.Point(924, 221);
            this.txtReasonCancel.Margin = new System.Windows.Forms.Padding(4);
            this.txtReasonCancel.Name = "txtReasonCancel";
            this.txtReasonCancel.Size = new System.Drawing.Size(203, 27);
            this.txtReasonCancel.TabIndex = 49;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(779, 225);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 20);
            this.label7.TabIndex = 48;
            this.label7.Text = "Reason Cancel";
            // 
            // btnAddtoBill
            // 
            this.btnAddtoBill.Location = new System.Drawing.Point(1462, 100);
            this.btnAddtoBill.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddtoBill.Name = "btnAddtoBill";
            this.btnAddtoBill.Size = new System.Drawing.Size(171, 63);
            this.btnAddtoBill.TabIndex = 50;
            this.btnAddtoBill.Text = "Export Bill By Order";
            this.btnAddtoBill.UseVisualStyleBackColor = true;
            this.btnAddtoBill.Click += new System.EventHandler(this.btnAddtoBill_Click);
            // 
            // cbxProvince
            // 
            this.cbxProvince.FormattingEnabled = true;
            this.cbxProvince.Location = new System.Drawing.Point(926, 287);
            this.cbxProvince.Margin = new System.Windows.Forms.Padding(4);
            this.cbxProvince.Name = "cbxProvince";
            this.cbxProvince.Size = new System.Drawing.Size(201, 24);
            this.cbxProvince.TabIndex = 51;
            this.cbxProvince.SelectedIndexChanged += new System.EventHandler(this.cbxProvince_SelectedIndexChanged);
            // 
            // cbxDistrict
            // 
            this.cbxDistrict.FormattingEnabled = true;
            this.cbxDistrict.Location = new System.Drawing.Point(926, 351);
            this.cbxDistrict.Margin = new System.Windows.Forms.Padding(4);
            this.cbxDistrict.Name = "cbxDistrict";
            this.cbxDistrict.Size = new System.Drawing.Size(201, 24);
            this.cbxDistrict.TabIndex = 51;
            this.cbxDistrict.SelectedIndexChanged += new System.EventHandler(this.cbxDistrict_SelectedIndexChanged);
            // 
            // cbxTown
            // 
            this.cbxTown.FormattingEnabled = true;
            this.cbxTown.Location = new System.Drawing.Point(926, 419);
            this.cbxTown.Margin = new System.Windows.Forms.Padding(4);
            this.cbxTown.Name = "cbxTown";
            this.cbxTown.Size = new System.Drawing.Size(201, 24);
            this.cbxTown.TabIndex = 51;
            // 
            // cbxCancelled
            // 
            this.cbxCancelled.AutoSize = true;
            this.cbxCancelled.Location = new System.Drawing.Point(926, 123);
            this.cbxCancelled.Name = "cbxCancelled";
            this.cbxCancelled.Size = new System.Drawing.Size(18, 17);
            this.cbxCancelled.TabIndex = 52;
            this.cbxCancelled.UseVisualStyleBackColor = true;
            // 
            // cbxDelivered
            // 
            this.cbxDelivered.AutoSize = true;
            this.cbxDelivered.Location = new System.Drawing.Point(926, 167);
            this.cbxDelivered.Name = "cbxDelivered";
            this.cbxDelivered.Size = new System.Drawing.Size(18, 17);
            this.cbxDelivered.TabIndex = 53;
            this.cbxDelivered.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(1287, 389);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 20);
            this.label12.TabIndex = 54;
            this.label12.Text = "OrderID";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(1275, 424);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 20);
            this.label13.TabIndex = 54;
            this.label13.Text = "Username";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(1282, 462);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(79, 20);
            this.label14.TabIndex = 54;
            this.label14.Text = "Canceled";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(1313, 499);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(48, 20);
            this.label15.TabIndex = 54;
            this.label15.Text = "From";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(1328, 528);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(28, 20);
            this.label17.TabIndex = 54;
            this.label17.Text = "To";
            // 
            // txtSearchOrderID
            // 
            this.txtSearchOrderID.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchOrderID.Location = new System.Drawing.Point(1363, 386);
            this.txtSearchOrderID.Margin = new System.Windows.Forms.Padding(4);
            this.txtSearchOrderID.Name = "txtSearchOrderID";
            this.txtSearchOrderID.Size = new System.Drawing.Size(270, 27);
            this.txtSearchOrderID.TabIndex = 55;
            // 
            // txtSearchUsername
            // 
            this.txtSearchUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchUsername.Location = new System.Drawing.Point(1363, 421);
            this.txtSearchUsername.Margin = new System.Windows.Forms.Padding(4);
            this.txtSearchUsername.Name = "txtSearchUsername";
            this.txtSearchUsername.Size = new System.Drawing.Size(270, 27);
            this.txtSearchUsername.TabIndex = 55;
            // 
            // chbSearchCanceled
            // 
            this.chbSearchCanceled.AutoSize = true;
            this.chbSearchCanceled.Location = new System.Drawing.Point(1363, 465);
            this.chbSearchCanceled.Name = "chbSearchCanceled";
            this.chbSearchCanceled.Size = new System.Drawing.Size(18, 17);
            this.chbSearchCanceled.TabIndex = 56;
            this.chbSearchCanceled.UseVisualStyleBackColor = true;
            // 
            // dtimeSearchFrom
            // 
            this.dtimeSearchFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtimeSearchFrom.Location = new System.Drawing.Point(1363, 494);
            this.dtimeSearchFrom.Margin = new System.Windows.Forms.Padding(4);
            this.dtimeSearchFrom.Name = "dtimeSearchFrom";
            this.dtimeSearchFrom.Size = new System.Drawing.Size(270, 27);
            this.dtimeSearchFrom.TabIndex = 57;
            // 
            // dtimeSearchTo
            // 
            this.dtimeSearchTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtimeSearchTo.Location = new System.Drawing.Point(1363, 526);
            this.dtimeSearchTo.Margin = new System.Windows.Forms.Padding(4);
            this.dtimeSearchTo.Name = "dtimeSearchTo";
            this.dtimeSearchTo.Size = new System.Drawing.Size(270, 27);
            this.dtimeSearchTo.TabIndex = 58;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(1363, 567);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(96, 34);
            this.btnSearch.TabIndex = 59;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnListProductById
            // 
            this.btnListProductById.Location = new System.Drawing.Point(1268, 267);
            this.btnListProductById.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnListProductById.Name = "btnListProductById";
            this.btnListProductById.Size = new System.Drawing.Size(365, 63);
            this.btnListProductById.TabIndex = 60;
            this.btnListProductById.Text = "Manage Product Each Order";
            this.btnListProductById.UseVisualStyleBackColor = true;
            this.btnListProductById.Click += new System.EventHandler(this.btnListProductById_Click);
            // 
            // OrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1902, 1033);
            this.Controls.Add(this.btnListProductById);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.dtimeSearchTo);
            this.Controls.Add(this.dtimeSearchFrom);
            this.Controls.Add(this.chbSearchCanceled);
            this.Controls.Add(this.txtSearchUsername);
            this.Controls.Add(this.txtSearchOrderID);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.cbxDelivered);
            this.Controls.Add(this.cbxCancelled);
            this.Controls.Add(this.cbxTown);
            this.Controls.Add(this.cbxDistrict);
            this.Controls.Add(this.cbxProvince);
            this.Controls.Add(this.btnAddtoBill);
            this.Controls.Add(this.txtReasonCancel);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgvOrder);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btnEditOrder);
            this.Controls.Add(this.btnGetAllProducts);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.lbResponsible);
            this.Controls.Add(this.lbCreatedDate);
            this.Controls.Add(this.lbOrderID);
            this.Controls.Add(this.dtimeOrderDate);
            this.Controls.Add(this.txtDistrictID);
            this.Controls.Add(this.txtProvinceId);
            this.Controls.Add(this.txtExactAddress);
            this.Controls.Add(this.txtRecieverPhone);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtOrderName);
            this.Controls.Add(this.txtTownId);
            this.Controls.Add(this.txtOrder_ID);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtOrdrePhone);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "OrderForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "District_ID";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OrderForm_FormClosed);
            this.Load += new System.EventHandler(this.OrderForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrder)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lbResponsible;
        private System.Windows.Forms.Label lbCreatedDate;
        private System.Windows.Forms.Label lbOrderID;
        private System.Windows.Forms.DateTimePicker dtimeOrderDate;
        private System.Windows.Forms.TextBox txtDistrictID;
        private System.Windows.Forms.TextBox txtProvinceId;
        private System.Windows.Forms.TextBox txtExactAddress;
        private System.Windows.Forms.Label txtRecieverPhone;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtOrderName;
        private System.Windows.Forms.TextBox txtTownId;
        private System.Windows.Forms.TextBox txtOrder_ID;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtOrdrePhone;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btnGetAllProducts;
        private System.Windows.Forms.Button btnEditOrder;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DataGridView dgvOrder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.TextBox txtReasonCancel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnAddtoBill;
        private System.Windows.Forms.ComboBox cbxProvince;
        private System.Windows.Forms.ComboBox cbxDistrict;
        private System.Windows.Forms.ComboBox cbxTown;
        private System.Windows.Forms.CheckBox cbxCancelled;
        private System.Windows.Forms.CheckBox cbxDelivered;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtSearchOrderID;
        private System.Windows.Forms.TextBox txtSearchUsername;
        private System.Windows.Forms.CheckBox chbSearchCanceled;
        private System.Windows.Forms.DateTimePicker dtimeSearchFrom;
        private System.Windows.Forms.DateTimePicker dtimeSearchTo;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnListProductById;
    }
}