﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ShoppingWF.BUS;
using ShoppingWF.Models;
using ShoppingWF;
using ShoppingWF.DAO;

namespace ShoppingWF.OrderAndBillForm
{
    public partial class BillForm : Form
    {
        string username;
        DataTable dtBill = null;
        public BillForm(string username)
        {
            InitializeComponent();
            this.username = username;
            lblUsername.Text = "Welcome, " + username;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dtimeSearcFrom.Value = DateTime.Now.AddDays(-30);
            dtimeSearchTo.Value = DateTime.Now;
        }

        private void btnGetAllBill_Click(object sender, EventArgs e)
        {
            var listBill = BillBUS.GetAllBills();

            dtBill = new DataTable();
            dtBill.Columns.Add("Bill_ID");
            dtBill.Columns.Add("Order_ID");
            dtBill.Columns.Add("Created_Date");
            dtBill.Columns.Add("Total_Price");
            dtBill.Columns.Add("Responsible_Man");
            dtBill.Columns.Add("Delivered_Date");
            dtBill.Columns.Add("Received_Date");
            dtBill.Columns.Add("Cancelled_Bill");
            dtBill.Columns.Add("Reason_Cancel");
            dtBill.Columns.Add("Shipper_ID");
            dtBill.Columns.Add("Reciever_Name");
            dtBill.Columns.Add("Reciever_Phone");
            dtBill.Columns.Add("Reciever_Addresss");

            foreach (var item in listBill)
            {
                DataRow row = dtBill.NewRow();
                row["Bill_ID"] = item.Bill_ID;
                row["Order_ID"] = item.Order_ID;
                row["Created_Date"] = item.Created_Date;
                row["Total_Price"] = item.Total_Price;
                row["Responsible_Man"] = item.Responsible_Man;
                row["Delivered_Date"] = item.Delivered_Date;
                row["Received_Date"] = item.Received_Date;
                row["Cancelled_Bill"] = item.Cancelled_Bill;
                row["Reason_Cancel"] = item.Reason_Cancel;
                row["Shipper_ID"] = item.Shipper_ID;
                row["Reciever_Name"] = item.Reciever_Name;
                row["Reciever_Phone"] = item.Reciever_Phone;
                row["Reciever_Addresss"] = item.Reciever_Addresss;
                dtBill.Rows.Add(row);
            }

            dtBill.PrimaryKey = new DataColumn[] { dtBill.Columns[0] };

            txtBillID.DataBindings.Clear();
            txtOrder_ID.DataBindings.Clear();
            txtCreated_Date.DataBindings.Clear();
            txtTotal_Price.DataBindings.Clear();
            txtResponsible_Man.DataBindings.Clear();
            TimeDeliveredDate.DataBindings.Clear();
            TimeReceiveredDate.DataBindings.Clear();
            txtReason_Cancel.DataBindings.Clear();
            txtShipperID.DataBindings.Clear();
            txtRecieverName.DataBindings.Clear();
            txtRecieverPhone.DataBindings.Clear();
            txtRecieverAddress.DataBindings.Clear();

            txtBillID.DataBindings.Add("Text", dtBill, "Bill_ID");
            txtOrder_ID.DataBindings.Add("Text", dtBill, "Order_ID");
            txtCreated_Date.DataBindings.Add("Text", dtBill, "Created_Date");
            txtTotal_Price.DataBindings.Add("Text", dtBill, "Total_Price");
            txtResponsible_Man.DataBindings.Add("Text", dtBill, "Responsible_Man");
            TimeDeliveredDate.DataBindings.Add("Text", dtBill, "Delivered_Date");
            TimeReceiveredDate.DataBindings.Add("Text", dtBill, "Received_Date");
            txtReason_Cancel.DataBindings.Add("Text", dtBill, "Reason_Cancel");
            txtShipperID.DataBindings.Add("Text", dtBill, "Shipper_ID");
            txtRecieverName.DataBindings.Add("Text", dtBill, "Reciever_Name");
            txtRecieverPhone.DataBindings.Add("Text", dtBill, "Reciever_Phone");
            txtRecieverAddress.DataBindings.Add("Text", dtBill, "Reciever_Addresss");

            dgvBill.DataSource = dtBill;
        }
        private void checkValidate()
        {
            if (txtBillID.Text.Trim().Equals(""))
            {
                lbBillID.Visible = true;
            }else
            {
                lbBillID.Visible = false;
            }

            if (txtOrder_ID.Text.Trim().Equals(""))
            {
                lbOrderID.Visible = true;
            }
            else
            {
                lbOrderID.Visible = false;
            }

            if (txtCreated_Date.Text.Trim().Equals(""))
            {
                lbCreatedDate.Visible = true;
            }
            else
            {
                lbCreatedDate.Visible = false;
            }

            if (txtResponsible_Man.Text.Trim().Equals(""))
            {
                lbResponsible.Visible = true;
            }
            else
            {
                lbResponsible.Visible = false;
            }

            if (txtTotal_Price.Text.Trim().Equals(""))
            {
                lbTotalPrice.Visible = true;
            }
            else
            {
                lbTotalPrice.Visible = false;
            }

        }
        

        private void BillForm_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void btnDeleteBill_Click(object sender, EventArgs e)
        {
            int billId = 0;
            int.TryParse(txtBillID.Text, out billId);
            if(billId == 0)
            {
                MessageBox.Show("Please choose bill to Delete.");
                return;
            }
            bool result = BillBUS.Delete(billId);
            if(result == true)
            {
                MessageBox.Show("Delete successfully!");
            }else
            {
                MessageBox.Show("Delete failed.");
            }
        }

        private void btnUpdateBill_Click(object sender, EventArgs e)
        {
            int billId = 0;
            int.TryParse(txtBillID.Text, out billId);
            if (billId == 0)
            {
                MessageBox.Show("Please choose bill to Update.");
                return;
            }

            float totalPrice = 0;
            float.TryParse(txtTotal_Price.Text, out totalPrice);
            if(totalPrice ==0)
            {
                MessageBox.Show("invalid Price!");
                return;
            }
            BillModels tmp = new BillModels
            {
                Bill_ID = billId,
                Order_ID = int.Parse(txtOrder_ID.Text),
                Created_Date = DateTime.Parse(txtCreated_Date.Text),
                Total_Price = totalPrice,
                Responsible_Man =txtResponsible_Man.Text,
                Delivered_Date = TimeDeliveredDate.Value,
                Received_Date = TimeDeliveredDate.Value,
                Reason_Cancel = txtReason_Cancel.Text,
                Shipper_ID = txtShipperID.Text,
                Reciever_Name = txtRecieverName.Text,
                Reciever_Phone = txtRecieverPhone.Text,
                Reciever_Addresss =txtRecieverAddress.Text
            };
            bool result = false;
            result = BillBUS.Update(tmp);
            if (result)
            {
                MessageBox.Show("Update successful.");
            }
            else
            {
                MessageBox.Show("Update fail.");
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ExportExcel.ExportToExcel(dgvBill, @"C:\Users\Hoang Man-PC\Desktop\", "Bills");
                MessageBox.Show("Export successful");
            }
            catch (Exception)
            {
                MessageBox.Show("Export Fail.");
                throw;

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int billId = 0;
            int orderId = 0;
            int.TryParse(txtSearchBillId.Text, out billId);
            int.TryParse(txtSearchOrderId.Text, out orderId);

            var listBill = BillBUS.Search(billId,orderId, chbSearchCancel.Checked,dtimeSearcFrom.Value,dtimeSearchTo.Value);

            dtBill = new DataTable();
            dtBill.Columns.Add("Bill_ID");
            dtBill.Columns.Add("Order_ID");
            dtBill.Columns.Add("Created_Date");
            dtBill.Columns.Add("Total_Price");
            dtBill.Columns.Add("Responsible_Man");
            dtBill.Columns.Add("Delivered_Date");
            dtBill.Columns.Add("Received_Date");
            dtBill.Columns.Add("Cancelled_Bill");
            dtBill.Columns.Add("Reason_Cancel");
            dtBill.Columns.Add("Shipper_ID");
            dtBill.Columns.Add("Reciever_Name");
            dtBill.Columns.Add("Reciever_Phone");
            dtBill.Columns.Add("Reciever_Addresss");

            foreach (var item in listBill)
            {
                DataRow row = dtBill.NewRow();
                row["Bill_ID"] = item.Bill_ID;
                row["Order_ID"] = item.Order_ID;
                row["Created_Date"] = item.Created_Date;
                row["Total_Price"] = item.Total_Price;
                row["Responsible_Man"] = item.Responsible_Man;
                row["Delivered_Date"] = item.Delivered_Date;
                row["Received_Date"] = item.Received_Date;
                row["Cancelled_Bill"] = item.Cancelled_Bill;
                row["Reason_Cancel"] = item.Reason_Cancel;
                row["Shipper_ID"] = item.Shipper_ID;
                row["Reciever_Name"] = item.Reciever_Name;
                row["Reciever_Phone"] = item.Reciever_Phone;
                row["Reciever_Addresss"] = item.Reciever_Addresss;
                dtBill.Rows.Add(row);
            }

            dtBill.PrimaryKey = new DataColumn[] { dtBill.Columns[0] };

            txtBillID.DataBindings.Clear();
            txtOrder_ID.DataBindings.Clear();
            txtCreated_Date.DataBindings.Clear();
            txtTotal_Price.DataBindings.Clear();
            txtResponsible_Man.DataBindings.Clear();
            TimeDeliveredDate.DataBindings.Clear();
            TimeReceiveredDate.DataBindings.Clear();
            txtReason_Cancel.DataBindings.Clear();
            txtShipperID.DataBindings.Clear();
            txtRecieverName.DataBindings.Clear();
            txtRecieverPhone.DataBindings.Clear();
            txtRecieverAddress.DataBindings.Clear();

            txtBillID.DataBindings.Add("Text", dtBill, "Bill_ID");
            txtOrder_ID.DataBindings.Add("Text", dtBill, "Order_ID");
            txtCreated_Date.DataBindings.Add("Text", dtBill, "Created_Date");
            txtTotal_Price.DataBindings.Add("Text", dtBill, "Total_Price");
            txtResponsible_Man.DataBindings.Add("Text", dtBill, "Responsible_Man");
            TimeDeliveredDate.DataBindings.Add("Text", dtBill, "Delivered_Date");
            TimeReceiveredDate.DataBindings.Add("Text", dtBill, "Received_Date");
            txtReason_Cancel.DataBindings.Add("Text", dtBill, "Reason_Cancel");
            txtShipperID.DataBindings.Add("Text", dtBill, "Shipper_ID");
            txtRecieverName.DataBindings.Add("Text", dtBill, "Reciever_Name");
            txtRecieverPhone.DataBindings.Add("Text", dtBill, "Reciever_Phone");
            txtRecieverAddress.DataBindings.Add("Text", dtBill, "Reciever_Addresss");

            dgvBill.DataSource = dtBill;
        }
    }
}
