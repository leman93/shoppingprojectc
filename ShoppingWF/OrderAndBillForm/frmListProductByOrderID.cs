﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ShoppingWF.BUS;
using ShoppingWF.Models;

namespace ShoppingWF.OrderAndBillForm
{
    public partial class UpdateBill : Form
    {
        int orderId;
        public UpdateBill(int orderId)
        {
            InitializeComponent();
            this.orderId = orderId;
        }

        private void txtResponsible_Man_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void btnUpdateBill_Click(object sender, EventArgs e)
        {
            var list = Product_Order_DetailsBUS.GetAllProductByOrderID(orderId);
            dataGridView1.DataSource = list;
        
        }

        private void UpdateBill_Load(object sender, EventArgs e)
        {
            this.btnUpdateBill_Click(sender, e);
        }
    }
}
