﻿namespace ShoppingWF.OrderAndBillForm
{
    partial class BillForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtBillID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtOrder_ID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTotal_Price = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtResponsible_Man = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtReason_Cancel = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtShipperID = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtRecieverName = new System.Windows.Forms.TextBox();
            this.txtRecieverPhone = new System.Windows.Forms.Label();
            this.txtReciverPhone = new System.Windows.Forms.TextBox();
            this.Reciever_Address = new System.Windows.Forms.Label();
            this.txtRecieverAddress = new System.Windows.Forms.TextBox();
            this.dgvBill = new System.Windows.Forms.DataGridView();
            this.TimeReceiveredDate = new System.Windows.Forms.DateTimePicker();
            this.TimeDeliveredDate = new System.Windows.Forms.DateTimePicker();
            this.btnGetAllBill = new System.Windows.Forms.Button();
            this.btnUpdateBill = new System.Windows.Forms.Button();
            this.btnDeleteBill = new System.Windows.Forms.Button();
            this.txtCreated_Date = new System.Windows.Forms.TextBox();
            this.lbBillID = new System.Windows.Forms.Label();
            this.lbOrderID = new System.Windows.Forms.Label();
            this.lbCreatedDate = new System.Windows.Forms.Label();
            this.lbTotalPrice = new System.Windows.Forms.Label();
            this.lbResponsible = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblUsername = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtSearchBillId = new System.Windows.Forms.TextBox();
            this.txtSearchOrderId = new System.Windows.Forms.TextBox();
            this.dtimeSearcFrom = new System.Windows.Forms.DateTimePicker();
            this.dtimeSearchTo = new System.Windows.Forms.DateTimePicker();
            this.chbSearchCancel = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBill)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(118, 111);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bill_ID";
            // 
            // txtBillID
            // 
            this.txtBillID.Location = new System.Drawing.Point(177, 111);
            this.txtBillID.Margin = new System.Windows.Forms.Padding(4);
            this.txtBillID.Name = "txtBillID";
            this.txtBillID.ReadOnly = true;
            this.txtBillID.Size = new System.Drawing.Size(225, 22);
            this.txtBillID.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(99, 161);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Order_ID";
            // 
            // txtOrder_ID
            // 
            this.txtOrder_ID.Location = new System.Drawing.Point(176, 158);
            this.txtOrder_ID.Margin = new System.Windows.Forms.Padding(4);
            this.txtOrder_ID.Name = "txtOrder_ID";
            this.txtOrder_ID.ReadOnly = true;
            this.txtOrder_ID.Size = new System.Drawing.Size(225, 22);
            this.txtOrder_ID.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(69, 211);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Created_Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(85, 257);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 17);
            this.label4.TabIndex = 0;
            this.label4.Text = "Total_Price";
            // 
            // txtTotal_Price
            // 
            this.txtTotal_Price.Location = new System.Drawing.Point(176, 254);
            this.txtTotal_Price.Margin = new System.Windows.Forms.Padding(4);
            this.txtTotal_Price.Name = "txtTotal_Price";
            this.txtTotal_Price.ReadOnly = true;
            this.txtTotal_Price.Size = new System.Drawing.Size(225, 22);
            this.txtTotal_Price.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(44, 308);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(121, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Responsible_Man";
            // 
            // txtResponsible_Man
            // 
            this.txtResponsible_Man.Location = new System.Drawing.Point(176, 303);
            this.txtResponsible_Man.Margin = new System.Windows.Forms.Padding(4);
            this.txtResponsible_Man.Name = "txtResponsible_Man";
            this.txtResponsible_Man.Size = new System.Drawing.Size(225, 22);
            this.txtResponsible_Man.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(59, 353);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Delivered_Date";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(479, 352);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 17);
            this.label7.TabIndex = 0;
            this.label7.Text = "Received_Date";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(476, 114);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(108, 17);
            this.label9.TabIndex = 0;
            this.label9.Text = "Reason_Cancel";
            // 
            // txtReason_Cancel
            // 
            this.txtReason_Cancel.Location = new System.Drawing.Point(603, 114);
            this.txtReason_Cancel.Margin = new System.Windows.Forms.Padding(4);
            this.txtReason_Cancel.Name = "txtReason_Cancel";
            this.txtReason_Cancel.Size = new System.Drawing.Size(225, 22);
            this.txtReason_Cancel.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(506, 162);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(78, 17);
            this.label10.TabIndex = 0;
            this.label10.Text = "Shipper_ID";
            // 
            // txtShipperID
            // 
            this.txtShipperID.Location = new System.Drawing.Point(604, 162);
            this.txtShipperID.Margin = new System.Windows.Forms.Padding(4);
            this.txtShipperID.Name = "txtShipperID";
            this.txtShipperID.Size = new System.Drawing.Size(225, 22);
            this.txtShipperID.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(475, 211);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(109, 17);
            this.label11.TabIndex = 0;
            this.label11.Text = "Reciever_Name";
            // 
            // txtRecieverName
            // 
            this.txtRecieverName.Location = new System.Drawing.Point(604, 211);
            this.txtRecieverName.Margin = new System.Windows.Forms.Padding(4);
            this.txtRecieverName.Name = "txtRecieverName";
            this.txtRecieverName.Size = new System.Drawing.Size(225, 22);
            this.txtRecieverName.TabIndex = 1;
            // 
            // txtRecieverPhone
            // 
            this.txtRecieverPhone.AutoSize = true;
            this.txtRecieverPhone.Location = new System.Drawing.Point(471, 261);
            this.txtRecieverPhone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.txtRecieverPhone.Name = "txtRecieverPhone";
            this.txtRecieverPhone.Size = new System.Drawing.Size(113, 17);
            this.txtRecieverPhone.TabIndex = 0;
            this.txtRecieverPhone.Text = "Reciever_Phone";
            // 
            // txtReciverPhone
            // 
            this.txtReciverPhone.Location = new System.Drawing.Point(604, 261);
            this.txtReciverPhone.Margin = new System.Windows.Forms.Padding(4);
            this.txtReciverPhone.Name = "txtReciverPhone";
            this.txtReciverPhone.Size = new System.Drawing.Size(225, 22);
            this.txtReciverPhone.TabIndex = 1;
            // 
            // Reciever_Address
            // 
            this.Reciever_Address.AutoSize = true;
            this.Reciever_Address.Location = new System.Drawing.Point(460, 307);
            this.Reciever_Address.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Reciever_Address.Name = "Reciever_Address";
            this.Reciever_Address.Size = new System.Drawing.Size(124, 17);
            this.Reciever_Address.TabIndex = 0;
            this.Reciever_Address.Text = "Reciever_Address";
            // 
            // txtRecieverAddress
            // 
            this.txtRecieverAddress.Location = new System.Drawing.Point(604, 307);
            this.txtRecieverAddress.Margin = new System.Windows.Forms.Padding(4);
            this.txtRecieverAddress.Name = "txtRecieverAddress";
            this.txtRecieverAddress.Size = new System.Drawing.Size(225, 22);
            this.txtRecieverAddress.TabIndex = 1;
            // 
            // dgvBill
            // 
            this.dgvBill.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBill.Location = new System.Drawing.Point(71, 475);
            this.dgvBill.Margin = new System.Windows.Forms.Padding(4);
            this.dgvBill.Name = "dgvBill";
            this.dgvBill.Size = new System.Drawing.Size(1106, 185);
            this.dgvBill.TabIndex = 2;
            // 
            // TimeReceiveredDate
            // 
            this.TimeReceiveredDate.Location = new System.Drawing.Point(603, 352);
            this.TimeReceiveredDate.Margin = new System.Windows.Forms.Padding(4);
            this.TimeReceiveredDate.Name = "TimeReceiveredDate";
            this.TimeReceiveredDate.Size = new System.Drawing.Size(225, 22);
            this.TimeReceiveredDate.TabIndex = 3;
            // 
            // TimeDeliveredDate
            // 
            this.TimeDeliveredDate.Location = new System.Drawing.Point(176, 353);
            this.TimeDeliveredDate.Margin = new System.Windows.Forms.Padding(4);
            this.TimeDeliveredDate.Name = "TimeDeliveredDate";
            this.TimeDeliveredDate.Size = new System.Drawing.Size(225, 22);
            this.TimeDeliveredDate.TabIndex = 3;
            // 
            // btnGetAllBill
            // 
            this.btnGetAllBill.Location = new System.Drawing.Point(965, 111);
            this.btnGetAllBill.Margin = new System.Windows.Forms.Padding(4);
            this.btnGetAllBill.Name = "btnGetAllBill";
            this.btnGetAllBill.Size = new System.Drawing.Size(133, 61);
            this.btnGetAllBill.TabIndex = 4;
            this.btnGetAllBill.Text = "Get All Bill";
            this.btnGetAllBill.UseVisualStyleBackColor = true;
            this.btnGetAllBill.Click += new System.EventHandler(this.btnGetAllBill_Click);
            // 
            // btnUpdateBill
            // 
            this.btnUpdateBill.Location = new System.Drawing.Point(1147, 111);
            this.btnUpdateBill.Margin = new System.Windows.Forms.Padding(4);
            this.btnUpdateBill.Name = "btnUpdateBill";
            this.btnUpdateBill.Size = new System.Drawing.Size(133, 61);
            this.btnUpdateBill.TabIndex = 4;
            this.btnUpdateBill.Text = "Update Bill";
            this.btnUpdateBill.UseVisualStyleBackColor = true;
            this.btnUpdateBill.Click += new System.EventHandler(this.btnUpdateBill_Click);
            // 
            // btnDeleteBill
            // 
            this.btnDeleteBill.Location = new System.Drawing.Point(965, 257);
            this.btnDeleteBill.Margin = new System.Windows.Forms.Padding(4);
            this.btnDeleteBill.Name = "btnDeleteBill";
            this.btnDeleteBill.Size = new System.Drawing.Size(133, 61);
            this.btnDeleteBill.TabIndex = 4;
            this.btnDeleteBill.Text = "Cancle Bill";
            this.btnDeleteBill.UseVisualStyleBackColor = true;
            this.btnDeleteBill.Click += new System.EventHandler(this.btnDeleteBill_Click);
            // 
            // txtCreated_Date
            // 
            this.txtCreated_Date.Location = new System.Drawing.Point(176, 206);
            this.txtCreated_Date.Margin = new System.Windows.Forms.Padding(4);
            this.txtCreated_Date.Name = "txtCreated_Date";
            this.txtCreated_Date.Size = new System.Drawing.Size(225, 22);
            this.txtCreated_Date.TabIndex = 1;
            // 
            // lbBillID
            // 
            this.lbBillID.AutoSize = true;
            this.lbBillID.Location = new System.Drawing.Point(173, 140);
            this.lbBillID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbBillID.Name = "lbBillID";
            this.lbBillID.Size = new System.Drawing.Size(99, 17);
            this.lbBillID.TabIndex = 5;
            this.lbBillID.Text = "Bill ID is empty";
            this.lbBillID.Visible = false;
            // 
            // lbOrderID
            // 
            this.lbOrderID.AutoSize = true;
            this.lbOrderID.Location = new System.Drawing.Point(173, 186);
            this.lbOrderID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbOrderID.Name = "lbOrderID";
            this.lbOrderID.Size = new System.Drawing.Size(118, 17);
            this.lbOrderID.TabIndex = 5;
            this.lbOrderID.Text = "Order ID is empty";
            this.lbOrderID.Visible = false;
            // 
            // lbCreatedDate
            // 
            this.lbCreatedDate.AutoSize = true;
            this.lbCreatedDate.Location = new System.Drawing.Point(173, 234);
            this.lbCreatedDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbCreatedDate.Name = "lbCreatedDate";
            this.lbCreatedDate.Size = new System.Drawing.Size(152, 17);
            this.lbCreatedDate.TabIndex = 5;
            this.lbCreatedDate.Text = "Created_Date is empty";
            this.lbCreatedDate.Visible = false;
            // 
            // lbTotalPrice
            // 
            this.lbTotalPrice.AutoSize = true;
            this.lbTotalPrice.Location = new System.Drawing.Point(173, 282);
            this.lbTotalPrice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbTotalPrice.Name = "lbTotalPrice";
            this.lbTotalPrice.Size = new System.Drawing.Size(132, 17);
            this.lbTotalPrice.TabIndex = 5;
            this.lbTotalPrice.Text = "Total Price is empty";
            this.lbTotalPrice.Visible = false;
            // 
            // lbResponsible
            // 
            this.lbResponsible.AutoSize = true;
            this.lbResponsible.Location = new System.Drawing.Point(172, 332);
            this.lbResponsible.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbResponsible.Name = "lbResponsible";
            this.lbResponsible.Size = new System.Drawing.Size(173, 17);
            this.lbResponsible.TabIndex = 5;
            this.lbResponsible.Text = "Responsible Man is empty";
            this.lbResponsible.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Yu Mincho", 28.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.label16.Location = new System.Drawing.Point(608, 14);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(281, 62);
            this.label16.TabIndex = 42;
            this.label16.Text = "Manage Bill";
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Font = new System.Drawing.Font("Yu Mincho", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lblUsername.Location = new System.Drawing.Point(99, 28);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(105, 43);
            this.lblUsername.TabIndex = 41;
            this.lblUsername.Text = "label1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1147, 257);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(133, 61);
            this.button1.TabIndex = 43;
            this.button1.Text = "Export to Excel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(37, 427);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(124, 20);
            this.label8.TabIndex = 44;
            this.label8.Text = "Search    Bill Id";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(293, 427);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(70, 20);
            this.label12.TabIndex = 45;
            this.label12.Text = "Order Id";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(1037, 426);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(61, 20);
            this.label13.TabIndex = 46;
            this.label13.Text = "Cancel";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(478, 427);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(48, 20);
            this.label14.TabIndex = 46;
            this.label14.Text = "From";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(761, 426);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(28, 20);
            this.label15.TabIndex = 47;
            this.label15.Text = "To";
            // 
            // txtSearchBillId
            // 
            this.txtSearchBillId.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchBillId.Location = new System.Drawing.Point(169, 424);
            this.txtSearchBillId.Margin = new System.Windows.Forms.Padding(4);
            this.txtSearchBillId.Name = "txtSearchBillId";
            this.txtSearchBillId.Size = new System.Drawing.Size(104, 27);
            this.txtSearchBillId.TabIndex = 48;
            // 
            // txtSearchOrderId
            // 
            this.txtSearchOrderId.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearchOrderId.Location = new System.Drawing.Point(371, 424);
            this.txtSearchOrderId.Margin = new System.Windows.Forms.Padding(4);
            this.txtSearchOrderId.Name = "txtSearchOrderId";
            this.txtSearchOrderId.Size = new System.Drawing.Size(104, 27);
            this.txtSearchOrderId.TabIndex = 49;
            // 
            // dtimeSearcFrom
            // 
            this.dtimeSearcFrom.Location = new System.Drawing.Point(525, 427);
            this.dtimeSearcFrom.Margin = new System.Windows.Forms.Padding(4);
            this.dtimeSearcFrom.Name = "dtimeSearcFrom";
            this.dtimeSearcFrom.Size = new System.Drawing.Size(226, 22);
            this.dtimeSearcFrom.TabIndex = 3;
            // 
            // dtimeSearchTo
            // 
            this.dtimeSearchTo.Location = new System.Drawing.Point(797, 424);
            this.dtimeSearchTo.Margin = new System.Windows.Forms.Padding(4);
            this.dtimeSearchTo.Name = "dtimeSearchTo";
            this.dtimeSearchTo.Size = new System.Drawing.Size(226, 22);
            this.dtimeSearchTo.TabIndex = 3;
            // 
            // chbSearchCancel
            // 
            this.chbSearchCancel.AutoSize = true;
            this.chbSearchCancel.Location = new System.Drawing.Point(1105, 430);
            this.chbSearchCancel.Name = "chbSearchCancel";
            this.chbSearchCancel.Size = new System.Drawing.Size(18, 17);
            this.chbSearchCancel.TabIndex = 50;
            this.chbSearchCancel.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1147, 418);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(102, 33);
            this.button2.TabIndex = 51;
            this.button2.Text = "Search";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // BillForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1355, 734);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.chbSearchCancel);
            this.Controls.Add(this.txtSearchOrderId);
            this.Controls.Add(this.txtSearchBillId);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.lbResponsible);
            this.Controls.Add(this.lbTotalPrice);
            this.Controls.Add(this.lbCreatedDate);
            this.Controls.Add(this.lbOrderID);
            this.Controls.Add(this.lbBillID);
            this.Controls.Add(this.btnDeleteBill);
            this.Controls.Add(this.btnUpdateBill);
            this.Controls.Add(this.btnGetAllBill);
            this.Controls.Add(this.dtimeSearchTo);
            this.Controls.Add(this.dtimeSearcFrom);
            this.Controls.Add(this.TimeDeliveredDate);
            this.Controls.Add(this.TimeReceiveredDate);
            this.Controls.Add(this.dgvBill);
            this.Controls.Add(this.txtRecieverAddress);
            this.Controls.Add(this.txtReciverPhone);
            this.Controls.Add(this.txtRecieverName);
            this.Controls.Add(this.txtShipperID);
            this.Controls.Add(this.txtReason_Cancel);
            this.Controls.Add(this.Reciever_Address);
            this.Controls.Add(this.txtRecieverPhone);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtTotal_Price);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtCreated_Date);
            this.Controls.Add(this.txtOrder_ID);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtResponsible_Man);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtBillID);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "BillForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bill Manager";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.BillForm_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBill)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBillID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtOrder_ID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTotal_Price;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtResponsible_Man;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtReason_Cancel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtShipperID;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtRecieverName;
        private System.Windows.Forms.Label txtRecieverPhone;
        private System.Windows.Forms.TextBox txtReciverPhone;
        private System.Windows.Forms.Label Reciever_Address;
        private System.Windows.Forms.TextBox txtRecieverAddress;
        private System.Windows.Forms.DataGridView dgvBill;
        private System.Windows.Forms.DateTimePicker TimeReceiveredDate;
        private System.Windows.Forms.DateTimePicker TimeDeliveredDate;
        private System.Windows.Forms.Button btnGetAllBill;
        private System.Windows.Forms.Button btnUpdateBill;
        private System.Windows.Forms.Button btnDeleteBill;
        private System.Windows.Forms.TextBox txtCreated_Date;
        private System.Windows.Forms.Label lbBillID;
        private System.Windows.Forms.Label lbOrderID;
        private System.Windows.Forms.Label lbCreatedDate;
        private System.Windows.Forms.Label lbTotalPrice;
        private System.Windows.Forms.Label lbResponsible;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtSearchBillId;
        private System.Windows.Forms.TextBox txtSearchOrderId;
        private System.Windows.Forms.DateTimePicker dtimeSearcFrom;
        private System.Windows.Forms.DateTimePicker dtimeSearchTo;
        private System.Windows.Forms.CheckBox chbSearchCancel;
        private System.Windows.Forms.Button button2;
    }
}

