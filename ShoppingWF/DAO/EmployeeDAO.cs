﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.DAO
{
    class EmployeeDAO
    {
        public static Employee getEmployeeByID(string employee_ID)
        {
            Employee result = null;
            using (OnlineShoppingEntities1 db =new OnlineShoppingEntities1())
            {
                result = db.Employees.FirstOrDefault<Employee>(m => m.Employee_ID.Equals(employee_ID));
            }
            return result;
        }

    }
}
