﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.DAO
{
    class Product_Order_DetailsDAO
    {
        public static List<Product_Order_Details> GetAllProductByOrderID(int id)
        {
            List<Product_Order_Details> objresult;
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                objresult = db.Product_Order_Details.Where(m => m.Order_ID == id).ToList();
            }
            return objresult;
        }
    }
}
