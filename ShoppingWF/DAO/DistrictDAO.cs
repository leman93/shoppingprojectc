﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.DAO
{
    class DistrictDAO
    {
        public static List<District> getAllDistricts()
        {
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                var result = db.Districts.SqlQuery("SELECT * FROM District").ToList<District>();
                return result;
            }
        }

        public static List<District> getDistrictsByProvinceId(int provinceId)
        {
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                var districs = from distric in db.Districts
                               where (distric.Province_ID == provinceId)
                               select distric;
                return districs.ToList<District>();
            }
        }

        public static District getDistrictById(int id)
        {
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                return db.Districts.SingleOrDefault(x => x.District_ID == id);
            }
        }
    }
}
