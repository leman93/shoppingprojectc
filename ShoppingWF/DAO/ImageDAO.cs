﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.DAO
{
    class ImageDAO
    {
        public static List<Image> getAllImages(int id)
        {
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                var images = from img in db.Images
                             where (img.Product_ID == id)
                             select img;
                return images.ToList<Image>();
            }
        }

        public static bool update(Image img)
        {
            bool result = false;
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                var currentImg = db.Images.Single(i => i.Image_ID == img.Image_ID);
                currentImg.Url = img.Url;
                db.SaveChanges();
                result = true;
            }
            return result;
        }

        public static bool insert(Image img)
        {
            bool result = false;
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                var tmp = db.Images.Add(img);
                if(tmp != null)
                {
                    db.SaveChanges();
                    result = true;
                }
            }
            return result;
        }

        public static bool delete(int id)
        {
            bool result = false;
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                var img = db.Images.SingleOrDefault(i => i.Image_ID == id);
                if(img != null)
                {
                    db.Images.Remove(img);
                    db.SaveChanges();
                    result = true;
                }
            }
            return result;
        }
    }
}
