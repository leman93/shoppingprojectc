﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.DAO
{
    class RoleDAO
    {
        public static List<Role> getAllRoles()
        {
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                return db.Roles.SqlQuery("SELECT * FROM Role").ToList<Role>();
            }
        }
    }
}
