﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.DAO
{
    class Order_DetailDAO
    {
        public static List<Order_Details> GetAllOrder_DetailsModels()
        {
            List<Order_Details> objresult;
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                objresult = db.Order_Details.SqlQuery("SELECT * FROM dbo.Order_Details").ToList<Order_Details>();
            }
            return objresult;
        }
        public static Order_Details GetOrder_DetailsById(int id)
        {
            var objresult = new Order_Details();
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                objresult = db.Order_Details.SingleOrDefault(m => m.Order_ID == id);
            }
            return objresult;
        }
        public static bool Update(Order_Details obj)
        {
            bool objResult = false;
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                var tempRs = db.Order_Details.Single(i => i.Order_ID == obj.Order_ID);

                tempRs.Email = obj.Email;
                tempRs.Ordered_Date = obj.Ordered_Date;
                tempRs.Delivered = obj.Delivered;
                tempRs.Cancelled_Order = obj.Cancelled_Order;
                tempRs.Reason_Cancel = obj.Reason_Cancel;
                tempRs.Order_Name = obj.Order_Name;
                tempRs.Exact_Address = obj.Exact_Address;
                tempRs.Order_Phone = obj.Order_Phone;
                tempRs.Province_ID = obj.Province_ID;
                tempRs.District_ID = obj.District_ID;
                tempRs.Town_ID = obj.Town_ID;

                db.SaveChanges();
                objResult = true;
            }
            return objResult;
        }
        public static bool Delete(long objID)
        {
            bool result = false;
            if (objID > 0)
            {
                using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
                {
                    var tempRs = db.Order_Details.SingleOrDefault(m => m.Order_ID == objID);
                    if (tempRs != null)
                    {
                        db.Order_Details.Remove(tempRs);
                        db.SaveChanges();
                        result = true;
                    }
                }
            }
            return result;
        }

        internal static List<Order_Details_Filter_Result> Search(int ordreId, string email,bool isCanceled , DateTime fromDate, DateTime toDate)
        {
            List<Order_Details_Filter_Result> objResult;
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                objResult = db.Order_Details_Filter(ordreId, email, isCanceled, fromDate, toDate).ToList();
            }
            return objResult;
        }
    }
}
