﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.DAO
{
    class TownDAO
    {
        public static List<Town> getAllTowns()
        {
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                return db.Towns.SqlQuery("SELECT * FROM Town").ToList<Town>();
            }
        }

        public static List<Town> getAllTownsByDistrictId(int districtId)
        {
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                var towns = from town in db.Towns
                            where town.District_ID == districtId
                            select town;
                return towns.ToList<Town>();
            }
        }

        public static Town getTownbyId(int id)
        {
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                return db.Towns.SingleOrDefault(town => town.Town_ID == id);
            }
        }
    }
}
