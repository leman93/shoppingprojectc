﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.DAO
{
    class PromotionDAO
    {
        public static List<Promotion> GetAllPromotions()
        {
            List<Promotion> objresult;
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                objresult = db.Promotions.SqlQuery("SELECT * FROM dbo.Promotion").ToList();
            }
            return objresult;
        }
        public static Promotion GetProtionById(int id)
        {
            var objresult = new Promotion();
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                objresult = db.Promotions.SingleOrDefault(m => m.Promotion_ID == id);
            }
            return objresult;
        }
        public static bool Update(Promotion obj)
        {
            bool objResult = false;
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                var tempRs = db.Promotions.Single(i => i.Promotion_ID == obj.Promotion_ID);

                tempRs.Discount_Percent = obj.Discount_Percent;
                tempRs.Promotion_Description = obj.Promotion_Description;
                tempRs.From_Date = obj.From_Date;
                tempRs.To_Date = obj.To_Date;
                tempRs.Enable = obj.Enable;

                db.SaveChanges();
                objResult = true;
            }
            return objResult;
        }
        public static bool Delete(int objID)
        {
            bool result = false;
            if (objID > 0)
            {
                using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
                {
                    var tempRs = db.Promotions.SingleOrDefault(m => m.Promotion_ID == objID);
                    if (tempRs != null)
                    {
                        db.Promotions.Remove(tempRs);
                        db.SaveChanges();
                        result = true;
                    }
                }
            }
            return result;
        }


        public static bool insert(Promotion newPromotion)
        {
            bool result = false;
            using (var db = new OnlineShoppingEntities1())
            {
                var insertedPromotion = db.Promotions.Add(newPromotion);
                if(insertedPromotion != null)
                {
                    db.SaveChanges();
                    result = true;
                }
            }
            return result;
        }

        internal static List<Promotion_Filter_Result> Search(int promoID, int proId, bool enable, DateTime fromDate, DateTime toDate)
        {
            List<Promotion_Filter_Result> objResult;
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                objResult = db.Promotion_Filter(promoID, proId, enable, fromDate, toDate).ToList();
            }
            return objResult;
        }

        internal static List<Promotion> Search(int promoID, int proId, bool enable)
        {
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                var result = from pro in db.Promotions
                             where pro.Enable == enable
                             select pro;
                if(proId > 0)
                {
                    result = result.Where(pro => pro.Product_ID == proId);
                }
                if(promoID > 0)
                {
                    result = result.Where(pro => pro.Promotion_ID == promoID);
                }
                return result.ToList<Promotion>();
            }
        }
    }
}
