﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using app = Microsoft.Office.Interop.Excel.Application;
using System.Windows.Forms;

namespace ShoppingWF.DAO
{
    class ExportExcel
    {
        public static void ExportToExcel(DataGridView dgvData, string link, string nameFile)
        {
            app obj = new app();
            obj.Application.Workbooks.Add(Type.Missing);
            obj.Columns.ColumnWidth = 25;
            for (int i = 1; i < dgvData.Columns.Count + 1; i++)
            {
                obj.Cells[1, i] = dgvData.Columns[i - 1].HeaderText;
            }
            for (int i = 0; i < dgvData.Rows.Count; i++)
            {
                for (int j = 0; j < dgvData.Columns.Count; j++)
                {
                    var tmp = dgvData.Rows[i].Cells[j].Value;
                    if (tmp != null)
                    {
                        obj.Cells[i + 2, j + 1] = dgvData.Rows[i].Cells[j].Value.ToString();
                    }
                }
            }
            obj.ActiveWorkbook.SaveCopyAs(link + DateTime.Now.Ticks+ "_" +nameFile + ".xls");
            obj.ActiveWorkbook.Saved = true;
        }
    }
}
