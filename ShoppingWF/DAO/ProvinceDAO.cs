﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShoppingWF.DAO
{
    class ProvinceDAO
    {
        public static List<Province> getAllProvinces()
        {
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                return db.Provinces.SqlQuery("SELECT * FROM Province").ToList<Province>();
            }
        }

        public static Province getProvinceById(int id)
        {
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                return db.Provinces.SingleOrDefault(x => x.Province_ID == id);
            }
        }
    }
}
