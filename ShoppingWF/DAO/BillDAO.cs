﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShoppingWF.Models;

namespace ShoppingWF.DAO
{
    class BillDAO
    {
        public static List<Bill> GetAllBills()
        {
            List<Bill> objresult;
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                objresult = db.Bills.SqlQuery("SELECT * FROM dbo.Bill").ToList<Bill>();
            }
            return objresult;
        }
        public static Bill GetBillById(int id)
        {
            var objresult = new Bill();
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                objresult = db.Bills.SingleOrDefault(m => m.Bill_ID == id);
            }
            return objresult;
        }
        public static Bill GetBillByOrderId(int id)
        {
            var objresult = new Bill();
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                objresult = db.Bills.SingleOrDefault(m => m.Order_ID == id);
            }
            return objresult;
        }
        public static bool Update(Bill obj)
        {
            bool objResult = false;
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                var tempRs = db.Bills.Single(i => i.Bill_ID == obj.Bill_ID);
                tempRs.Responsible_Man = obj.Responsible_Man;
                tempRs.Delivered_Date = obj.Delivered_Date;
                tempRs.Received_Date = obj.Received_Date;
                tempRs.Reason_Cancel = obj.Reason_Cancel;
                tempRs.Shipper_ID = obj.Shipper_ID;
                tempRs.Reciever_Name = obj.Reciever_Name;
                tempRs.Reciever_Phone = obj.Reciever_Phone;
                tempRs.Reciever_Addresss = obj.Reciever_Addresss;

                db.SaveChanges();
                objResult = true;
            }
            return objResult;
        }
        public static bool Delete(long objID)
        {
            bool result = false;
            if (objID > 0)
            {
                using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
                {
                    var tempRs = db.Bills.SingleOrDefault(m => m.Bill_ID == objID);
                    tempRs.Cancelled_Bill = true;
                    db.SaveChanges();
                    result = true;

                }
            }
            return result;
        }

        internal static List<Bill_Filter_Result> Search(int billId, int orderId, DateTime fromDate, DateTime toDate, bool isCancel)
        {
            List<Bill_Filter_Result> objResult;
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                objResult = db.Bill_Filter(billId, orderId, fromDate, toDate, isCancel).ToList();
            }
            return objResult;
        }

        internal static bool Insert(int cataID, bool isSale, string proName, float price, int level_trending, string description, int proAvai, int totalSole, string guaranteeDesc, string title_image, float tax_percent, string manufacture, string userCreated)
        {
            bool result = false;
            Product pro = new Product
            {
                Catalogue_ID = cataID,
                Is_Sale = isSale,
                Product_Name = proName,
                Price = price,
                Level_Trending = level_trending,
                Description = description,
                Products_Available = proAvai,
                Total_Sold = totalSole,
                Created_Date = DateTime.Now,
                Created_Username = userCreated,
                Guarantee_Description = guaranteeDesc,
                Title_Image = title_image,
                Tax_Percent = tax_percent,
                Manufacturer = manufacture
            };
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                var tempRs = db.Products.Add(pro);
                if (tempRs != null)
                {
                    db.SaveChanges();
                    result = true;
                }
            }
            return result;
        }

        internal static bool Insert(Bill bill)
        {
            bool result = false;
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                var tempRs = db.Bills.Add(bill);
                if (tempRs != null)
                {
                    db.SaveChanges();
                    result = true;
                }
            }
            return result;
        }
    }
}
