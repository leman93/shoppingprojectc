﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ShoppingWF.DAO
{
    class ProductDAO
    {
        public static List<Product> GetAllProducts()
        {
            List<Product> objresult;
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                objresult = db.Products.SqlQuery("SELECT * FROM dbo.Product").ToList<Product>();
            }
            return objresult;
        }
        public static Product GetProductById(int id)
        {
            var objresult = new Product();
            using( OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                objresult = db.Products.SingleOrDefault(m => m.Product_ID == id);
            }
            return objresult;
        }
        public static bool Update(Product obj)
        {
            bool objResult = false;
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                var tempRs = db.Products.Single(i => i.Product_ID == obj.Product_ID);

                tempRs.Catalogue_ID = obj.Catalogue_ID;
                tempRs.Is_Sale = obj.Is_Sale;
                tempRs.Product_Name = obj.Product_Name;
                tempRs.Price = obj.Price;
                tempRs.Level_Trending = obj.Level_Trending;
                tempRs.Description = obj.Description;
                tempRs.Products_Available = obj.Products_Available;
                tempRs.Total_Sold = obj.Total_Sold;
                tempRs.Created_Date = obj.Created_Date;
                tempRs.Created_Username = obj.Created_Username;
                tempRs.Guarantee_Description = obj.Guarantee_Description;
                tempRs.Title_Image = obj.Title_Image;
                tempRs.Tax_Percent = obj.Tax_Percent;
                tempRs.Manufacturer = obj.Manufacturer;

                db.SaveChanges();
                objResult = true;
            }
            return objResult;
        }
        public static bool Delete(long objID)
        {
            bool result = false;
            if (objID > 0)
            {
                using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
                {
                    var tempRs = db.Products.SingleOrDefault(m => m.Product_ID == objID);
                    if (tempRs != null)
                    {
                        db.Products.Remove(tempRs);
                        db.SaveChanges();
                        result = true; 
                    }
                } 
            }
            return result;
        }

        public static List<Product> Search(int proID, int cataId, string proName)
        {
            using (var db = new OnlineShoppingEntities1())
            {
                var products = from product in db.Products
                               select product;
                Debug.Print("count: " + products.Count());

                if (proID > 0)
                {
                    products = products.Where( product => product.Product_ID == proID);
                }
                if(cataId > 0)
                {
                    products = products.Where(product => product.Catalogue_ID == cataId);
                }
                if (!string.IsNullOrWhiteSpace(proName))
                {
                    products = products.Where(product => product.Product_Name.ToUpper().Contains(proName.ToUpper()) == true);
                }
                Debug.Print("count: " + products.Count());
                return products.ToList<Product>();
            }
        }

        internal static List<Product_Filter_Result> Search(int proID, int cataId, string proName, DateTime fromDate, DateTime toDate)
        {
            List<Product_Filter_Result> objResult;
            using (OnlineShoppingEntities1 db =new OnlineShoppingEntities1())
            {
                objResult = db.Product_Filter(proID, cataId, proName, fromDate, toDate).ToList();
            }
            return objResult;
        }

        internal static bool Insert(int cataID, bool isSale, string proName, float price, int level_trending, string description, int proAvai, int totalSole, string guaranteeDesc, string title_image, float tax_percent, string manufacture , string userCreated)
        {
            bool result = false;
            Product pro = new Product
            {
                Catalogue_ID = cataID,
                Is_Sale = isSale,
                Product_Name = proName,
                Price = price,
                Level_Trending = level_trending,
                Description = description,
                Products_Available = proAvai,
                Total_Sold = totalSole,
                Created_Date = DateTime.Now,
                Created_Username = userCreated,
                Guarantee_Description = guaranteeDesc,
                Title_Image = title_image,
                Tax_Percent = tax_percent,
                Manufacturer = manufacture
            };
            using(OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                var tempRs = db.Products.Add(pro);
                if( tempRs != null)
                {
                    db.SaveChanges();
                    result = true;
                }
            }
            return result;
        }
    }
}
