﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShoppingWF.Models;
namespace ShoppingWF.DAO
{
    public class Employee_AccountDAO
    {
        public static Employee_Account getEmployeeByUsername(string userName)
        {
            Employee_Account emp = null;
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                emp = db.Employee_Account.FirstOrDefault(m => m.Username.Equals(userName));
            }
            return emp;
        }
        public static List<Employee_Account> GetAllEmployee_Account()
        {
            List<Employee_Account> objresult;
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                objresult = db.Employee_Account.SqlQuery("SELECT * FROM dbo.Employee_Account").ToList<Employee_Account>();
            }
            return objresult;
        }
        public static bool Delete(string username)
        {
            bool result = false;
            
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                var tmp = db.Employee_Account.SingleOrDefault(m => m.Username.Equals(username));
                db.Employee_Account.Remove(tmp);
                db.SaveChanges();
                result = true;
            }
            return result;
        }
        public static bool Insert(Employee_Account obj)
        {
            bool result = false;
            using (OnlineShoppingEntities1 db =new OnlineShoppingEntities1())
            {
                if( db.Employee_Account.SingleOrDefault(m => m.Username == obj.Username) == null)
                {
                    db.Employee_Account.Add(obj);
                    db.SaveChanges();
                    result = true;
                }
            }
            return result;
        }

        internal static bool Update(Employee_Account obj)
        {
            bool result = false;
            using (OnlineShoppingEntities1 db = new OnlineShoppingEntities1())
            {
                var tmp = db.Employee_Account.SingleOrDefault(m => m.Username == obj.Username);
                if( tmp != null)
                {
                    tmp.Password = obj.Password;
                    tmp.Level_Access = obj.Level_Access;
                    db.SaveChanges();
                    result = true;
                }
            }
            return result;
        }
    }
}
