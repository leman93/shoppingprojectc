namespace Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;

    [Table("Employee")]
    public partial class Employee
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Employee()
        {
            Bills = new HashSet<Bill>();
            Employee_Account = new HashSet<Employee_Account>();
        }

        [Key]
        [StringLength(50)]
        public string Employee_ID { get; set; }

        [StringLength(250)]
        public string Full_Name { get; set; }

        [Column(TypeName = "ntext")]
        public string Address { get; set; }

        [StringLength(250)]
        public string Role_ID { get; set; }

        [StringLength(50)]
        public string Phone_Number { get; set; }

        public bool? Enable { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Bill> Bills { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Employee_Account> Employee_Account { get; set; }

        public virtual Role Role { get; set; }
    }
}
