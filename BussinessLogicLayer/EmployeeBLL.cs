﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;
using Encryption;
using System.Data;
using System.Data.SqlClient;

namespace BussinessLogicLayer
{
 public   class EmployeeBLL
    {   
        public bool checkEmployeeID(string Employee_ID)
        {
            EmployeeDAL DAL = new EmployeeDAL();

            return DAL.getEmployeeID(Employee_ID);
        }

        public DataSet getAllEmp()
        {
            DataSet dsEmp = new DataSet();
            EmployeeDAL DAL = new EmployeeDAL();
            dsEmp = DAL.GetEmpByDataSet();
            return dsEmp;

        }
        public string createEmployee(string fullname,string address,string Role,string Phone)
        {
            EmployeeDAL DAL = new EmployeeDAL();
            string ID = getEmpID();
            bool check = DAL.createEmployee(ID, fullname, address, Role, Phone);
            if (check)
            {
                return ID;
            }
            else
            {
                return "No";
            }
           
        }
        public string getEmpID()
        {
            EmployeeDAL DAL = new EmployeeDAL();
            string TOPID = DAL.getTOPEmployee();
            string formatID = "E";
            int substring = int.Parse(TOPID.Substring(1)) + 1;
            string ID2 = substring.ToString();
            string newIDstring = string.Concat(formatID, ID2);
            return newIDstring;
        }
        public bool DeleteAccount(string Username)
        {
            EmployeeDAL DAL = new EmployeeDAL();
            if (DAL.deleteAccount(Username))
            {

                return true;
            }
            return false;
        }

        public bool UpdateEmployee(string ID, string fullName, string role,string address,string phone)
        {
            EmployeeDAL DAL = new EmployeeDAL();
            if (DAL.updateAccount(ID, fullName, role, address, phone))
            {

                return true;
            }
            return false;
             
        }

        public DataSet SearchEmployeeNameByDataSet(string searchValue)
        {
            DataSet dsEmp = new DataSet();
            EmployeeDAL DAL = new EmployeeDAL();
            dsEmp = DAL.searchByUsername(searchValue);
            return dsEmp;
        }

    }
}
