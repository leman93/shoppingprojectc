﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using System.Data;
using System.Data.SqlClient;
using Encryption;
using DataAccessLayer;


namespace BussinessLogicLayer
{
    public class Employee_AccountBLL
    {
        public int checkRoleLogin(string Username)
        {
            
            Employee_Account user = new Employee_Account();
            Employee_AccountDAL DAL = new Employee_AccountDAL();
            user = DAL.GetAccountByUsername(Username);
            if (user == null)
            {
                return 0;
            }
            int role = user.Level_Access;
            return role;
        } //ok
        public bool checkLogin(string username,string password)
        {
            Employee_AccountDAL DAL = new Employee_AccountDAL();
            Employee_Account account = DAL.GetAccountByUsername(username);
            if (account != null)
            {

                EncryptionSHA sha = new EncryptionSHA();
                bool result = sha.doesPasswordMatch(password, account.Password);
                if (result)
                {
                    return true;
                }
            }


            return false;
        }//ok
      
        public DataSet getAllAccount()
        {
            DataSet dsAccount = new DataSet();
            Employee_AccountDAL DAL = new Employee_AccountDAL();
            dsAccount = DAL.GetAccountByDataSet();
            return dsAccount;

        }

        public bool checkUsernameCreated(string Username)
        {
            Employee_AccountDAL DAL = new Employee_AccountDAL();
            return DAL.getUsername(Username);
        }
        public bool CreateAccountRole(string Username)
        {
            Employee_AccountDAL DAL = new Employee_AccountDAL();

            Employee_Account admin = DAL.GetAccountByUsername(Username);
            if (admin == null)
            {
                return false;
            }
            int role =admin.Level_Access;
            if (role == 1)
            {
                return true;
            }

            return false;
        }

        public int checkRole(string role)
        {
            if (role.Equals("admin"))
            {
                return 1;
            }else if(role.Equals("manager"))
            {
                return 2;

            }else
            {
                return 3;
            }
            
            
        }
        
      public bool createAccount(string username,string password,string Employee_ID,string Level_Access,string created_by_Username)
        {
            
            EncryptionSHA SHA = new EncryptionSHA();
            string pass2 = SHA.getHashedPassword(password);
            int role = checkRole(Level_Access);
            DateTime Created_Date = DateTime.Now;


            Employee_Account acc = new Employee_Account { Username=username,Password =pass2,Employee_ID = Employee_ID,Level_Access=role,Enable=true,Created_Date=Created_Date,Created_By_Username = created_by_Username};
             Employee_AccountDAL empDAL = new Employee_AccountDAL();
             if (empDAL.addNewAccount(acc))
             {
                
                 return true;
             }
            return false;
           
        }
       
        public Employee_Account getInfoAccount(string Username)
        {
            Employee_AccountDAL DAL = new Employee_AccountDAL();

            return DAL.GetAccountByUsername(Username);
        }

        public bool UpdateAccount(string Username,string password, string level_Access)
        {
            EncryptionSHA SHA = new EncryptionSHA();
            string pass2 = SHA.getHashedPassword(password);
            int role = checkRole(level_Access);
            Employee_AccountDAL DAL = new Employee_AccountDAL();
            if (DAL.updateAccount(Username,pass2,role))
            {

                return true;
            }
            return false;

        }
        public bool DeleteAccount(string Username)
        {
            Employee_AccountDAL DAL = new Employee_AccountDAL();
            if (DAL.deleteAccount(Username))
            {

                return true;
            }
            return false;
        }

        public List<Employee_Account> searchbyUsername(string Keyword)
        {
            List<Employee_Account> list = new List<Employee_Account>();
            Employee_AccountDAL DAL = new Employee_AccountDAL();
            list = DAL.searchByUsername(Keyword);
            return list;
        }

        public bool checkEmployeeID(string id)
        {
            Employee_AccountDAL DAL = new Employee_AccountDAL();
            return DAL.getEmployeeID(id);
        }
    }
}
